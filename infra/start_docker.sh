source $(realpath $(dirname "$0"))/env.sh
docker start $APB_DATABASE
if [ ! -f "$APB_HOME/etc/db-migrated" ]; then
  source $(realpath $(dirname "$0"))/db_migrate.sh &&
  touch $APB_HOME/etc/db-migrated
fi
docker start $APB_BE
docker start $APB_NGINX
