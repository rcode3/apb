APB_HOME=$(dirname $(dirname $(dirname $(realpath "$0"))))

# a timestamp.... useful to have especially when creating backup files.
APB_TIMESTAMP=$(date +"%Y-%m-%d_%H-%M-%S")

# Docker network name.
APB_NET="apb-net"

# Docker database container name
APB_DATABASE="apb-database"

# Host name (used in URLs and such)
APB_HOST="example.com"

# Name of postgres user
APB_PGADMIN="apb_admin"

# Postgres users password
APB_PGADMINPW="apb_rules"

# JWT secret
APB_JWT_SECRET="CHANGE_NOW"

# JWT issuer
APB_JWT_ISSUER="com.example.apb"

# JWT realm
APB_JWT_REALM="My_APB"

# Name of nginx Docker container
APB_NGINX="apb-nginx"

# The Docker image to use for JRE
APB_DOCKER_JRE="timbru31/java-node:11-jre"

# Name of the nginx configuration file
APB_NGINX_CONF="nginx.conf"

# Host port for nginx to listen on
APB_NGINX_PORT=9000

# Name of the APB back-end Docker container
APB_BE="apb-be"

# Used by nginx to reference proxying to APB back-end
APB_BE_PROXY_PASS=$APB_BE

# Variables for the running of the CLI commands.
#
# An example of changing this to work outside of Docker (i.e. dev work)
#   APB_CLI_PRECMD=""
#   APB_CLI_JARLOC="$HOME/projects/apb/apb_be/cli/build/libs/apb-cli-fat.jar"
#   APB_CLI_JAVABIN="$JAVA_HOME/bin/java"

# Used for running in Docker.
APB_CLI_MEDIA_VOL="-v $APB_HOME/media:/media"
APB_CLI_LIB_VOL="-v $APB_HOME/libs:/apb-libs"
APB_CLI_ETC_VOL="-v $APB_HOME/etc:/apb-etc"
APB_CLI_LOG_VOL="-v $APB_HOME/logs:/var/log"
APB_CLI_PRECMD="docker run --rm -it --network $APB_NET $APB_CLI_ETC_VOL $APB_CLI_MEDIA_VOL $APB_CLI_LIB_VOL $APB_CLI_LOG_VOL --env-file $APB_HOME/etc/docker-env.list $APB_DOCKER_JRE"

# The location of the JAR file. When in Docker, it needs to be located in the docker volume.
APB_CLI_JARLOC="/apb-libs/apb-cli-fat.jar"

# The java executable. For the timbru31 Docker images, it's just 'java'
APB_CLI_JAVABIN="java -Dlogback.configurationFile=/apb-etc/logback-cli.xml"

APB_EXPOSED_DB_PORT_OPTION=""

# User level overrides... very useful to devs
if [ -f "$HOME/.apb_env.sh" ]; then
    echo "Sourcing $HOME/.apb_env.sh"
    . "$HOME/.apb_env.sh"
fi

echo "APB Home determined to be $APB_HOME"

# Deployment specific overrides
if [ -f "$APB_HOME/etc/apb_env.sh" ]; then
    echo "Sourcing $APB_HOME/etc/apb_env.sh"
    . "$APB_HOME/etc/apb_env.sh"
fi
