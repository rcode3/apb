source $(realpath $(dirname "$0"))/env.sh

# do exports for envsubst
export APB_DATABASE
export APB_PGADMIN
export APB_PGADMINPW
export APB_HOST
export APB_HOME
export APB_JWT_SECRET
export APB_JWT_ISSUER
export APB_JWT_REALM
export APB_BE_PROXY_PASS

# docker-env.list
if [ -f "$APB_HOME/etc/docker-env.list" ]; then
  echo "Creating a backup of docker-env.list"
  cp $APB_HOME/etc/docker-env.list $APB_HOME/etc/docker-env.list.bak.$APB_TIMESTAMP
fi
echo "Writing $APB_HOME/etc/docker-env.list"
envsubst <$APB_HOME/bin/example-docker-env.list >$APB_HOME/etc/docker-env.list

# nginx.conf
if [ -f "$APB_HOME/etc/nginx.conf" ]; then
  echo "Creating a backup of nginx.conf"
  cp $APB_HOME/etc/nginx.conf $APB_HOME/etc/nginx.conf.bak.$APB_TIMESTAMP
fi
echo "Writing $APB_HOME/etc/nginx.conf"
envsubst '$APB_BE_PROXY_PASS $APB_HOST' <$APB_HOME/bin/example-nginx.conf >$APB_HOME/etc/nginx.conf
