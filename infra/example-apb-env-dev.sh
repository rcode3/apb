# Change to your host name
# Should match what will be in your Host header, but this is ultimately what goes in the database as the host.
export APB_HOST=[YOUR HOST NAME HERE]

# Used for signing JWTs. Keep it secret. Change it now!
export APB_JWT_SECRET="secretjwtstuff"

# Change if you want, but leaving as your host name is ok.
export APB_JWT_ISSUER=$APB_HOST

# Change to something simple
export APB_JWT_REALM="APBDEVSTUFF"

# Change this to point to your JDK
export JAVA_HOME=$HOME/.jdks/corretto-11.0.10

# Change this to point to your deploy directory
APB_HOME=$HOME/apb
export APB_MEDIA_DIR=$APB_HOME/media
export APB_URL_SCHEME=http

# This is used to expose Postgres ports to the host.
APB_EXPOSED_DB_PORT_OPTION="-p 5432:5432"

# Used to get the CLIs to work locally. Hopefully you don't have to touch these.
APB_CLI_PRECMD=""
APB_CLI_JARLOC="$HOME/projects/apb/be/cli/build/libs/apb-cli-fat.jar"
APB_CLI_JAVABIN="$JAVA_HOME/bin/java -Dlogback.statusListenerClass=ch.qos.logback.core.status.OnConsoleStatusListener -Dlogback.configurationFile=$APB_HOME/etc/logback-cli.xml"
