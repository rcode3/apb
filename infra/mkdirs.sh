source $(dirname $(realpath "$0"))/env.sh
THIS_BUILD=$(dirname $(dirname $(realpath "$0")))
echo "This build determined to be $THIS_BUILD"
mkdir -p $APB_HOME/etc &&
mkdir -p $APB_HOME/logs &&
mkdir -p $APB_HOME/media &&
ln -fn -s $THIS_BUILD $APB_HOME/build &&
ln -fn -s $APB_HOME/build/bin $APB_HOME/bin &&
ln -fn -s $APB_HOME/build/libs $APB_HOME/libs &&
ln -fn -s $APB_HOME/build/webroot $APB_HOME/webroot
if [ ! -f "$APB_HOME/etc/apb_env.sh" ]; then
  cp $THIS_BUILD/bin/example-apb_env.sh $APB_HOME/etc/apb_env.sh
  chmod u+x $APB_HOME/etc/apb_env.sh
fi
if [ ! -f "$APB_HOME/etc/logback.xml" ]; then
  cp $THIS_BUILD/bin/example-logback.xml $APB_HOME/etc/logback.xml
fi
if [ ! -f "$APB_HOME/etc/logback-cli.xml" ]; then
  cp $THIS_BUILD/bin/example-logback-cli.xml $APB_HOME/etc/logback-cli.xml
fi
