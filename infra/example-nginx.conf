
user  nginx;
worker_processes  1;

error_log  /var/log/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;

  upstream backend {
    server ${APB_BE_PROXY_PASS}:8080;
  }

  server {
    listen       80;
    server_name  ${APB_HOST};
    access_log   /var/log/apb.access.log  main;
    client_max_body_size 4M;

    error_page 491 = @static_location;
    error_page 492 = @backend_location;
    error_page 493 = @media_location;

    location @static_location {
      root    /webroot;
      expires 30d;
    }

    location @backend_location {
      proxy_pass      http://backend;
    }

    location @media_location {
      root    /;
      expires 30d;
    }

    location / {
      root    /webroot;
      index __app.html;
      expires 30d;
      try_files $uri $uri/ /__app.html;
    }
    location /images/ {
        return 491;
    }
    location /build/ {
        return 491;
    }

    # objects
    location /o/ {
      return 491;
    }

    # displayable persons
    location /p/ {
      return 491;
    }

    # instance articles
    location /a/ {
      return 491;
    }

    # displayable groups
    location /g/ {
      return 491;
    }

    location /.well-known/ {
      return 492;
    }
    location /api/ {
      return 492;
    }
    location /pub/ {
      return 492;
    }

    location /media/ {
        return 493;
    }

  }
}
