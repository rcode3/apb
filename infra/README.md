# Infrastructure and Admin Scripts
This repository contains infrastructure and admin scripts for APB.

## A Note About Quoting

Because of the nested shell scripts, it may be necessary to quote string with whitespace
using a nested double quoted string inside a single quoted string. For example:

```angular2html
./site_data.sh APB '"APB Rules"'
```

## Listing Postgres Connections

https://dataedo.com/kb/query/postgresql/list-database-sessions

## Host Headers for Local Development

There are browser extensions that do this, but some don't work.

Another solution is to add something like this to `/etc/hosts`:

```angular2html
127.0.1.1	APB.local
```

Where `APB.local` matches the `APB_DEV_HOSTNAME` in `env.sh`


