source $(realpath $(dirname "$0"))/env.sh
docker network inspect $APB_NET >/dev/null 2>&1 || \
	docker network create $APB_NET &&
docker inspect $APB_DATABASE >/dev/null 2>&1 || \
	docker create --name $APB_DATABASE \
	--network=$APB_NET \
	--user $(id -u):$(id -g) \
	$APB_EXPOSED_DB_PORT_OPTION \
	-e "POSTGRES_USER=$APB_PGADMIN" \
	-e "POSTGRES_DB=apb" \
	-e "POSTGRES_PASSWORD=$APB_PGADMINPW" \
	-e "PGDATA=/var/lib/postgresql/data/pgdata" \
	-v $APB_HOME:/var/lib/postgresql/data \
	postgres:13.1 &&
echo "Using nginx configuration in $APB_NGINX_CONF" &&
docker inspect $APB_NGINX >/dev/null 2>&1 || \
  docker create --name $APB_NGINX  \
  --network=$APB_NET \
  -p $APB_NGINX_PORT:80 \
  -v $APB_HOME/webroot:/webroot \
  -v $APB_HOME/media:/media \
  -v $APB_HOME/logs/nginx:/var/log \
  -v $APB_HOME/etc/$APB_NGINX_CONF:/etc/nginx/nginx.conf:ro  \
  nginx &&
docker inspect $APB_BE >/dev/null 2>&1 || \
  docker create --name $APB_BE \
  --network=$APB_NET \
  --user $(id -u):$(id -g) \
  --env-file $APB_HOME/etc/docker-env.list \
  -v $APB_HOME/libs:/apb-libs \
  -v $APB_HOME/logs:/var/log \
  -v $APB_HOME/etc:/apb-etc \
  -v $APB_HOME/media:/media \
  $APB_DOCKER_JRE \
  java -Dlogback.configurationFile=/apb-etc/logback.xml -jar /apb-libs/apb-ktor-fat.jar
