# Change to your host name
APB_HOST=example.com

# Used for signing JWTs. Keep it secret. Change it now!
APB_JWT_SECRET="CHANGE_NOW"

# Change if you want, but leaving as your host name is ok.
APB_JWT_ISSUER=$APB_HOST

# Change to something simple
APB_JWT_REALM="My_APB"
