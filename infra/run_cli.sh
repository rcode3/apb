source $(realpath $(dirname "$0"))/env.sh

function run_cli() {
  local CLASSNAME=$1
  shift
  local CMD="$APB_CLI_PRECMD $APB_CLI_JAVABIN -cp $APB_CLI_JARLOC $CLASSNAME $@"
  echo "Running command $CMD"
  eval "$CMD"
}
