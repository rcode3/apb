# Build

## Overview and Preparation

Building APB uses Docker to avoid complications with dependency conflicts. 
It also allows you to build the code on one machine and then deploy it to
another machine, even if the build machine is x86_64 and the target is arm64. 
Both the build and deploy are based off of the `timbru31/java-node` Docker 
images which are available for both x86_64 and arm64.

If you don't have Docker installed on your build machine, do that now and 
then return to these instructions.

## Preparing

Create a build directory of your choice, and `cd` into it. The example
here uses `$HOME/build/apb`:

```
mkdir -p $HOME/build/apb
cd $HOME/build/apb

```

Download an APB tarball release and untar it. Substitute `<name of tarfile>` with
the file you downloaded.

```
tar -xvzf <name of tarfile> 
```

This will create a directory such as `apb-XXXXX` where XXXX is the commit
number of the code.

Setup references to the downloaded code:

```
<apb-XXXXX>/make/setup_code.sh

```

This will create a `code` symlink to make the next commands easier.


Next, a Docker build image needs to be created. This step is done to avoid 
running the build with root privileges. If you run `docker images` and see `apb-java-node`, then you have a build image
and don't need to create a new one unless it has been a particularly long time since you built it or you just want to 
do it for fun. To create the build image, do the following:

```
code/make/create_docker_image.sh
```

This Docker image comes with a JDK (for building the APB backend) and 
Node (for building the APB frontend). This Docker image is cross platform so you can use it on x86 and ARM systems.

## Building

Now let's use Docker to create a container to build the code.

```
code/make/create_docker_container.sh
```

Now, run the build

```
code/make/run_build_in_docker.sh
```

The above script invokes Docker which calls `code/make/build.sh`. You can
look at it to see what it is doing: `cat code/make/build.sh`.

Once complete, there will be a compressed tar file (`.tgz`) in the `code` directory
starting with the name `apb-build` and with a build ID on it. The build ID consists of the APB version 
(taken from source code), the APB code commit (taken from the tarfile name), and the current timestamp. This build 
artifact has all the code to be installed on the server.


To run the build again, execute the `run_build_in_docker.sh` command.

## Building Subsequent Versions

To build another version, follow these steps.

1. Untar the file
1. Run `setup_code.sh`
1. If it has been awhile since you have build the build image, then run `create_docker_image.sh`
1. Run `create_docker_container.sh`
1. Run `run_build_in_docker.sh`

## Deploying

Instructions on deploying can be found [here]($ROOT/doc/$CURRENT/docs/deploy.md)

## Cleaning Up

If you need to clean things up, you can do the following.

```
docker rm apb-build
docker rmi apb-java-node
rm code
rm -rf apb-*
```
