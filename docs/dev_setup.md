# Dev Setup

Setting up an environment for development use is a bit different from setting up an environment for deployment use.
Mostly, the developer doesn't want to develop code (Kotlin, Typescript, or bash) in a Docker container because the
round-trips necessary for iterative development and testing are too onerous. Therefore, a split setup is needed,
where development of code occurs locally on the host computer but docker can still be used for Nginx and Postgres.

In this split setup, ktor runs locally (on port 8080) and Typescript/Javascript for the front-end is run locally
in a hot-compiled environment using Spassr (on port 5000). Nginx and Postgres still run in Docker.

## Create a "Deployment" Home

You'll need a directory that somewhat mimics a deployment directory. In general something like `$HOME/apb` works
just fine, but wherever you put it is up to you. Inside the directory create `etc`, `logs`, and `media` subdirectories,
similar as the ones that would appear in a deployment directory:

```shell
mkdir etc
mkdir logs
mkdir media
```

From the code repository, the following files into `etc` and rename them:

* `example-logback-dev.xml` to `logback.xml`
* `example-logback-cli-dev.xml` to `logback-cli.xml`
* `example-nginx-dev.conf` to `nginx.conf`

## Edit Logback Configurations

You can edit the logback configurations to whatever scenario you desire. The simplest thing do to is to use them as-is
and replace the `[YOUR DEPLOY LOG DIR HERE]` occurrences with the `logs` directory in your deployment directory.

## Edit Nginx Configuration

The Nginx configuration for development is different from the one for deployment in that it proxies to Spassr for the
front-end in addition to proxying to ktor for the backend.

Note that you want to replace `[YOUR HOST NAME HERE]` with the value that will be used in your browser to access
the front-end. You can actually have multiple, space separate values if necessary. One of those values must match
what will appear in the `Host` header in HTTP.

Additionally, you want to replace `[YOUR DEV HOST IP HERE]` with the IP address of you dev host. This is required because
Nginx needs to reach out beyond the Docker bridged network.

## Setup a Local APB_ENV

Copy the `infra/example-apb-env-dev.sh` file to `~/.apb_env.sh`. This will create a local, custom version of the
environment settings.

Be sure to edit the file, using the guidance in the comments of the file.

## Create Nginx and Postgres Containers

To create the Nginx and Postgres containers, use the same command as in a deployment:

```shell
cd <CODE>
infra/create_docker_containers.sh
```

The APB_BE container may fail to be created, but that's a good thing. You can run `docker ps` to see if the Nginx
and Postgres containers were created.

## Start Postgres and Set the Schema

```shell
docker start apb-database
cd <CODE>
infra/db_migrate.sh
```

You should see messages about the schema migration.

## Start Ktor

```shell
cd <CODE>/be
./run_ktor.sh
```

This should connect to the database and start listening on 8080.

## Start Spassr for Svelte Development

```shell
cd <CODE>/svelte
npm run dev
```

## Start Nginx

```shell
docker start apb-nginx
```

## Profit!!