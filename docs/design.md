# Some Design and Architecture Ideas

## General Concepts

APB users can have many "personas", where a persona maps to an ActivityPub person object.
Users have login credentials.

APB persons can be members of many groups. Note, persons are members of groups. Users are owners of persons.

Groups and persons can publish notes and articles.

Notes have identifiers picked by APB using the timestamp of creation.

Articles have identifiers that are related to the title of the Article. Article's must have a title.
Articles can also be global to the instance of APB and not specific to a user or person.

## PreferredUsername

When picking a preferredUsername for an actor (group or person), the user must specific a
PUN that is one word (no spaces) and contains no URL subdelimiters (as specified by RFC 3896)
which are `!$&'()*+,;=`. Subdelimiters are used elsewhere in the system.

For non-person actor types, the system will append a signifier in the form of `*g` for groups.
Future actor types such as system or application maybe `*s` or `*g`.

## URLs

(** where Alice is the preferredUsername)

### UI URLs

* /o - remote objects (because identifiers of other systems won't neatly fit into this)
* /a - local articles (that are global to the instance)
* /p - local persons (aka users) actor on the system (`/p` is reserved for pages in the future)
  * /n notes of person
  * /a articles of person (such as /u/alice/a/about_me)
* /g - local groups (actor) on the system
  * same as /u
  
### ActivityPub URLs (ActivityPub Identifiers)

* /pub
  * /p/alice - persons -- these are shortened because IDs do get exposed from time to time
  * /g/bikerclub - groups -- these are shortened because IDs do get exposed from time to time
  * /note/alice_year-month-dayofmonth-milliofday (note _ separates pun from date while - is in the date)
  * /article
    * for instance articles /article/something-about-the-title
    * for person or group articles /article/alice_something-about-the-title (note _ separates pun from title whereas - is used to separate words in title)
    
### URL Mapping

The UI will need to differentiate between activity pub URLs that are local and remote to map
between UI URLs with /o (for remote object types) and the "friendly" local URLs. This can be done 
by comparing the beginning of the activity pub URL identifier, and the parsing the remainder.

For example with host apb.example.com, if the URL begins with `https://apb.example.com/pub/note`
then the UI knows it is a local note and can display using the local URLs.

For determining the person (i.e. preferredUsername) of the note, parsing the remainder of the URL with the `,` character yields
the PUN of the person.

## Webfinger

Lookup of actors via Webfinger unfortunately uses `acct` URLs. Therefore ActivityPub identifiers
must fit into the `acct` scheme. Therefore, APB actor identifiers are `alice@apb.example.com`
or `punks*g@apb.example.com` (a group called "punks").

