## Overall Project To Dos

1. general Sanitizer on input
   * ktor
   * svelte
   * code exists, just need to audit
1. call to renew session in ktor
   * ktor 
   * svelte background session renew
   * code exists, but there are some bug in it
   * issue could be lack of session storage on server reboot. use session storage (https://ktor.io/docs/storages.html)
1. enumerate registration type in db
1. Look into these status things
   * /api/v1/instance
   * /api/statusnet/config.json
   * /.well-known/nodeinfo
1. Upgrade marked (security vulnerability in current version)
   * maybe done?
