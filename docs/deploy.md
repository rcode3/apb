# Deploy

## Overview and Preparation

If the deployment machine does not have Docker, go install it and return to these instructions once complete.

## APB Home Directory

If you have not done so before, create a "home" directory for APB. It doesn't matter where you put the directory or under which user you deploy the software. This is all a matter of preference. However, you should create the directory under the same user that will be running the software.

For the sake of these instructions, `<ABP_HOME>` will be denoted by the directory created to be the home of APB.

## Untar the Build

Download the APB build tar file into `<APB_HOME>`. Then untar it (where `<apb-build-XXXX>` is the tar file you downloaded:

```
cd <APB_HOME>
tar -xvzf <apb-build-XXXX>
```

This should create a directory where the build is with the same timestamp as the tar file. (We'll call this the build directory.)

Next, build the home directory structure by executing the following script (where `<apb-build-XXXX>` is the build code):

```
<apb-build-XXXX>/bin/mkdirs.sh
```

If you type `ls -al` you'll now see some handy symbolic links. The `mkdirs.sh` command will force symlinks to the build directory, so if you run it again with from a different tar installation, be mindful of that.

## Configure APB

The `mkdirs.sh` command will have put an `apb_env.sh` file in the `<APB_HOME>/etc` directory. Edit this file for the specifics of your APB site.

The `mkdirs.sh` command will have also created a `logback.xml` file in the same directory. Edit it if you want to customize the logging of the APB backend (probably unnecessary).

Once you have finished editing those files, run the following command:

```
bin/config_env.sh
```

This command updates configuration files.

## Deploy a System Media Pack

If you want to install a system media pack, download them from [here](/uvlist) and untar them in <APB_HOME>: `tar -xvzf <file>`.

## Start the Docker Containers

To start the Docker containers:

```
bin/start-docker.sh
```

To verify the docker containers started, type `docker ps` and make sure there is `apb-nginx`, `apb-database`, and `apb-be`.

## Stop the Docker Containers

Use this command:

```
bin/stop-docker.sh
```