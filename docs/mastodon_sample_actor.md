# Example Mastodon Actor ActivityPub JSON

```json
{
   "@context" : [
      "https://www.w3.org/ns/activitystreams",
      "https://w3id.org/security/v1",
      {
         "Curve25519Key" : "toot:Curve25519Key",
         "Device" : "toot:Device",
         "Ed25519Key" : "toot:Ed25519Key",
         "Ed25519Signature" : "toot:Ed25519Signature",
         "EncryptedMessage" : "toot:EncryptedMessage",
         "IdentityProof" : "toot:IdentityProof",
         "PropertyValue" : "schema:PropertyValue",
         "alsoKnownAs" : {
            "@id" : "as:alsoKnownAs",
            "@type" : "@id"
         },
         "cipherText" : "toot:cipherText",
         "claim" : {
            "@id" : "toot:claim",
            "@type" : "@id"
         },
         "deviceId" : "toot:deviceId",
         "devices" : {
            "@id" : "toot:devices",
            "@type" : "@id"
         },
         "discoverable" : "toot:discoverable",
         "featured" : {
            "@id" : "toot:featured",
            "@type" : "@id"
         },
         "featuredTags" : {
            "@id" : "toot:featuredTags",
            "@type" : "@id"
         },
         "fingerprintKey" : {
            "@id" : "toot:fingerprintKey",
            "@type" : "@id"
         },
         "focalPoint" : {
            "@container" : "@list",
            "@id" : "toot:focalPoint"
         },
         "identityKey" : {
            "@id" : "toot:identityKey",
            "@type" : "@id"
         },
         "manuallyApprovesFollowers" : "as:manuallyApprovesFollowers",
         "messageFranking" : "toot:messageFranking",
         "messageType" : "toot:messageType",
         "movedTo" : {
            "@id" : "as:movedTo",
            "@type" : "@id"
         },
         "publicKeyBase64" : "toot:publicKeyBase64",
         "schema" : "http://schema.org#",
         "suspended" : "toot:suspended",
         "toot" : "http://joinmastodon.org/ns#",
         "value" : "schema:value"
      }
   ],
   "attachment" : [],
   "devices" : "https://fosstodon.org/users/rcode3/collections/devices",
   "discoverable" : false,
   "endpoints" : {
      "sharedInbox" : "https://fosstodon.org/inbox"
   },
   "featured" : "https://fosstodon.org/users/rcode3/collections/featured",
   "featuredTags" : "https://fosstodon.org/users/rcode3/collections/tags",
   "followers" : "https://fosstodon.org/users/rcode3/followers",
   "following" : "https://fosstodon.org/users/rcode3/following",
   "icon" : {
      "mediaType" : "image/png",
      "type" : "Image",
      "url" : "https://fosstodon.b-cdn.net/accounts/avatars/000/317/625/original/35b6bcca67d12842.png"
   },
   "id" : "https://fosstodon.org/users/rcode3",
   "image" : {
      "mediaType" : "image/jpeg",
      "type" : "Image",
      "url" : "https://fosstodon.b-cdn.net/accounts/headers/000/317/625/original/21617028d8237f84.jpg"
   },
   "inbox" : "https://fosstodon.org/users/rcode3/inbox",
   "manuallyApprovesFollowers" : false,
   "name" : "RCODE3",
   "outbox" : "https://fosstodon.org/users/rcode3/outbox",
   "preferredUsername" : "rcode3",
   "publicKey" : {
      "id" : "https://fosstodon.org/users/rcode3#main-key",
      "owner" : "https://fosstodon.org/users/rcode3",
      "publicKeyPem" : "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxWvsQskS6Y0We0wD6n22\nZLiDive0EyqBFIsgGx/Hp14lEd9FhkY50B5bqF+uqESrvxcp9pycxBFWA3t/dP8P\nC6lIcxLzTl/tIiyHFjXx+BFZ0WiiMKzhLVauaAx63zr3jwTtPKy0/Rfm+MRZ6y0a\nTzF/1PhuAscPRUzwKRFGEN/E7jh8evkMPTtGbXIce5XJkCivJ4Wt3rGZNUwHKo1O\nwvaCzEU6W8IciFO/Bw0h6iGOJJ6r8D0KJ9cIT1pUYTkJgLV9ahPz4u45AeO4tnvP\nR3YdDKM/hiqWmjpJ2m8FYYR3kLpPXshmRtB20ofCirf7dT3/RkY6uTE8pxWmm+oZ\noQIDAQAB\n-----END PUBLIC KEY-----\n"
   },
   "published" : "2021-02-24T00:00:00Z",
   "summary" : "<p>Something clever goes here.</p>",
   "tag" : [],
   "type" : "Person",
   "url" : "https://fosstodon.org/@rcode3"
}
```