# Installing

At present, installing requires you to build the code before deploying it. 
To avoid complications where the deploy machine is constrained, the build 
may take place on another machine even if the it is not the target 
architecture of the deploy machine. Building and deploying uses docker to
minimize dependency issues and allow for targeting x86_64 and arm64 architectures.

Building and deploying has been tested on x86_64 and Raspberry Pi arm64 (RPI4)
using Ubuntu Server 20.04. Ubuntu Server 20.04 is the recommended target OS.
The code should build and deploy on other Linux distributions that run Docker
and are of a recent vintage.

Instructions for building and deploying:

* [Build]($ROOT/doc/$CURRENT/docs/build.md)
* [Deploy]($ROOT/doc/$CURRENT/docs/deploy.md)
