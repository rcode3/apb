***NOTICE*** **This software is no longer under active development and is incomplete.**

Though incomplete and no longer actively developed, this software does show how to combine various open source projects, most notably in the Kotlin back-end (JOOQ, Ktor).

# APB

APB is the ActivityPub (AP) Bulletin Board System (BBS).

APB is intended to be a general purpose ActivityPub system used for information exchange.
In this way, APB is like the 'kitch-sink' approach of the old BBSes. However, APB is a web
based system and not text-based system such as the old BBSes. In other words, it draws inspiration for the old days but with a more modern ActivityPub web approach.

## Directory Layout

* `be` is the ktor backend
* `cypress` are the Cypress UI end-to-end tests
* `infra` are the scripts for installing and operating
* `make` are the scripts for building using Docker (non-dev build scripts)
* `svelte` is the Svelte JS front-end
* `docs` are markdown files containing documentation for the project
