import { LoremIpsum } from "lorem-ipsum";

describe('Create multiple personas', () => {
    const lorem = new LoremIpsum({
        sentencesPerParagraph: {
            max: 8,
            min: 4
        },
        wordsPerSentence: {
            max: 16,
            min: 4
        }
    });

    let personalityCount = 0

    before(() => {
        cy.exec("cd ../infra && ./db_test_setup.sh", {log: true})
        cy.exec("cd ../infra && ./new_user.sh schitzo schitzo@example.com secretpassword", {log: true})
    })

    beforeEach(()=>{
        cy.login("schitzo", "secretpassword")
        cy.contains("Account")
        cy.url().should('include','acct')
        cy.visit("/acct")
    })

    after(() => {
        cy.logout()
    })


    Cypress._.times(25, () => {
        it('Create multiple personas', ()=>{
            cy.get('a[href="/acct/newPersona')
                .should("not.be.disabled")
                .click()
            let personaName = lorem.generateWords(1) + personalityCount
            personalityCount++
            cy.get('#persona_name').type(personaName)
            cy.get('#new_persona_submit').click()

            let displayName = lorem.generateWords(2)
            let summary = lorem.generateSentences(3)
            cy.get('#display_name').type(displayName)
            cy.get('#summary').type(summary)
            cy.get('#name_summary_submit').click()

            cy.url().should('include','acct')
            cy.get('a[href="/acct/editPersona?persona='+personaName+'"')
                .should("not.be.disabled")
                .click()

            cy.get('#display_name').should('have.value', displayName)
            cy.get('#summary').should('have.value', summary)
        })
    })
})
