describe('Login and Logout users of various privilege', () => {
    before(()=>{
        cy.exec("cd ../infra && ./db_test_setup.sh", {log:true})
    })

    it('Login Sysop', () => {
        cy.login("sysop", "secretpassword")

        cy.contains("Account")
        cy.url().should('include','acct')

        cy.get('a[href="/p/sysop"]').should("not.be.disabled")

        cy.get('a[href="/acct/editPersona?persona=sysop"]')
            .should("not.be.disabled")

        cy.logout()
    })

    it('Login Sysmod', () => {
        cy.login("sysmod", "secretpassword")

        cy.contains("Account")
        cy.url().should('include','acct')

        cy.get('a[href="/p/sysmod"]').should("not.be.disabled")

        cy.get('a[href="/acct/editPersona?persona=sysmod"]')
            .should("not.be.disabled")

        cy.logout()
    })

    it('Login Testuser', () => {
        cy.login("testuser", "secretpassword")

        cy.contains("Account")
        cy.url().should('include','acct')

        cy.get('a[href="/p/testuser"]').should("not.be.disabled")

        cy.get('a[href="/acct/editPersona?persona=testuser"]')
            .should("not.be.disabled")

        cy.logout()
    })

    it('Bad login attempt with bad password', ()=> {
        cy.visit('/')
        cy.get('[href="/login"]').filter(':visible').first().click()
        cy.url().should('include','login')

        cy.get("#login_submit")
            .should("be.disabled")

        cy.get("#login_name")
            .type('testuser')
            .should('have.value', 'testuser')

        cy.get("#login_password")
            .type('badpassword')

        cy.get("#login_submit")
            .should("not.be.disabled")

        cy.get("#login_submit")
            .click()

        cy.contains("Login Failed").should("exist")
    })

    it('Bad login attempt with bad username', ()=> {
        cy.visit('/')
        cy.get('[href="/login"]').filter(':visible').first().click()
        cy.url().should('include','login')

        cy.get("#login_submit")
            .should("be.disabled")

        cy.get("#login_name")
            .type('notauser')
            .should('have.value', 'notauser')

        cy.get("#login_password")
            .type('secretpassword')

        cy.get("#login_submit")
            .should("not.be.disabled")

        cy.get("#login_submit")
            .click()

        cy.contains("Login Failed").should("exist")
    })
})