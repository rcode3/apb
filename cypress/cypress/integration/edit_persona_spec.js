describe('Edit persona', () => {
    before(() => {
        cy.exec("cd ../infra && ./db_test_setup.sh", {log: true})
    })

    beforeEach(()=>{
        cy.login("testuser", "secretpassword")
        cy.contains("Account")
        cy.url().should('include','acct')
        cy.visit("/acct")
        cy.get('a[href="/acct/editPersona?persona=testuser"]')
            .should("not.be.disabled")
            .click()
    })

    after(() => {
        cy.logout()
    })

    it('Set Testuser Persona Visibility to Visible', () => {
        cy.get('#visible').check()
        cy.get('#visibility_submit').click()

        cy.url().should('include','acct')
        cy.get('a[href="/acct/editPersona?persona=testuser"]')
            .should("not.be.disabled")
            .click()
        cy.get('#visible').should("be.checked")
    })

    it('Set Testuser Persona Visibility to Invisible', () => {
        cy.get('#invisible').check()
        cy.get('#visibility_submit').click()

        cy.url().should('include','acct')
        cy.get('a[href="/acct/editPersona?persona=testuser"]')
            .should("not.be.disabled")
            .click()
        cy.get('#invisible').should("be.checked")
    })

    it('Set Testuser Persona Name and Summary', ()=>{
        let name = 'Testy McTest User'
        let summary = 'I am a test user. Test me about.'
        cy.get('#display_name').type(name)
        cy.get('#summary').type(summary)
        cy.get('#name_summary_submit').click()

        cy.url().should('include','acct')
        cy.get('a[href="/acct/editPersona?persona=testuser"]')
            .should("not.be.disabled")
            .click()

        cy.get('#display_name').should('have.value', name)
        cy.get('#summary').should('have.value', summary)
    })
})
