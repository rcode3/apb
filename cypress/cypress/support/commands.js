// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
Cypress.Commands.add("login", (username, password) => {
    cy.visit('/')
    cy.get('[href="/login"]').filter(':visible').first().click()
    cy.url().should('include','login')

    cy.get("#login_submit")
        .should("be.disabled")

    cy.get("#login_name")
        .type(username)
        .should('have.value', username)

    cy.get("#login_password")
        .type(password)

    cy.get("#login_submit")
        .should("not.be.disabled")

    cy.get("#login_submit")
        .click()
})

Cypress.Commands.add("logout", () => {
    cy.get('a[href="/acct"]').filter(':visible').first().click()
    // cy.get('span:contains("Logout")').filter(':visible').first().click({force: true})
    cy.get('span[data-cy="logout"]').filter(':visible').first().click()
    cy.url().should('include', 'login')
})

//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
