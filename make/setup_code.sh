CODE_HOME=$(dirname $(dirname $(dirname $(realpath "$0"))))
echo "Code Home determined to be $CODE_HOME"
THIS_CODE=$(dirname $(dirname $(realpath "$0")))
echo "This code determined to be $THIS_CODE"
ln -fn -s $THIS_CODE $CODE_HOME/code
source $THIS_CODE/make/version.sh
echo "Code version determined to be $CODE_VERSION"
COMMIT_DIR=$(basename $THIS_CODE)
CODE_COMMIT=${COMMIT_DIR##apb-}
echo "Code commit determined to be $CODE_COMMIT"
export CODE_COMMIT
export CODE_VERSION
envsubst <$THIS_CODE/make/example-artifact_id.sh >$THIS_CODE/make/artifact_id.sh
chmod +x $THIS_CODE/make/artifact_id.sh
