#!/bin/bash
TIMESTAMP=$(date +"%Y-%m-%d_%H-%M-%S")
source $(dirname $(realpath "$0"))/artifact_id.sh
BUILD_ID=${ARTIFACT_ID}_${TIMESTAMP}
echo "Build Id determined to be $BUILD_ID"
cd /code/svelte &&
npm install &&
npm run build:app &&
cd /code/be &&
./gradlew clean shadowJar &&
cd /code &&
mkdir -p /code/build/bin &&
mkdir -p /code/build/libs &&
mkdir -p /code/build/webroot &&
cp -v /code/infra/* /code/build/bin &&
cp -vr /code/svelte/dist/*  /code/build/webroot &&
cp -v  /code/be/ktor/build/libs/apb-ktor-fat.jar /code/build/libs &&
cp -v  /code/be/cli/build/libs/apb-cli-fat.jar /code/build/libs &&
tar -czvf apb-build-${BUILD_ID}.tgz \
  --transform "s/^code\/build/apb-build-$BUILD_ID/" \
  /code/build/*
