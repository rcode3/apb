FROM timbru31/java-node:11-jdk-14
RUN apt update && apt upgrade -y && apt install -y python-dev make g++
RUN npm cache clean --force
RUN mkdir -p "/.npm"
RUN chown -R 1000:1000 "/.npm"
RUN mkdir -p "/.config"
RUN chown -R 1000:1000 "/.config"
