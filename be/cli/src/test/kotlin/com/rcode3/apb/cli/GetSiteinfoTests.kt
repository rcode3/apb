package com.rcode3.apb.cli

import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.jooq.enums.SiteRegistrationType
import com.rcode3.apb.jooq.tables.pojos.SiteInfo
import org.junit.jupiter.api.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class GetSiteinfoTests {
    @Test
    fun testGetSiteData() {
        val mockSiteDataActions = mock<SiteInfoPersistence> {
            on { fetch() } doReturn SiteInfo(
                shortName = "foo",
                longName = "bar",
                defaultTheme = "none",
                registrationType = SiteRegistrationType.`Closed Registration`
            )
        }
        val getSiteInfoData = GetSiteInfo(mockSiteDataActions)
        getSiteInfoData.main(listOf())
    }
}