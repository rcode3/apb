package com.rcode3.apb.cli

import com.rcode3.apb.core.persistence.SiteInfoPersistence
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock

class SetSiteNameTests {
    @Test
    fun testSetSiteData() {
        val mockSiteDataActions = mock(SiteInfoPersistence::class.java)
        val setSiteInfoData = SetSiteNames(mockSiteDataActions)
        setSiteInfoData.main(listOf("apb", "apb rules"))
    }
}