package com.rcode3.apb.cli

import com.rcode3.apb.core.persistence.UserPersistence
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock

class NewUserTests {
    @Test
    fun testNewUserRegistration() {
        val mockNewUserRegistrationAction = mock(UserPersistence::class.java)
        val newUser = NewUser(mockNewUserRegistrationAction)
        newUser.main(listOf("test", "test@example.com", "password"))
    }
}