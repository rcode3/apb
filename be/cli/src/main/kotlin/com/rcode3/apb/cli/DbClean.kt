package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.rcode3.apb.core.constants.DB_DBPASSWORD
import com.rcode3.apb.core.constants.DB_DBUSER
import com.rcode3.apb.core.constants.DB_JDBCURL
import com.typesafe.config.ConfigFactory
import org.flywaydb.core.Flyway

class DbClean : CliktCommand(help = "Clean database.") {
    override fun run() {
        val config = ConfigFactory.load()
        val flyway: Flyway = Flyway.configure().dataSource(
            config.getString(DB_JDBCURL),
            config.getString(DB_DBUSER),
            config.getString(DB_DBPASSWORD)
        ).load()
        flyway.clean()
    }
}

fun main(args: Array<String>) = DbClean().main(args)
