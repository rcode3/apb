package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.rcode3.apb.core.persistence.SystemRolePersistence
import com.rcode3.apb.core.persistence.UserPersistence
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.jooq.enums.SystemRoleType
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType

class NewUser(
    val userPersistence: UserPersistence = UserPersistence(),
    val systemRolePersistence: SystemRolePersistence = SystemRolePersistence()
) : CliktCommand(help = "Creates a new user") {
    val name: String by argument(help = "user/actor name")
    val email: String by argument(help = "email address")
    val password: String by argument(help = "login password")
    val sysopRole: Boolean by option("-s", help = "SysOp").flag()
    val sysmodRole: Boolean by option("-m", help = "SysMod").flag()
    override fun run() {
        val newUser = NewUserRegistration(
            name = name,
            plainTextPassword = password,
            email = email,
            status = UserPrincipalStatusType.Active
        )
        val user = userPersistence.registerNewUser(newUser)
        println("User $name registered.")
        if (sysmodRole) {
            systemRolePersistence.addToRole(user.first, SystemRoleType.`System Moderator`)
        }
        if (sysopRole) {
            systemRolePersistence.addToRole(user.first, SystemRoleType.`System Operator`)
        }
    }
}

fun main(args: Array<String>) = NewUser().main(args)
