package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rcode3.apb.core.persistence.SiteInfoPersistence


class GetSiteInfo(val siteInfoPersistence: SiteInfoPersistence = SiteInfoPersistence()) : CliktCommand(help = "Get site data.") {
    override fun run() {
        val siteInfoData = siteInfoPersistence.fetch()
        val gson: Gson = GsonBuilder().setPrettyPrinting().create()
        println(gson.toJson(siteInfoData))
    }

}

fun main(args: Array<String>) = GetSiteInfo().main(args)
