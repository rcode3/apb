package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.rcode3.apb.core.dto.SiteNames
import com.rcode3.apb.core.persistence.SiteInfoPersistence


class SetSiteNames(
    val siteInfoPersistence: SiteInfoPersistence = SiteInfoPersistence()
) : CliktCommand(help = "Set site names.") {
    val shortName: String by argument(help = "Site short name")
    val longName: String by argument(help = "Site long name")
    override fun run() {
        val siteNames = SiteNames(shortName = shortName, longName = longName)
        siteNames.validate()
        siteInfoPersistence.updateSiteNames(siteNames)
        println("Site names set.")
    }
}

fun main(args: Array<String>) = SetSiteNames().main(args)
