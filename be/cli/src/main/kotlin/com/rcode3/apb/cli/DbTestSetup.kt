package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.rcode3.apb.core.persistence.SystemRolePersistence
import com.rcode3.apb.core.persistence.UserPersistence
import com.rcode3.apb.core.constants.DB_DBPASSWORD
import com.rcode3.apb.core.constants.DB_DBUSER
import com.rcode3.apb.core.constants.DB_JDBCURL
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.dto.SiteNames
import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.jooq.enums.SystemRoleType
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.typesafe.config.ConfigFactory
import org.flywaydb.core.Flyway

class DbTestSetup(
    val userPersistence: UserPersistence = UserPersistence(),
    val systemRolePersistence: SystemRolePersistence = SystemRolePersistence(),
    val siteInfoPersistence: SiteInfoPersistence = SiteInfoPersistence()
) : CliktCommand(help = "Reset database and put in test data.") {
    override fun run() {
        val config = ConfigFactory.load()
        val flyway: Flyway = Flyway.configure().dataSource(
            config.getString(DB_JDBCURL),
            config.getString(DB_DBUSER),
            config.getString(DB_DBPASSWORD)
        ).load()
        println("Cleaning Database")
        flyway.clean()
        println("Migrating Database")
        flyway.migrate()
        println("Setting example site name")
        val siteNames = SiteNames(shortName = "Test", longName = "A test site.")
        siteInfoPersistence.updateSiteNames(siteNames)
        println("Creating SysOp")
        val sysop = userPersistence.registerNewUser(
            NewUserRegistration(
                name = "sysop",
                plainTextPassword = "secretpassword",
                email = "sysop@example.com",
                status = UserPrincipalStatusType.Active
            )
        )
        systemRolePersistence.addToRole(sysop.first, SystemRoleType.`System Operator`)
        println("Creating SysMod")
        val sysmod = userPersistence.registerNewUser(
            NewUserRegistration(
                name = "sysmod",
                plainTextPassword = "secretpassword",
                email = "sysmod@example.com",
                status = UserPrincipalStatusType.Active
            )
        )
        systemRolePersistence.addToRole(sysmod.first, SystemRoleType.`System Moderator`)
        println("Creating unpriviledged user")
        userPersistence.registerNewUser(
            NewUserRegistration(
                name = "testuser",
                plainTextPassword = "secretpassword",
                email = "testuser@example.com",
                status = UserPrincipalStatusType.Active
            )
        )
    }
}

fun main(args: Array<String>) = DbTestSetup().main(args)
