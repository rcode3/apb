package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.types.enum
import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.jooq.enums.SiteRegistrationType


class ChangeRegistration(val siteInfoPersistence: SiteInfoPersistence = SiteInfoPersistence()) :
    CliktCommand(help = "Enable site registrations.") {
    val registrationType by argument(
        "Registration Type",
        """Values are '"Open Registration"', '"Closed Registration"', or '"Moderated Registration"'"""
    ).enum<SiteRegistrationType>()

    override fun run() {
        siteInfoPersistence.changeRegistration(registrationType)
        println("Registration set to $registrationType")
    }
}

fun main(args: Array<String>) = ChangeRegistration().main(args)
