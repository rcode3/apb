package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.rcode3.apb.core.persistence.DataSchemaPersistence

class SchemaVersion : CliktCommand() {
    override fun run() {
        val schemaVersion = DataSchemaPersistence().fetchFlywaySchema()
        println("Schema Version: $schemaVersion")
    }
}

fun main(args: Array<String>) = SchemaVersion().main(args)