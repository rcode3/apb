package com.rcode3.apb.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigRenderOptions

class KtorConfig : CliktCommand() {
    override fun run() {
        val renderOpts = ConfigRenderOptions.defaults().setOriginComments(false).setComments(false).setJson(false)
        val config = ConfigFactory.load()
        println(config.root().render(renderOpts))
    }
}

fun main(args: Array<String>) = KtorConfig().main(args)