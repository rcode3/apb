/*
 * This file is generated by jOOQ.
 */
package com.rcode3.apb.jooq.tables.pojos


import java.io.Serializable
import java.time.LocalDateTime


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
data class FlywaySchemaHistory(
    val installedRank: Int? = null, 
    val version: String? = null, 
    val description: String? = null, 
    val type: String? = null, 
    val script: String? = null, 
    val checksum: Int? = null, 
    val installedBy: String? = null, 
    val installedOn: LocalDateTime? = null, 
    val executionTime: Int? = null, 
    val success: Boolean? = null
): Serializable {

    override fun toString(): String {
        val sb = StringBuilder("FlywaySchemaHistory (")

        sb.append(installedRank)
        sb.append(", ").append(version)
        sb.append(", ").append(description)
        sb.append(", ").append(type)
        sb.append(", ").append(script)
        sb.append(", ").append(checksum)
        sb.append(", ").append(installedBy)
        sb.append(", ").append(installedOn)
        sb.append(", ").append(executionTime)
        sb.append(", ").append(success)

        sb.append(")")
        return sb.toString()
    }
}
