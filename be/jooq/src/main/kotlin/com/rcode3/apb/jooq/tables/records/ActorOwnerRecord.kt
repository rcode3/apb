/*
 * This file is generated by jOOQ.
 */
package com.rcode3.apb.jooq.tables.records


import com.rcode3.apb.jooq.tables.ActorOwner

import java.time.OffsetDateTime

import org.jooq.Field
import org.jooq.Record2
import org.jooq.Record5
import org.jooq.Row5
import org.jooq.impl.UpdatableRecordImpl


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
open class ActorOwnerRecord() : UpdatableRecordImpl<ActorOwnerRecord>(ActorOwner.ACTOR_OWNER), Record5<String?, String?, Boolean?, OffsetDateTime?, OffsetDateTime?> {

    var actorId: String?
        set(value) = set(0, value)
        get() = get(0) as String?

    var userOwner: String?
        set(value) = set(1, value)
        get() = get(1) as String?

    var defaultActor: Boolean?
        set(value) = set(2, value)
        get() = get(2) as Boolean?

    var createdAt: OffsetDateTime?
        set(value) = set(3, value)
        get() = get(3) as OffsetDateTime?

    var modifiedAt: OffsetDateTime?
        set(value) = set(4, value)
        get() = get(4) as OffsetDateTime?

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    override fun key(): Record2<String?, String?> = super.key() as Record2<String?, String?>

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    override fun fieldsRow(): Row5<String?, String?, Boolean?, OffsetDateTime?, OffsetDateTime?> = super.fieldsRow() as Row5<String?, String?, Boolean?, OffsetDateTime?, OffsetDateTime?>
    override fun valuesRow(): Row5<String?, String?, Boolean?, OffsetDateTime?, OffsetDateTime?> = super.valuesRow() as Row5<String?, String?, Boolean?, OffsetDateTime?, OffsetDateTime?>
    override fun field1(): Field<String?> = ActorOwner.ACTOR_OWNER.ACTOR_ID
    override fun field2(): Field<String?> = ActorOwner.ACTOR_OWNER.USER_OWNER
    override fun field3(): Field<Boolean?> = ActorOwner.ACTOR_OWNER.DEFAULT_ACTOR
    override fun field4(): Field<OffsetDateTime?> = ActorOwner.ACTOR_OWNER.CREATED_AT
    override fun field5(): Field<OffsetDateTime?> = ActorOwner.ACTOR_OWNER.MODIFIED_AT
    override fun component1(): String? = actorId
    override fun component2(): String? = userOwner
    override fun component3(): Boolean? = defaultActor
    override fun component4(): OffsetDateTime? = createdAt
    override fun component5(): OffsetDateTime? = modifiedAt
    override fun value1(): String? = actorId
    override fun value2(): String? = userOwner
    override fun value3(): Boolean? = defaultActor
    override fun value4(): OffsetDateTime? = createdAt
    override fun value5(): OffsetDateTime? = modifiedAt

    override fun value1(value: String?): ActorOwnerRecord {
        this.actorId = value
        return this
    }

    override fun value2(value: String?): ActorOwnerRecord {
        this.userOwner = value
        return this
    }

    override fun value3(value: Boolean?): ActorOwnerRecord {
        this.defaultActor = value
        return this
    }

    override fun value4(value: OffsetDateTime?): ActorOwnerRecord {
        this.createdAt = value
        return this
    }

    override fun value5(value: OffsetDateTime?): ActorOwnerRecord {
        this.modifiedAt = value
        return this
    }

    override fun values(value1: String?, value2: String?, value3: Boolean?, value4: OffsetDateTime?, value5: OffsetDateTime?): ActorOwnerRecord {
        this.value1(value1)
        this.value2(value2)
        this.value3(value3)
        this.value4(value4)
        this.value5(value5)
        return this
    }

    /**
     * Create a detached, initialised ActorOwnerRecord
     */
    constructor(actorId: String? = null, userOwner: String? = null, defaultActor: Boolean? = null, createdAt: OffsetDateTime? = null, modifiedAt: OffsetDateTime? = null): this() {
        this.actorId = actorId
        this.userOwner = userOwner
        this.defaultActor = defaultActor
        this.createdAt = createdAt
        this.modifiedAt = modifiedAt
    }
}
