/*
 * This file is generated by jOOQ.
 */
package com.rcode3.apb.jooq.enums


import com.rcode3.apb.jooq.Public

import org.jooq.Catalog
import org.jooq.EnumType
import org.jooq.Schema


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
enum class PostableType(@get:JvmName("literal") val literal: String) : EnumType {
    Note("Note"),
    Article("Article");
    override fun getCatalog(): Catalog? = schema.catalog
    override fun getSchema(): Schema = Public.PUBLIC
    override fun getName(): String = "postable_type"
    override fun getLiteral(): String = literal
}
