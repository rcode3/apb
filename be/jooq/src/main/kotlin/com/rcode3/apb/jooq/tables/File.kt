/*
 * This file is generated by jOOQ.
 */
package com.rcode3.apb.jooq.tables


import com.rcode3.apb.jooq.Public
import com.rcode3.apb.jooq.indexes.FILE_RESIZE_URL_IDX
import com.rcode3.apb.jooq.indexes.FILE_USAGE_USER_OWNER_IDX
import com.rcode3.apb.jooq.indexes.FILE_USER_OWNER_IDX
import com.rcode3.apb.jooq.indexes.FILE_USER_OWNER_ID_IDX
import com.rcode3.apb.jooq.keys.FILE_PKEY
import com.rcode3.apb.jooq.keys.FILE__FILE_USER_OWNER_FKEY
import com.rcode3.apb.jooq.tables.records.FileRecord

import java.time.OffsetDateTime

import kotlin.collections.List

import org.jooq.Field
import org.jooq.ForeignKey
import org.jooq.Identity
import org.jooq.Index
import org.jooq.Name
import org.jooq.Record
import org.jooq.Row10
import org.jooq.Schema
import org.jooq.Table
import org.jooq.TableField
import org.jooq.TableOptions
import org.jooq.UniqueKey
import org.jooq.impl.DSL
import org.jooq.impl.Internal
import org.jooq.impl.SQLDataType
import org.jooq.impl.TableImpl


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
open class File(
    alias: Name,
    child: Table<out Record>?,
    path: ForeignKey<out Record, FileRecord>?,
    aliased: Table<FileRecord>?,
    parameters: Array<Field<*>?>?
): TableImpl<FileRecord>(
    alias,
    Public.PUBLIC,
    child,
    path,
    aliased,
    parameters,
    DSL.comment(""),
    TableOptions.table()
) {
    companion object {

        /**
         * The reference instance of <code>public.file</code>
         */
        val FILE = File()
    }

    /**
     * The class holding records for this type
     */
    override fun getRecordType(): Class<FileRecord> = FileRecord::class.java

    /**
     * The column <code>public.file.id</code>.
     */
    val ID: TableField<FileRecord, Int?> = createField(DSL.name("id"), SQLDataType.INTEGER.nullable(false).identity(true), this, "")

    /**
     * The column <code>public.file.url</code>.
     */
    val URL: TableField<FileRecord, String?> = createField(DSL.name("url"), SQLDataType.VARCHAR.nullable(false), this, "")

    /**
     * The column <code>public.file.local_path</code>.
     */
    val LOCAL_PATH: TableField<FileRecord, String?> = createField(DSL.name("local_path"), SQLDataType.VARCHAR.nullable(false), this, "")

    /**
     * The column <code>public.file.original_filename</code>.
     */
    val ORIGINAL_FILENAME: TableField<FileRecord, String?> = createField(DSL.name("original_filename"), SQLDataType.VARCHAR, this, "")

    /**
     * The column <code>public.file.usage</code>.
     */
    val USAGE: TableField<FileRecord, String?> = createField(DSL.name("usage"), SQLDataType.VARCHAR, this, "")

    /**
     * The column <code>public.file.content_type</code>.
     */
    val CONTENT_TYPE: TableField<FileRecord, String?> = createField(DSL.name("content_type"), SQLDataType.VARCHAR, this, "")

    /**
     * The column <code>public.file.user_owner</code>.
     */
    val USER_OWNER: TableField<FileRecord, String?> = createField(DSL.name("user_owner"), SQLDataType.VARCHAR, this, "")

    /**
     * The column <code>public.file.resize_url</code>.
     */
    val RESIZE_URL: TableField<FileRecord, String?> = createField(DSL.name("resize_url"), SQLDataType.VARCHAR, this, "")

    /**
     * The column <code>public.file.resize_local_path</code>.
     */
    val RESIZE_LOCAL_PATH: TableField<FileRecord, String?> = createField(DSL.name("resize_local_path"), SQLDataType.VARCHAR, this, "")

    /**
     * The column <code>public.file.created_at</code>.
     */
    val CREATED_AT: TableField<FileRecord, OffsetDateTime?> = createField(DSL.name("created_at"), SQLDataType.TIMESTAMPWITHTIMEZONE(6).nullable(false).defaultValue(DSL.field("now()", SQLDataType.TIMESTAMPWITHTIMEZONE)), this, "")

    private constructor(alias: Name, aliased: Table<FileRecord>?): this(alias, null, null, aliased, null)
    private constructor(alias: Name, aliased: Table<FileRecord>?, parameters: Array<Field<*>?>?): this(alias, null, null, aliased, parameters)

    /**
     * Create an aliased <code>public.file</code> table reference
     */
    constructor(alias: String): this(DSL.name(alias))

    /**
     * Create an aliased <code>public.file</code> table reference
     */
    constructor(alias: Name): this(alias, null)

    /**
     * Create a <code>public.file</code> table reference
     */
    constructor(): this(DSL.name("file"), null)

    constructor(child: Table<out Record>, key: ForeignKey<out Record, FileRecord>): this(Internal.createPathAlias(child, key), child, key, FILE, null)
    override fun getSchema(): Schema = Public.PUBLIC
    override fun getIndexes(): List<Index> = listOf(FILE_RESIZE_URL_IDX, FILE_USAGE_USER_OWNER_IDX, FILE_USER_OWNER_ID_IDX, FILE_USER_OWNER_IDX)
    override fun getIdentity(): Identity<FileRecord, Int?> = super.getIdentity() as Identity<FileRecord, Int?>
    override fun getPrimaryKey(): UniqueKey<FileRecord> = FILE_PKEY
    override fun getKeys(): List<UniqueKey<FileRecord>> = listOf(FILE_PKEY)
    override fun getReferences(): List<ForeignKey<FileRecord, *>> = listOf(FILE__FILE_USER_OWNER_FKEY)
    fun userPrincipal(): UserPrincipal = UserPrincipal(this, FILE__FILE_USER_OWNER_FKEY)
    override fun `as`(alias: String): File = File(DSL.name(alias), this)
    override fun `as`(alias: Name): File = File(alias, this)

    /**
     * Rename this table
     */
    override fun rename(name: String): File = File(DSL.name(name), null)

    /**
     * Rename this table
     */
    override fun rename(name: Name): File = File(name, null)

    // -------------------------------------------------------------------------
    // Row10 type methods
    // -------------------------------------------------------------------------
    override fun fieldsRow(): Row10<Int?, String?, String?, String?, String?, String?, String?, String?, String?, OffsetDateTime?> = super.fieldsRow() as Row10<Int?, String?, String?, String?, String?, String?, String?, String?, String?, OffsetDateTime?>
}
