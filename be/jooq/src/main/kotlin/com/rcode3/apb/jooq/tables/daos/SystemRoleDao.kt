/*
 * This file is generated by jOOQ.
 */
package com.rcode3.apb.jooq.tables.daos


import com.rcode3.apb.jooq.enums.SystemRoleType
import com.rcode3.apb.jooq.tables.SystemRole
import com.rcode3.apb.jooq.tables.records.SystemRoleRecord

import java.time.OffsetDateTime

import kotlin.collections.List

import org.jooq.Configuration
import org.jooq.Record2
import org.jooq.impl.DAOImpl


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
open class SystemRoleDao(configuration: Configuration?) : DAOImpl<SystemRoleRecord, com.rcode3.apb.jooq.tables.pojos.SystemRole, Record2<SystemRoleType, String>>(SystemRole.SYSTEM_ROLE, com.rcode3.apb.jooq.tables.pojos.SystemRole::class.java, configuration) {

    /**
     * Create a new SystemRoleDao without any configuration
     */
    constructor(): this(null)

    override fun getId(o: com.rcode3.apb.jooq.tables.pojos.SystemRole): Record2<SystemRoleType, String>? = compositeKeyRecord(o.roleType, o.member)

    /**
     * Fetch records that have <code>role_type BETWEEN lowerInclusive AND upperInclusive</code>
     */
    fun fetchRangeOfRoleType(lowerInclusive: SystemRoleType?, upperInclusive: SystemRoleType?): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetchRange(SystemRole.SYSTEM_ROLE.ROLE_TYPE, lowerInclusive, upperInclusive)

    /**
     * Fetch records that have <code>role_type IN (values)</code>
     */
    fun fetchByRoleType(vararg values: SystemRoleType): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetch(SystemRole.SYSTEM_ROLE.ROLE_TYPE, *values)

    /**
     * Fetch records that have <code>member BETWEEN lowerInclusive AND upperInclusive</code>
     */
    fun fetchRangeOfMember(lowerInclusive: String?, upperInclusive: String?): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetchRange(SystemRole.SYSTEM_ROLE.MEMBER, lowerInclusive, upperInclusive)

    /**
     * Fetch records that have <code>member IN (values)</code>
     */
    fun fetchByMember(vararg values: String): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetch(SystemRole.SYSTEM_ROLE.MEMBER, *values)

    /**
     * Fetch records that have <code>created_at BETWEEN lowerInclusive AND upperInclusive</code>
     */
    fun fetchRangeOfCreatedAt(lowerInclusive: OffsetDateTime?, upperInclusive: OffsetDateTime?): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetchRange(SystemRole.SYSTEM_ROLE.CREATED_AT, lowerInclusive, upperInclusive)

    /**
     * Fetch records that have <code>created_at IN (values)</code>
     */
    fun fetchByCreatedAt(vararg values: OffsetDateTime): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetch(SystemRole.SYSTEM_ROLE.CREATED_AT, *values)

    /**
     * Fetch records that have <code>modified_at BETWEEN lowerInclusive AND upperInclusive</code>
     */
    fun fetchRangeOfModifiedAt(lowerInclusive: OffsetDateTime?, upperInclusive: OffsetDateTime?): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetchRange(SystemRole.SYSTEM_ROLE.MODIFIED_AT, lowerInclusive, upperInclusive)

    /**
     * Fetch records that have <code>modified_at IN (values)</code>
     */
    fun fetchByModifiedAt(vararg values: OffsetDateTime): List<com.rcode3.apb.jooq.tables.pojos.SystemRole> = fetch(SystemRole.SYSTEM_ROLE.MODIFIED_AT, *values)
}
