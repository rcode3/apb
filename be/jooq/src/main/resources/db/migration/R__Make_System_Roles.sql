create or replace function make_system_roles()
    returns trigger as $$
declare
    user_name varchar;
    user_system_roles system_role_type[];
begin
    if (tg_op = 'DELETE') then
        user_name = OLD.member;
    else
        user_name = NEW.member;
    end if;
    select into user_system_roles array_remove(array_agg(role_type),null)
        from system_role where member=user_name group by member;
    if (user_system_roles IS NULL) then
        user_system_roles := '{}';
    end if;
    update user_principal
    set system_roles=user_system_roles
    where name=user_name;
    return null;
end
$$ language 'plpgsql';