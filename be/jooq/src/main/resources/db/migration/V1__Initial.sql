--
-- Initial implementation for the triggers. Also in the repeatable files.
--
create function update_modified_at_column()
    returns trigger as $$
begin
    NEW.modified_at = now();
return NEW;
end;
$$ language 'plpgsql';

create function make_system_roles()
    returns trigger as $$
begin
end;
$$ language 'plpgsql';

--
-- Users, Passwords, and SystemRoles
--

create type password_type as enum(
    'bcrypt'
);

create type user_principal_status_type as enum (
    'Active',
    'Suspended',
    'Awaiting Approval'
);

create type user_principal_email_verified_type as enum(
    'Verified',
    'Awaiting Verification'
);

create type system_role_type as enum(
    'System Operator',
    'System Moderator'
);

create table user_principal (
    name character varying primary key,
    password varchar,
    password_type password_type,
    last_seen_at timestamptz,
    last_seen_from varchar,
    last_login timestamptz,
    previous_login timestamptz,
    email varchar,
    email_verified user_principal_email_verified_type,
    email_verified_at timestamptz,
    theme varchar,
    status user_principal_status_type default 'Awaiting Approval',
    system_roles system_role_type[] not null default '{}',
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null
);

create index on user_principal(created_at); -- used for pagination

create trigger update_userprincipal_modified_at
    before update on user_principal
    for each row execute procedure update_modified_at_column();


create table system_role (
    role_type system_role_type not null,
    member varchar not null references user_principal(name),
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null,
    primary key(role_type,member)
);

create trigger update_systemrole_modified_at
    before update on system_role
    for each row execute procedure update_modified_at_column();

create trigger make_system_roles
    after update or insert or delete on system_role
    for each row execute procedure make_system_roles();

create index on system_role(role_type);

--
-- System Info
--

create type site_registration_type as enum (
    'Open Registration',
    'Moderated Registration',
    'Closed Registration'
    );

create table site_info (
                          id int primary key, -- should always be 1
                          short_name varchar,
                          long_Name varchar,
                          default_theme varchar,
                          registration_type site_registration_type default 'Closed Registration',
                          created_at timestamptz default now() not null,
                          modified_at timestamptz default now() not null
);

create trigger update_siteinfo_modified_at
    before update on site_info
    for each row execute procedure update_modified_at_column();

insert into site_info
(id, short_name, long_name)
values
(1, 'apb', 'activity pub bbs');

--
-- Actors
--

create type actor_accessibility_status_type as enum (
    'Accessible', -- denotes the actor has been fetched recently (not applicable for local actors)
    'Inaccessible' -- denotes the actor cannot be fetched
    );

create type actor_visibility_type as enum (
    'Visible', -- publicly visible and discoverable
    'Invisible', -- visible only to the user and administrators
    'Banned' -- visible only to the administrators
    );

-- should mirror activity pub actor types
create type actor_type as enum (
    'Group',
    'Person',
    'Service'
    );

create table actor (
                       id varchar primary key,
                       type actor_type not null,
                       acct varchar unique,
                       webfinger_json jsonb,
                       activity_pub_json jsonb,
                       private_key_pem varchar,
                       public_key_pem varchar not null,
                       visibility actor_visibility_type default 'Visible',
                       accessibility_status actor_accessibility_status_type default 'Accessible',
                       last_heard_at timestamptz, -- the last time an actor was heard from (see accessibility status)
                       last_checked_at timestamptz, -- the last time an actor was checked (see accessibility status)
                       created_at timestamptz default now() not null,
                       modified_at timestamptz default now() not null
);

create index on actor(modified_at); -- used for pagination

create trigger update_actor_modified_at
    before update on actor
    for each row execute procedure update_modified_at_column();

create table actor_owner (
                                  actor_id varchar references actor(id) not null,
                                  user_owner varchar references user_principal(name) not null,
                                  default_actor boolean default false,
                                  created_at timestamptz default now() not null,
                                  modified_at timestamptz default now() not null,
                                  primary key (actor_id, user_owner)
);

-- creates a unique index for the default actor allowing multiple false
create unique index on actor_owner(user_owner, default_actor) where (default_actor);

create trigger update_actor_owner_modified_at
    before update on actor_owner
    for each row execute procedure update_modified_at_column();

--
-- Files
--

create table file (
                      id integer generated always as identity,
                      url varchar not null,
                      local_path varchar not null,
                      original_filename varchar,
                      usage varchar,
                      content_type varchar,
                      user_owner varchar references user_principal(name),
                      resize_url varchar,
                      resize_local_path varchar,
                      created_at timestamptz default now() not null,
                      primary key (id)
);

create index on file(usage, user_owner);
create index on file(user_owner);
create index on file(user_owner, id);
create index on file(resize_url);

--
-- Job Queues
--

create type job_type as enum (
    'Make Thumbnail',
    'Resize Actor Icon',
    'Resize Actor Banner',
    'Fetch Remote Icon',
    'Fetch Remote Banner',
    'Send Activity Pub'
    );

create table job_queue(
                         id integer generated always as identity,
                         job_type job_type not null,
                         details jsonb not null,
                         attempts_remaining int default 1 not null,
                         no_sooner_than timestamptz default now() not null,
                         created_at timestamptz default now() not null
);

create index on job_queue(job_type, attempts_remaining, no_sooner_than);

--
-- Postable - activitypub object types that actors can "post"
--

-- these should mirror the ActivityPub object type names
create type postable_type as enum (
    'Note',
    'Article'
);

create table postable (
                      id varchar primary key,
                      type postable_type not null,
                      actor_owner varchar references actor(id) not null,
                      activity_pub_json jsonb,
                      addressed_to varchar[],
                      addressed_bto varchar[],
                      addressed_cc varchar[],
                      addressed_bcc varchar[],
                      created_at timestamptz default now() not null,
                      modified_at timestamptz default now() not null
);

create index on postable(modified_at ); -- used for pagination
create index on postable(modified_at, actor_owner); -- used for pagination
create index on postable(modified_at, actor_owner, type); -- used for pagination
create index on postable using gin(addressed_to);
create index on postable using gin(addressed_bto);
create index on postable using gin(addressed_cc);
create index on postable using gin(addressed_bcc);

create trigger update_postable_modified_at
    before update on postable
    for each row execute procedure update_modified_at_column();
