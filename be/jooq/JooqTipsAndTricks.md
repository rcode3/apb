# Return JSON for specific columns

```kotlin
fun checkPassword(name: String, password: String, lastSeenFrom: String) : String? {
    val canonicalizedName = canonicalizedUserName(name)
    val result = appContext.dsf.jooqDsl()
        .update(USERPRINCIPAL)
        .set(USERPRINCIPAL.LAST_SEEN_FROM, lastSeenFrom)
        .set(USERPRINCIPAL.LAST_SEEN_AT, currentOffsetDateTime())
        .set(USERPRINCIPAL.LAST_LOGIN, currentOffsetDateTime())
        .set(USERPRINCIPAL.PREVIOUS_LOGIN,
            select(USERPRINCIPAL.LAST_LOGIN)
                .from(USERPRINCIPAL)
                .where(USERPRINCIPAL.NAME.eq(canonicalizedName))
        )
        .where(USERPRINCIPAL.NAME.eq(canonicalizedName))
        .returningResult(
            USERPRINCIPAL.PASSWORD,
            jsonObject(
                key("name").value(USERPRINCIPAL.NAME),
                key("theme").value(USERPRINCIPAL.THEME)
            ),
            USERPRINCIPAL.STATUS
        )
        .fetchOne()
    if (result != null) {
        println("result is $result")
        val cryptedPassword = result[USERPRINCIPAL.PASSWORD]
        if ( BCrypt.checkpw(password.trim(), cryptedPassword) && result[USERPRINCIPAL.STATUS] == Userprincipalstatustype.Active ) {
            return result.value2().toString()
        }
    }
    return null
}
```
