## From Flyway Website

```angular2html
gradle flywayMigrate -Dflyway.url=... -Dflyway.user=... -Dflyway.password=...
```

However, this should work if the URL, user, pw are in `build.gradle`:

```angular2html
gradle flywayMigrate
```

### T9t JSONB Kotlin DSL

Info [here](https://github.com/t9t/jooq-postgresql-json)
