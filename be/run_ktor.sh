source ../infra/env.sh

./gradlew ktor:run -Dlogback.statusListenerClass=ch.qos.logback.core.status.OnConsoleStatusListener -Dlogback.configurationFile=$APB_HOME/etc/logback.xml