package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.actions.CreateActivityPubPost
import com.rcode3.apb.core.persistence.NotePersistence
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.NOTES_SEGMENT
import com.rcode3.apb.core.dto.NotePost
import com.rcode3.apb.ktor.WebUser
import com.rcode3.apb.ktor.respondBadRequest
import com.rcode3.apb.ktor.respondForbidden
import com.rcode3.apb.ktor.respondJson
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.request.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.SerializationException

fun Application.noteModule(
    notes: NotePersistence = NotePersistence(),
    create: CreateActivityPubPost = CreateActivityPubPost()
) {
    val cookieName = attributes[COOKIE_ATTRIBUTE]

    routing {

        route("/$API_SEGMENT/$NOTES_SEGMENT") {

            //only for authenticated users
            authenticate(JWT_AUTH, cookieName, optional = false) {
                post {
                    val webUser = call.authentication.principal<WebUser>()
                    try {
                        val noteObject = call.receive<NotePost>()
                        val notePostResponse = withContext(Dispatchers.IO) {
                            create.execute(noteObject, webUser!!.name, notes.createNote)
                        }
                        if (notePostResponse != null) {
                            call.respondJson(notePostResponse)
                        } else {
                            call.respondForbidden("${webUser?.name} cannot post as ${noteObject.actorId}")
                        }
                    } catch (missing: SerializationException) {
                        call.respondBadRequest(missing.message!!)
                    }
                }
            }
        }
    }
}