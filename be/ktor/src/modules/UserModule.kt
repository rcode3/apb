package com.rcode3.apb.ktor.modules

import com.lectra.koson.obj
import com.lectra.koson.rawJson
import com.rcode3.apb.core.persistence.UserPersistence
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.LOGIN_SEGMENT
import com.rcode3.apb.core.constants.USER_SEGMENT
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.dto.singleTokenRegex
import com.rcode3.apb.core.dto.validateAndThrowOnError
import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.SiteRegistrationType
import com.rcode3.apb.ktor.*
import io.konform.validation.Validation
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

@Serializable
data class UserRegistrationResult(val result: Boolean = false)

@Serializable
data class UserPasswordLogin(val name: String, val password: String) {
    fun validate() = Validation<UserPasswordLogin> {
        UserPasswordLogin::name required {
            minLength(1)
            pattern(singleTokenRegex)
        }
        UserPasswordLogin::password required {
            minLength(1)
            pattern(singleTokenRegex)
        }
    }.validateAndThrowOnError(this)
}

@Serializable
data class UserTheme(val theme: String)

const val USER = "user"
const val SESSION_EXPIRY = "sessionExpiry"

const val AUTH_TOKEN = "authToken"

fun Application.userModule(
    action: UserPersistence = UserPersistence(),
    siteInfoPersistence: SiteInfoPersistence = SiteInfoPersistence()
) {
    val cookieName = attributes[COOKIE_ATTRIBUTE]
    routing {
        route("/$API_SEGMENT/$LOGIN_SEGMENT") {
            get("/available/{name}") {
                val name = call.parameters["name"] ?: return@get call.respondBadRequest("Missing or malformed name")
                val result = withContext(Dispatchers.IO) { action.checkUserNameAvailability(Username(name)) }
                call.respond(UserRegistrationResult(result))
            }

            post("/register") {
                val siteInfo = siteInfoPersistence.fetch()!!
                if (siteInfo.registrationType == SiteRegistrationType.`Closed Registration`) {
                    call.respondText("Registration is closed", status = HttpStatusCode.Unauthorized)
                } else {
                    log.info("registering new user")
                    try {
                        val newUser = call.receive<NewUserRegistration>()
                        newUser.validate()
                        withContext(Dispatchers.IO) {
                            action.registerNewUser(newUser)
                        }
                        call.respond(UserRegistrationResult(true))
                    } catch (ex: IllegalArgumentException) {
                        call.respondBadRequest(ex.localizedMessage)
                    }
                }
            }

            post("/user") {
                val lastSeenFrom = call.request.origin.remoteHost
                val userCreds = call.receive<UserPasswordLogin>()
                try {
                    userCreds.validate()
                    val result = withContext(Dispatchers.IO) {
                        action.checkPassword(Username(userCreds.name), userCreds.password, lastSeenFrom)
                    }
                    val json = if (result != null) {
                        val (authToken, expiration) = JwtConfig().makeToken(userCreds.name)
                        val expiresAt = toIsoOffsetDateTimeString(expiration)
                        call.sessions.set(cookieName, AuthSession(authToken))
                        obj {
                            RESULT to true
                            USER to rawJson(result)
                            SESSION_EXPIRY to expiresAt
                        }
                    } else {
                        FALSE_RESULT
                    }
                    call.respondJson(json)
                } catch (ex: IllegalArgumentException) {
                    call.respondBadRequest(ex.localizedMessage)
                }
            }

            authenticate(JWT_AUTH, cookieName, optional = false) {
                get("/authToken") {
                    val webUser = call.authentication.principal<WebUser>()
                        ?: return@get call.respondBadRequest("You are unauthenticated and cannot get a temporary token")
                    val (authToken, _) = JwtConfig().makeToken(webUser.name.toString())
                    call.respondText(authToken)
                }

                get("/authTokenInJson") {
                    val webUser = call.authentication.principal<WebUser>()
                        ?: return@get call.respondBadRequest("You are unauthenticated and cannot get a temporary token")
                    val (authToken, _) = JwtConfig().makeToken(webUser.name.toString())
                    call.respondJson(obj {
                        AUTH_TOKEN to authToken
                    })
                }

                get("/logout") {
                    val webUser = call.authentication.principal<WebUser>()
                    log.info("Logging out ${webUser!!.name}")
                    call.sessions.clear(cookieName)
                    call.respondJson(TRUE_RESULT)
                }

                get("/refresh") {
                    val webUser = call.authentication.principal<WebUser>()
                    val (authToken, expiration) = JwtConfig().makeToken(webUser!!.name.toString())
                    val expiresAt = toIsoOffsetDateTimeString(expiration)
                    call.sessions.set(cookieName, AuthSession(authToken))
                    call.respondJson(obj {
                        SESSION_EXPIRY to expiresAt
                    })
                }
            }
        }

        route("/$API_SEGMENT/$USER_SEGMENT") {
            authenticate(JWT_AUTH, cookieName, optional = false) {
                post("/theme") {
                    val theme = call.receive<UserTheme>()
                    val webUser = call.authentication.principal<WebUser>()
                    withContext(Dispatchers.IO) {
                        action.setUserTheme(webUser!!.name, theme.theme)
                    }
                    call.respondJson(TRUE_RESULT)
                }

                get("/info") {
                    val webUser = call.authentication.principal<WebUser>()
                    val result = withContext(Dispatchers.IO) {
                        action.getUserInfo(webUser!!.name)
                    }
                    call.respondJson(result!!)
                }
            }
        }
    }
}

fun toIsoOffsetDateTimeString(expiration: Date): String {
    val instant = expiration.toInstant()
    val offset = ZoneId.systemDefault().rules.getOffset(instant)
    return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(instant.atOffset(offset))
}

