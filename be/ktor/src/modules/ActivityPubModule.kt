package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.app.apBaseUrl
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.jooq.enums.ActorType
import com.rcode3.apb.ktor.ActivityPubService
import com.rcode3.apb.ktor.respondBadRequest
import com.rcode3.apb.ktor.respondJson
import com.rcode3.apb.ktor.respondNotFound
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.pipeline.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

fun Application.activityPubModule(
    apService : ActivityPubService = ActivityPubService()
) {
    val applicationConfig = ConfigFactory.load().resolve()

    // TODO - multi-tenancy
    //  we will need to pass in the host to these methods to get the
    //  actor ID

    routing {
        route(AP_SEGMENT) {
            // person
            route(AP_PERSON_SEGMENT) {
                actorRoutes(apService, ActorType.Person, applicationConfig)
            }
            // group
            route(AP_GROUP_SEGMENT) {
                actorRoutes(apService, ActorType.Group, applicationConfig)
            }
            // service
            route(AP_SERVICE_SEGMENT) {
                actorRoutes(apService, ActorType.Service, applicationConfig)
            }
            // shared inbox
            route(AP_SHARED_INBOX_SEGMENT) {
                accept(ACTIVITY_PUB_CONTENT_TYPE) {
                    post {
                        postInboxCommon(apService)
                    }
                }
                accept(ACTIVITY_STREAMS_TYPE) {
                    post {
                        postInboxCommon(apService)
                    }
                }
            }
        }
    }

}

private fun Route.actorRoutes(
    apService: ActivityPubService,
    actorType: ActorType,
    applicationConfig: Config
) {
    accept(ACTIVITY_PUB_CONTENT_TYPE) {
        getPun(apService, actorType, ACTIVITY_PUB_CONTENT_TYPE)
        postInbox(apService)
        getInbox(apService)
        getOutbox(apService)
    }
    accept(ACTIVITY_STREAMS_TYPE) {
        getPun(apService, actorType, ACTIVITY_STREAMS_TYPE)
        postInbox(apService)
        getInbox(apService)
        getOutbox(apService)
    }
    accept(ContentType.Text.Html) {
        get("{$PREFERRED_USER_NAME}") {
            val pun = call.punParameter() ?: return@get call.respondBadRequest("no person given")
            call.respondRedirect("${apBaseUrl(applicationConfig)}/$PERSON_PROFILE_SEGMENT/$pun", true)
        }
    }
}

private fun Route.getPun(apService: ActivityPubService, actorType: ActorType, contentType: ContentType) {
    get("{$PREFERRED_USER_NAME}") {
        val pun = call.punParameter() ?: return@get call.respondBadRequest("no person given")
        val result = withContext(Dispatchers.IO) {
            apService.getActorByPun(pun, actorType)
        } ?: return@get call.respondNotFound()
        call.respondJson(result, contentType)
    }
}

private fun Route.postInbox(apService: ActivityPubService) {
    post("{$PREFERRED_USER_NAME}/$AP_INBOX_SEGMENT") {
        postInboxCommon(apService)
    }
}

private suspend fun PipelineContext<Unit, ApplicationCall>.postInboxCommon(
    apService: ActivityPubService
) {
    val content = call.receive<String>()
    val result = apService.doPostInbox(
        call.request.headers,
        call.request.local.uri,
        content
    )
    if (result.isSuccess) {
        call.respond(HttpStatusCode.OK)
    } else {
        result.exceptionOrNull()?.message?.let { it1 -> call.respondBadRequest(it1) }
            ?: run { call.respondBadRequest("unknown error from ${result.exceptionOrNull()?.javaClass}") }
    }
}

private fun Route.getInbox(apService: ActivityPubService) {
    get("${PREFERRED_USER_NAME}/$AP_INBOX_SEGMENT") {
        call.respondText("not implemented", ContentType.Text.Plain, HttpStatusCode.NotImplemented)
    }
}

private fun Route.getOutbox(apService: ActivityPubService) {
    get("${PREFERRED_USER_NAME}/$AP_OUTBOX_SEGMENT") {
        call.respondText("not implemented", ContentType.Text.Plain, HttpStatusCode.NotImplemented)
    }
}

private fun ApplicationCall.punParameter() = parameters[PREFERRED_USER_NAME]
