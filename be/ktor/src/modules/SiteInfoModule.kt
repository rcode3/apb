package com.rcode3.apb.ktor.modules

import com.lectra.koson.obj
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.SITE_SEGMENT
import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.ktor.respondJson
import com.rcode3.apb.ktor.respondNotFound
import io.ktor.application.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

fun Application.siteInfoModule(action: SiteInfoPersistence = SiteInfoPersistence()) {
    routing {
        route("/$API_SEGMENT/$SITE_SEGMENT") {
            get("/info") {
                val result = withContext(Dispatchers.IO) {
                    action.fetch()
                } ?: return@get call.respondNotFound()
                call.respondJson(obj {
                    "shortName" to result.shortName
                    "longName" to result.longName
                    "registrationType" to result.registrationType
                    "defaultTheme" to result.defaultTheme
                    "host" to action.appContext.common.apHost
                    "apBaseUrl" to action.appContext.common.apBaseUrl
                    "apNoteBaseUrl" to action.appContext.common.apNoteBaseUrl
                    "apArticleBaseUrl" to action.appContext.common.apArticleBaseUrl
                    "apPersonBaseUrl" to action.appContext.common.apPersonBaseUrl
                })
            }
        }
    }
}

