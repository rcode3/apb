package com.rcode3.apb.ktor.modules

import com.lectra.koson.arr
import com.lectra.koson.obj
import com.lectra.koson.rawJson
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.PERSONAS_SEGMENT
import com.rcode3.apb.core.constants.PREFERRED_USER_NAME
import com.rcode3.apb.core.dto.ActorNameSummary
import com.rcode3.apb.core.dto.NewActor
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.persistence.PersonPersistence
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.ktor.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.request.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.SerializationException

fun Application.personModule(
    personPersistence: PersonPersistence = PersonPersistence()
) {
    val cookieName = attributes[COOKIE_ATTRIBUTE]

    routing {

        // only for authenticated users
        route("/$API_SEGMENT/$PERSONAS_SEGMENT") {
            authenticate(JWT_AUTH, cookieName, optional = false) {

                get {
                    val webUser = call.authentication.principal<WebUser>()
                    val result = withContext(Dispatchers.IO) {
                        personPersistence.fetchAllPersonsByOwner(webUser!!.name)
                    }
                    val response = arr[ result.map { rawJson(it.returnedJson) } ]
                    call.respondJson(response)
                }

                get("/{$PREFERRED_USER_NAME}") {
                    val webUser = call.authentication.principal<WebUser>()
                    val preferredUsername = call.parameters[PREFERRED_USER_NAME]
                        ?: return@get call.respondBadRequest("preferredUsername bad or missing")
                    val result =
                        withContext(Dispatchers.IO) {
                            personPersistence.fetchPersonByPunAndOwner(PreferredUsername(preferredUsername), webUser!!.name)
                        }
                    call.respondJson(result!!.returnedJson)
                }

                get("/{$PREFERRED_USER_NAME}/check") {
                    val preferredUsername = call.parameters[PREFERRED_USER_NAME]
                        ?: return@get call.respondBadRequest("preferredUsername bad or missing")
                    val result = withContext(Dispatchers.IO) {
                        personPersistence.checkPersonaNameAvailability(PreferredUsername(preferredUsername))
                    }
                    call.respondJson(obj {
                        RESULT to result
                    })
                }

                post {
                    val webUser = call.authentication.principal<WebUser>()
                    try {
                        val newPersonActor = call.receive<NewActor>()
                        newPersonActor.validate()
                        withContext(Dispatchers.IO) {
                            personPersistence.createNewPersonActor(newPersonActor, webUser!!.name)
                        }
                        call.respondJson(TRUE_RESULT)
                    } catch (ex: IllegalArgumentException) {
                        call.respondBadRequest(ex.localizedMessage)
                    }
                }

                post("/{$PREFERRED_USER_NAME}/visibility") {
                    val preferredUsername = call.parameters[PREFERRED_USER_NAME]
                        ?: return@post call.respondBadRequest("preferredUsername bad or missing")
                    val webUser = call.authentication.principal<WebUser>()
                    try {
                        val visibility = call.receive<ActorVisibilityType>()
                        withContext(Dispatchers.IO) {
                            personPersistence.setVisibility(PreferredUsername(preferredUsername), webUser!!.name, visibility)
                        }
                        call.respondJson(TRUE_RESULT)
                    } catch (ex: IllegalArgumentException) {
                        call.respondBadRequest(ex.message!!)
                    } catch (missing: SerializationException) {
                        call.respondBadRequest(missing.message!!)
                    }
                }

                post("/{$PREFERRED_USER_NAME}/nameSummary") {
                    val preferredUsername = call.parameters[PREFERRED_USER_NAME]
                        ?: return@post call.respondBadRequest("preferredUsername bad or missing")
                    val webUser = call.authentication.principal<WebUser>()
                    try {
                        val actorNameSummary = call.receive<ActorNameSummary>()
                        withContext(Dispatchers.IO) {
                            personPersistence.setNameSummary(PreferredUsername(preferredUsername), webUser!!.name, actorNameSummary)
                        }
                        call.respondJson(TRUE_RESULT)
                    } catch (missing: SerializationException) {
                        call.respondBadRequest(missing.message!!)
                    }
                }

                post("/{$PREFERRED_USER_NAME}/default") {
                    val preferredUsername = call.parameters[PREFERRED_USER_NAME]
                        ?: return@post call.respondBadRequest("preferredUsername bad or missing")
                    val webUser = call.authentication.principal<WebUser>()
                    try {
                        withContext(Dispatchers.IO) {
                            personPersistence.setAsDefaultActor(PreferredUsername(preferredUsername), webUser!!.name)
                        }
                        call.respondJson(TRUE_RESULT)
                    } catch (missing: SerializationException) {
                        call.respondBadRequest(missing.message!!)
                    }
                }
            }
        }
    }
}

