package com.rcode3.apb.ktor.modules

import com.lectra.koson.obj
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.PREFERRED_USER_NAME
import com.rcode3.apb.core.constants.UPLOAD_SEGMENT
import com.rcode3.apb.core.logic.LocalFileUpload
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.persistence.FilePersistence
import com.rcode3.apb.core.persistence.PersonPersistence
import com.rcode3.apb.core.util.copyToSuspend
import com.rcode3.apb.ktor.WebUser
import com.rcode3.apb.ktor.respondJson
import com.rcode3.apb.ktor.respondNotFound
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

fun Application.uploadModule(
    filePersistence: FilePersistence = FilePersistence(),
    actorPersistence: ActorPersistence = ActorPersistence(),
    localUploads: LocalFileUpload = LocalFileUpload()
) {

    val cookieName = attributes[COOKIE_ATTRIBUTE]

    routing {
        // TODO perhaps remove this if it is unused
        authenticate(JWT_AUTH, cookieName, optional = false) {
            route("/$API_SEGMENT") {
                get("/uploadInfo") {
                    val webUser = call.authentication.principal<WebUser>()
                    // in the future this is likely to be a configurable upload type
                    val uploadInfo = localUploads.getAuthorizedUploadUrl(webUser!!.name.toString())
                    call.respondJson(obj {
                        "type" to uploadInfo.uploadType.name
                        "url" to uploadInfo.uploadUrl
                    })
                }

            }
        }

        generalUploadRoutes(cookieName, filePersistence, localUploads)
        actorUploadRoutes(cookieName, filePersistence, actorPersistence, localUploads)
    }
}

fun Route.generalUploadRoutes(
    cookieName: String,
    filePersistence: FilePersistence,
    localUploads: LocalFileUpload = LocalFileUpload()
) {
    val path = "/$API_SEGMENT/$UPLOAD_SEGMENT/general/{usage?}"
    authenticate(JWT_AUTH, cookieName, optional = false) {
        accept(ContentType.MultiPart.FormData) {
            post(path) {
                val webUser = call.authentication.principal<WebUser>()
                val multipart = call.receiveMultipart()
                val usage = call.parameters["usage"]
                var fileId: Int? = null
                multipart.forEachPart { part ->
                    when (part) {
                        is PartData.FileItem -> {
                            val ext = File(part.originalFileName).extension
                            val dest = localUploads.getLocalFilePath(webUser!!.name.toString(), ext)
                            val file = File(dest.path)
                            part.streamProvider().use { input ->
                                file.outputStream().buffered().use { output ->
                                    input.copyToSuspend(output)
                                }
                            }
                            fileId = withContext(Dispatchers.IO) {
                                filePersistence.addFile(
                                    FilePersistence.General(
                                        url = dest.url,
                                        localPath = dest.path,
                                        owner = webUser.name,
                                        originalFilename = part.originalFileName!!,
                                        usage = usage,
                                        contentType = part.contentType?.toString()
                                    )
                                )
                            }
                        }
                    }
                    part.dispose()
                }
                call.respondText(fileId.toString())
            }
        }
    }

    deleteFileUpload(cookieName, path, filePersistence)
}

fun Route.actorUploadRoutes(
    cookieName: String,
    filePersistence: FilePersistence,
    actorPersistence: ActorPersistence,
    localUploads: LocalFileUpload = LocalFileUpload()
) {
    val path = "/$API_SEGMENT/$UPLOAD_SEGMENT/actor/{sizeAction}/{uploadType}/{preferredUsername}"
    authenticate(JWT_AUTH, cookieName, optional = false) {
        val NORESIZE = "noResize"
        val RESIZE = "resize"
        accept(ContentType.MultiPart.FormData) {
            post(path) {
                val webUser = call.authentication.principal<WebUser>()
                val multipart = call.receiveMultipart()
                val pun = call.parameters[PREFERRED_USER_NAME]
                val sizeAction = call.parameters["sizeAction"]
                val uploadType = call.parameters["uploadType"]
                var fileId: Int? = null
                multipart.forEachPart { part ->
                    when (part) {
                        is PartData.FileItem -> {
                            val ext = File(part.originalFileName).extension
                            val dest = localUploads.getLocalFilePath(webUser!!.name.toString(), ext)
                            val file = File(dest.path)
                            part.streamProvider().use { input ->
                                file.outputStream().buffered().use { output ->
                                    input.copyToSuspend(output)
                                }
                            }
                            fileId = withContext(Dispatchers.IO) {
                                try {
                                    doUploadFileAction(
                                        sizeAction,
                                        RESIZE,
                                        actorPersistence,
                                        pun,
                                        webUser,
                                        uploadType,
                                        NORESIZE,
                                        dest,
                                        part,
                                        filePersistence
                                    )
                                } catch (ex: NotFoundException) {
                                    null
                                }
                            }
                        }
                    }
                    part.dispose()
                }
                if (fileId != null) {
                    call.respondText(fileId.toString())
                } else {
                    call.respondNotFound("preferredUsername $pun is not found")
                }
            }
        }
    }

    deleteFileUpload(cookieName, path, filePersistence)
}

private fun doUploadFileAction(
    sizeAction: String?,
    RESIZE: String,
    actorPersistence: ActorPersistence,
    pun: String?,
    webUser: WebUser,
    uploadType: String?,
    NORESIZE: String,
    dest: LocalFileUpload.LocalFileInfo,
    part: PartData.FileItem,
    filePersistence: FilePersistence
): Int? {
    val actorId = actorPersistence.reconstituteActorId(PreferredUsername(pun!!))
    if (!actorPersistence.isKnownActor(actorId)) {
        throw NotFoundException("unknown preferred user name $pun")
    }
    val actionType: FilePersistence.Collaborator = when (uploadType) {
        "icon" -> {
            if (sizeAction.equals(NORESIZE)) {
                FilePersistence.NoReize(
                    url = dest.url,
                    localPath = dest.path,
                    owner = webUser.name,
                    originalFilename = part.originalFileName!!,
                    usage = pun,
                    contentType = part.contentType?.toString(),
                    actorId = actorId,
                    actorActions = ActorPersistence.Companion::setFixedSizedIcon
                )
            } else {
                FilePersistence.ReizeIcon(
                    url = dest.url,
                    localPath = dest.path,
                    owner = webUser.name,
                    originalFilename = part.originalFileName!!,
                    usage = pun,
                    contentType = part.contentType?.toString(),
                    actorId = actorId
                )
            }
        }
        "banner" -> {
            if (sizeAction.equals(NORESIZE)) {
                FilePersistence.NoReize(
                    url = dest.url,
                    localPath = dest.path,
                    owner = webUser.name,
                    originalFilename = part.originalFileName!!,
                    usage = pun,
                    contentType = part.contentType?.toString(),
                    actorId = actorId,
                    actorActions = PersonPersistence.Companion::setBanner
                )
            } else {
                FilePersistence.ReizeBanner(
                    url = dest.url,
                    localPath = dest.path,
                    owner = webUser.name,
                    originalFilename = part.originalFileName!!,
                    usage = pun,
                    contentType = part.contentType?.toString(),
                    actorId = actorId
                )
            }
        }
        else -> {
            FilePersistence.General(
                url = dest.url,
                localPath = dest.path,
                owner = webUser.name,
                originalFilename = part.originalFileName!!,
                usage = pun,
                contentType = part.contentType?.toString()
            )
        }
    }
    return filePersistence.addFile(
        actionType
    )
}

fun Route.deleteFileUpload(cookieName: String, path: String, filePersistence: FilePersistence) {
    authenticate(JWT_AUTH, cookieName, optional = false) {
        delete(path) {
            val id = call.receive<String>()
            val webUser = call.authentication.principal<WebUser>()
            val result = withContext(Dispatchers.IO) {
                filePersistence.cleanFiles(id.toInt(), webUser!!.name.toString())
            }
            if (result) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
    }
}
