package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.persistence.PostablePersistence
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.logic.PostableId
import com.rcode3.apb.ktor.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

fun Application.postableModule(
    action: PostablePersistence = PostablePersistence()
) {
    val cookieName = attributes[COOKIE_ATTRIBUTE]

    routing {

        route("/$API_SEGMENT/$POSTABLES_SEGMENT") {

            authenticate(JWT_AUTH, cookieName, optional = true) {
                get {
                    val id = call.request.queryParameters[ID]
                        ?: return@get call.respondBadRequest(
                            "Query parameter $ID is expected"
                        )
                    val postable = withContext(Dispatchers.IO) {
                        action.fetchPostable(PostableId(id))
                    } ?: return@get call.respondNotFound()
                    call.respondJson(postable)
                }
            }
        }
    }
}