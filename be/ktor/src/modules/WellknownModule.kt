package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.app.apBaseUrl
import com.rcode3.apb.core.app.apHost
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.ktor.webfingerAcctParts
import com.rcode3.apb.ktor.respondBadRequest
import com.rcode3.apb.ktor.respondNotFound
import com.typesafe.config.ConfigFactory
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

fun Application.wellknownModule(
    actorPersistence: ActorPersistence = ActorPersistence()
) {
    val applicationConfig = ConfigFactory.load().resolve()

    routing {
        route("/$WELL_KNOWN_SEGMENT") {

            route(WEBFINGER_SEGMENT) {
                get {
                    val resource =
                        call.parameters["resource"] ?: return@get call.respondBadRequest("missing 'resource'")
                    val (acct, host) = webfingerAcctParts(resource) ?: return@get call.respondBadRequest("bad 'resource'")
                    if (!apHost(applicationConfig).equals(host)) {
                        return@get call.respondNotFound("$host is not us")
                    }
                    val result = withContext(Dispatchers.IO) {
                        actorPersistence.fetchWebfinger("$acct@$host")
                    } ?: return@get call.respondNotFound()
                    call.respondText(
                        text = result,
                        status = HttpStatusCode.OK,
                        contentType = JRD_CONTENT_TYPE
                    )
                }
            }

            route(HOST_META_SEGMENT) {
                get {
                    val xrd = """
                        <?xml version="1.0" encoding="UTF-8"?>
                        <XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
                            <Link 
                                rel="lrdd" 
                                template="${apBaseUrl(applicationConfig)}/.well-known/webfinger?resource={uri}" 
                                type="application/xrd+xml" />
                        </XRD>
                    """.trimIndent()
                    call.respondText(
                        text = xrd,
                        status = HttpStatusCode.OK,
                        contentType = XRD_CONTENT_TYPE
                    )
                }
            }

        }
    }
}
