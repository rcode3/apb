package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.actions.CreateActivityPubPost
import com.rcode3.apb.core.persistence.ArticlePersistence
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.ARTICLES_SEGMENT
import com.rcode3.apb.core.dto.ArticlePost
import com.rcode3.apb.ktor.WebUser
import com.rcode3.apb.ktor.respondBadRequest
import com.rcode3.apb.ktor.respondForbidden
import com.rcode3.apb.ktor.respondJson
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.request.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jsoup.SerializationException

fun Application.articleModule(
    articles: ArticlePersistence = ArticlePersistence(),
    create: CreateActivityPubPost = CreateActivityPubPost()
) {
    val cookieName = attributes[COOKIE_ATTRIBUTE]

    routing {

        route("/$API_SEGMENT/$ARTICLES_SEGMENT") {

            //only for authenticated users
            authenticate(JWT_AUTH, cookieName, optional = false) {
                post {
                    val webUser = call.authentication.principal<WebUser>()
                    try {
                        val articleObject = call.receive<ArticlePost>()
                        val articlePostResponse = withContext(Dispatchers.IO) {
                            create.execute(articleObject, webUser!!.name, articles.createArticle)
                        }
                        if (articlePostResponse != null) {
                            call.respondJson(articlePostResponse)
                        } else {
                            call.respondForbidden("${webUser?.name} cannot post as ${articleObject.actorId}")
                        }
                    } catch (missing: SerializationException) {
                        call.respondBadRequest(missing.message!!)
                    }
                }
            }
        }
    }
}