package com.rcode3.apb.ktor.modules

import com.auth0.jwt.exceptions.TokenExpiredException
import com.rcode3.apb.core.persistence.UserPersistence
import com.rcode3.apb.core.constants.JWT_VALIDITY
import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.ktor.JwtConfig
import com.rcode3.apb.ktor.WebUser
import com.typesafe.config.ConfigFactory
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.auth.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.serialization.*
import io.ktor.server.engine.*
import io.ktor.sessions.*
import io.ktor.util.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.slf4j.event.Level

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.mainModule(
    userPersistence: UserPersistence = UserPersistence(),
    siteInfoPersistence: SiteInfoPersistence = SiteInfoPersistence()
) {
    val applicationConfig = ConfigFactory.load().resolve()
    val jwtConfig = JwtConfig()
    val cookieName = siteInfoPersistence.fetch()?.shortName!!
    attributes.put(COOKIE_ATTRIBUTE, cookieName)

    //start the job queue
    //just requires getting a reference which can be done with anything that
    //has a reference to appcontext. Yeah, there could be a more direct reference but why?
    siteInfoPersistence.appContext.persistenJobQueue.start()
    siteInfoPersistence.appContext.scheduledJobs.start()

    install(Locations) {
    }

    install(AutoHeadResponse)

    install(CallLogging) {
        level = Level.INFO
//        level = Level.TRACE
        filter { call -> call.request.path().startsWith("/") }
    }

    install(ConditionalHeaders)

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        allowNonSimpleContentTypes = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    install(DataConversion)

    install(DefaultHeaders) {
        header("X-Engine", "Ktor") // will send this header with each response
    }

    install(ForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy
    install(XForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy

    install(Sessions) {
        cookie<AuthSession>(cookieName) {
            cookie.path = "/"
            cookie.httpOnly = true
            cookie.extensions["SameSite"] = "strict"
            cookie.maxAgeInSeconds = applicationConfig.getDuration(JWT_VALIDITY).seconds
        }
    }

    install(Authentication) {
        jwt(JWT_AUTH) {
            verifier(jwtConfig.verifier)
            realm = jwtConfig.realm
            validate {
                val lastSeenFrom = this.request.origin.remoteHost
                it.payload.getClaim(jwtConfig.JWT_CLAIM_NAME).asString()?.let { userName ->
                    val record = userPersistence.isUserPrincipalActive(Username(userName), lastSeenFrom)
                    if (record != null) {
                        WebUser(Username(record.name!!), record.systemRoles!!)
                    } else {
                        null
                    }
                }
            }
        }
        session<AuthSession>(cookieName) {
            validate { session ->
                val lastSeenFrom = this.request.origin.remoteHost
                try {
                    val decodedJWT = jwtConfig.verifier.verify(session.authToken)
                    decodedJWT.getClaim(jwtConfig.JWT_CLAIM_NAME).asString()?.let { userName ->
                        val record = userPersistence.isUserPrincipalActive(Username(userName), lastSeenFrom)
                        if (record != null) {
                            WebUser(Username(record.name!!), record.systemRoles!!)
                        } else {
                            null
                        }
                    }
                } catch (tokenExpired: TokenExpiredException) {
                    log.warn("$lastSeenFrom -- ${tokenExpired.message}")
                    null
                } catch (e: Exception) {
                    null
                }
            }
        }
    }

    install(ContentNegotiation) {
//        gson {
//            setPrettyPrinting()
//        }
        json(
            Json {
                coerceInputValues = true
            }
        )
    }

    install(ShutDownUrl.ApplicationCallFeature) {
        // The URL that will be intercepted (you can also use the application.conf's ktor.deployment.shutdown.url key)
        shutDownUrl = "/ktor/application/shutdown"
        // A function that will be executed to get the exit code of the process
        exitCodeSupplier = { 0 } // ApplicationCall.() -> Int
    }

    val client = HttpClient(CIO) {
        install(HttpTimeout) {
        }
        install(Auth) {
        }
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
        install(Logging) {
            level = LogLevel.HEADERS
        }
        BrowserUserAgent() // install default browser-like user-agent
        // install(UserAgent) { agent = "some user agent" }
    }
    runBlocking {
        // Sample for making a HTTP Client request
        /*
        val message = client.post<JsonSampleClass> {
            url("http://127.0.0.1:8080/path/to/endpoint")
            contentType(ContentType.Application.Json)
            body = JsonSampleClass(hello = "world")
        }
        */
    }

//    install(io.ktor.websocket.WebSockets) {
//        pingPeriod = Duration.ofSeconds(15)
//        timeout = Duration.ofSeconds(15)
//        maxFrameSize = Long.MAX_VALUE
//        masking = false
//    }

//    routing {
//
//        install(StatusPages) {
//            exception<AuthenticationException> { _ ->
//                call.respond(HttpStatusCode.Unauthorized)
//            }
//            exception<AuthorizationException> { _ ->
//                call.respond(HttpStatusCode.Forbidden)
//            }
//
//        }
//
//        webSocket("/myws/echo") {
//            send(Frame.Text("Hi from server"))
//            while (true) {
//                val frame = incoming.receive()
//                if (frame is Frame.Text) {
//                    send(Frame.Text("Client said: " + frame.readText()))
//                }
//            }
//        }
//    }
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

const val JWT_AUTH = "JWT-authentication"
val COOKIE_ATTRIBUTE = AttributeKey<String>("Cookie-Name")

@Serializable
data class AuthSession(
    val authToken: String
)

