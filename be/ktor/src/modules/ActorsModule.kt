package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.app.apHost
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.WebfingerAcct
import com.rcode3.apb.ktor.*
import com.rcode3.apb.core.actions.FetchRemoteActor
import com.rcode3.apb.jooq.enums.ActorType
import com.typesafe.config.ConfigFactory
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

fun Application.actorsModule(
    actorPersistence: ActorPersistence = ActorPersistence(),
    fetchRemoteActor: FetchRemoteActor = FetchRemoteActor()
) {
    val cookieName = attributes[COOKIE_ATTRIBUTE]
    val applicationConfig = ConfigFactory.load().resolve()

    routing {

        // potentially public viewing
        route("/$API_SEGMENT/$ACTORS_SEGMENT") {

            authenticate(JWT_AUTH, cookieName, optional = true) {
                get {
                    try {
                        val actorId = call.request.queryParameters[ACTOR_ID]
                        if (actorId != null) {
                            val webUser = call.webuser()
                            val result = withContext(Dispatchers.IO) {
                                actorPersistence.fetchActorById(ActorId(actorId))
                            }
                            var found = false
                            if (result != null) {
                                found = isActorViewable(result, webUser)
                            }
                            if (found) {
                                call.respondJson(result!!.returnedJson)
                            } else {
                                call.respondNotFound(actorId)
                            }
                        } else {
                            val nextDate = call.queryParameterDate(NEXT_DATE)
                            val limit = call.queryParameterInt(LIMIT, 20) ?: 20
                            val result = withContext(Dispatchers.IO) {
                                actorPersistence.fetchActors(nextDate, limit, ActorType.Person)
                            }
                            call.respondJson(result)
                        }
                    } catch (re: RuntimeException) {
                        call.respondBadRequest(re.message ?: "query parameters are bad")
                    }
                }

            }
        }

        //route to lookup actor by webfinger acct resource
        route("/$API_SEGMENT/$WEBFINGER_SEGMENT") {
            authenticate(JWT_AUTH, cookieName, optional = false) {
                get {
                    val acct = call.request.queryParameters[ACCT]
                        ?: return@get call.respondBadRequest("no webfinger acct url given")
                    val acctParts = webfingerAcctParts(acct)
                        ?: return@get call.respondBadRequest("acct $acct is not a valid acct")
                    val knownActor = withContext(Dispatchers.IO) {
                        actorPersistence.fetchActorByAcct(WebfingerAcct(acct))
                    }
                    if (knownActor == null) {
                        //if the actor is on our system, then return not found
                        if (apHost(applicationConfig).equals(acctParts.second)) {
                            call.respondNotFound("$acct is not found")
                        } else {
                            val remoteActor = fetchRemoteActor.fetchRemoteActor(acctParts)
                            if (remoteActor != null) {
                                call.respondJson(remoteActor.returnedJson)
                            } else {
                                call.respondNotFound("$acct is not found on remote system")
                            }
                        }
                    } else {
                        if (isActorViewable(knownActor, call.webuser())) {
                            call.respondJson(knownActor.returnedJson)
                        } else {
                            call.respondNotFound("$acct is not found")
                        }
                    }
                }
            }
        }
    }
}

