package com.rcode3.apb.ktor

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

suspend fun ApplicationCall.respondBadRequest(message: String) {
    this.application.log.warn("Bad HTTP request due to $message")
    this.respondText(message, status = HttpStatusCode.BadRequest)
}