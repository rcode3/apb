package com.rcode3.apb.ktor

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

suspend fun ApplicationCall.respondForbidden(message: String) {
    this.respondText(message, status = HttpStatusCode.Forbidden)
}