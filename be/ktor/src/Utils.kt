package com.rcode3.apb.ktor

import com.rcode3.apb.core.dto.ActorInfoResponse
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.jooq.enums.SystemRoleType
import io.ktor.application.*
import io.ktor.auth.*
import java.time.OffsetDateTime
import java.time.format.DateTimeParseException

fun isSysMod(webUser: WebUser) = webUser.systemRoles.contains(SystemRoleType.`System Moderator`)
fun isSysOp(webUser: WebUser) = webUser.systemRoles.contains(SystemRoleType.`System Operator`)

fun isActorViewable(actorInfoResponse: ActorInfoResponse, webUser: WebUser?): Boolean {
    if (actorInfoResponse.visibility == ActorVisibilityType.Visible) {
        return true
    } else if (webUser != null) {
        if (isSysOp(webUser) || isSysMod(webUser)) {
            return true
        } else if (actorInfoResponse.owners.contains(webUser.name.toString())) {
            return true
        }
    }
    return false
}

fun ApplicationCall.webuser() = this.authentication.principal<WebUser>()

fun ApplicationCall.queryParameterLong(parameterName: String): Long? {
    val s = this.request.queryParameters[parameterName] ?: return null
    try {
        return s.toLong()
    } catch (e: NumberFormatException) {
        throw RuntimeException("query parameter $parameterName is not a number")
    }
}

fun ApplicationCall.queryParameterInt(parameterName: String, max: Int? = null): Int? {
    val s = this.request.queryParameters[parameterName] ?: return null
    try {
        val i = s.toInt()
        if (max != null && i > max) {
            throw RuntimeException("query parameter $parameterName is greater than the maximum allowable value of $max")
        }
        return i
    } catch (e: NumberFormatException) {
        throw RuntimeException("query parameter $parameterName is not a number")
    }
}

fun ApplicationCall.queryParameterDate(parameterName: String): OffsetDateTime? {
    val s = this.request.queryParameters[parameterName] ?: return null
    try {
        return OffsetDateTime.parse(s)
    } catch (e: DateTimeParseException) {
        throw RuntimeException("query parameter $parameterName is not a datetime value")
    }
}

val acctRegex = """^acct:(?<acct>[^@]+)@(?<host>.*)$""".toRegex()

fun webfingerAcctParts(resource: String): Pair<String, String>? {
    return acctRegex.matchEntire(resource)?.let { match ->
        Pair(match.groups["acct"]!!.value, match.groups["host"]!!.value)
    }
}
