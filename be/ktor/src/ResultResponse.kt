package com.rcode3.apb.ktor

import com.lectra.koson.obj

const val RESULT = "result"

val TRUE_RESULT = obj {
    RESULT to true
}

val FALSE_RESULT = obj {
    RESULT to false
}