package com.rcode3.apb.ktor

import com.lectra.koson.ArrayType
import com.lectra.koson.ObjectType
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

suspend fun ApplicationCall.respondJson(jsonString: String?, contentType: ContentType = ContentType.Application.Json) {
    if (jsonString != null) {
        this.respondText(
            jsonString,
            contentType
        )
    } else {
        this.respond(
            message = "Not found",
            status = HttpStatusCode.NotFound,
        )
    }
}

suspend fun ApplicationCall.respondJson(jsonObject: ObjectType?, contentType: ContentType = ContentType.Application.Json) {
    jsonObject ?: return respondJson(jsonString = null)
    return respondJson(jsonObject.pretty(), contentType)
}

suspend fun ApplicationCall.respondJson(jsonArray: ArrayType?, contentType: ContentType = ContentType.Application.Json) {
    jsonArray ?: return respondJson(jsonString = null)
    return respondJson(jsonArray.pretty(), contentType)
}