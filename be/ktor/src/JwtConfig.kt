package com.rcode3.apb.ktor

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.rcode3.apb.core.constants.JWT_ISSUER
import com.rcode3.apb.core.constants.JWT_REALM
import com.rcode3.apb.core.constants.JWT_SECRET
import com.rcode3.apb.core.constants.JWT_VALIDITY
import com.typesafe.config.ConfigFactory
import java.util.*

class JwtConfig {
    private val applicationConfig = ConfigFactory.load().resolve()
    private val secret = applicationConfig.getString(JWT_SECRET)
    private val issuer = applicationConfig.getString(JWT_ISSUER)
    private val validity = applicationConfig.getDuration(JWT_VALIDITY)
    private val algorithm = Algorithm.HMAC512(secret)

    val realm = applicationConfig.getString(JWT_REALM)

    val verifier: JWTVerifier = JWT
        .require(algorithm)
        .withIssuer(issuer)
        .build()

    val JWT_CLAIM_NAME = "name"

    fun makeToken(userName: String): Pair<String, Date> {
        val expiration = getExpiration()
        val token = JWT.create()
            .withSubject("Authentication")
            .withIssuer(issuer)
            .withClaim(JWT_CLAIM_NAME, userName)
            .withExpiresAt(expiration)
            .sign(algorithm)
        return Pair(token, expiration)
    }

    private fun getExpiration() = Date(System.currentTimeMillis() + validity.toMillis())
}