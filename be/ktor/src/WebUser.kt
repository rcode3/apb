package com.rcode3.apb.ktor

import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.SystemRoleType
import io.ktor.auth.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class WebUser(
    val name: Username,
    @SerialName("systemRoles")
    val systemRoles: Array<SystemRoleType?>
) : Principal