package com.rcode3.apb.ktor

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

suspend fun ApplicationCall.respondNotFound(message: String = "not found") {
    this.respondText(
        message, status = HttpStatusCode.NotFound
    )
}