package com.rcode3.apb.ktor

import com.rcode3.apb.core.actions.FetchRemoteActor
import com.rcode3.apb.core.app.ClientFactory
import com.rcode3.apb.core.app.StandardClientFactory
import com.rcode3.apb.core.jobs.FetchRemoteBannerJob
import com.rcode3.apb.core.jobs.FetchRemoteIconJob
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.logic.parsePostableActivityPub
import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.persistence.PostablePersistence
import com.rcode3.apb.jooq.enums.ActorType
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ActivityPubService(
    val fetchRemoteActor: FetchRemoteActor = FetchRemoteActor(),
    val httpSig: HttpSig = HttpSig(),
    val actors: ActorPersistence = ActorPersistence(),
    val postables: PostablePersistence = PostablePersistence(),
) {
    companion object {
        fun actorIdFromKeyId(keyId: String): ActorId {
            val url = URLBuilder(keyId)
            url.fragment = ""
            return ActorId(url.buildString())
        }
    }

    val getPublicKey: (suspend (String) -> String?) = { keyId ->
        val actorId = actorIdFromKeyId(keyId)
        withContext(Dispatchers.IO) {
            val postingKeyInfo = actors.fetchKeyInfo(actorId)
            postingKeyInfo?.publicKeyPem ?: fetchRemoteActor.fetchRemoteActor(actorId)?.publicKeyPem
        }
    }

    suspend fun doPostInbox(
        headers: Headers,
        path: String,
        content: String
    ): Result<String> {
        val result = httpSig.verifyHttpSignature(
            headers,
            path,
            "post",
            getPublicKey
        )
        if (result.isSuccess) {
            val parsed = parsePostableActivityPub(content)
            withContext(Dispatchers.IO) {
                postables.createRemotePostable(
                    actorId = parsed.owningActor,
                    postableId = parsed.postableId,
                    stringJson = parsed.cleanedJson,
                    type = parsed.postableType,
                    postingAddresses = parsed.postingAddresses
                )
            }
        }
        return result
    }

    fun getActorByPun(pun: String, actorType: ActorType): String? {
        val actorId = actors.reconstituteActorId(PreferredUsername(pun), actorType)
        return actors.fetchActivityPub(actorId)
    }
}