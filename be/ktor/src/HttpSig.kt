package com.rcode3.apb.ktor

import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.util.HTTP_DATE_TIME_FORMATTER
import com.rcode3.apb.core.util.parseRsaPublicKeyPem
import io.ktor.http.*
import mu.KotlinLogging
import java.security.Signature
import java.time.OffsetDateTime
import java.util.*

private val logger = KotlinLogging.logger {}

class HttpSig {
    val HEADER_PARAM_SPLIT_REGX = Regex("\\s*,\\s*")
    val PARAM_VALUE_SPLIT_REGEX = Regex("\\s*=\\s*")
    val WSP_SPLIT_REGEX = Regex("\\s+")

    suspend fun verifyHttpSignature(
        headers: Headers,
        httpPath: String,
        httpMethod: String,
        getPublcKey: suspend (String) -> String?
    ): Result<String> {
        try {
            requiredHeaders(headers, HttpHeaders.Host, HttpHeaders.Date, HttpHeaders.Signature)
            val sigParams = headerValueParams(headers[HttpHeaders.Signature]!!)
            requiredHeaderParams(sigParams, HEADER_PARAM_ALGORITHM, HEADER_PARAM_HEADERS, HEADER_PARAM_KEYID, HEADER_PARAM_SIGNATURE)
            if (!(sigParams[HEADER_PARAM_ALGORITHM].equals("rsa-sha256") || sigParams[HEADER_PARAM_ALGORITHM].equals("hs2019"))) {
                throw RuntimeException("Only 'rsa-sha256' and 'hs2019' algorithms are supported")
            }
            val signedHeaders = sigParams[HEADER_PARAM_HEADERS]!!.split(WSP_SPLIT_REGEX)
            requiredValues(signedHeaders, PSUEDO_HEADER_REQUEST_TARGET, HttpHeaders.Host.lowercase(), HttpHeaders.Date.lowercase())
            val signingDate = OffsetDateTime.from(HTTP_DATE_TIME_FORMATTER.parse(headers[HttpHeaders.Date]))
            val now = OffsetDateTime.now()
            if (signingDate.isBefore(now.minusHours(1)) || signingDate.isAfter(now.plusHours(1))) {
                throw RuntimeException("signature date has drifted over 1 hour")
            }
            val pseudoHeaders = mapOf<String, String>(PSUEDO_HEADER_REQUEST_TARGET to "${httpMethod.lowercase()} $httpPath")
            val tbs = toBeSignedFromHeaders(headers, pseudoHeaders, signedHeaders)
            val signature = Base64.getDecoder().decode(sigParams[HEADER_PARAM_SIGNATURE])

            //all the inexpensive verification has happened, now let's do the expensive public key lookup
            val publicKeyPem = getPublcKey(sigParams[HEADER_PARAM_KEYID]!!)
                ?: throw RuntimeException("no public key available for ${sigParams[HEADER_PARAM_KEYID]}")

            val publicKey = parseRsaPublicKeyPem(publicKeyPem)
            val verifier = Signature.getInstance("SHA256withRSA")
            verifier.initVerify(publicKey)
            verifier.update(tbs.toByteArray())
            val verification = verifier.verify(signature)
            if (verification) {
                return Result.success("Signature verification succeeded.")
            }
            //else
            logger.debug { "public key info: alg ${publicKey.algorithm} format ${publicKey.format}" }
            logger.debug { "tbs $tbs" }
            headers.forEach { s, list ->
                list.forEach {
                    logger.debug { "header $s value $it" }
                }
            }
            logger.debug { "sig header ${headers[HttpHeaders.Signature]}" }
            logger.debug { "host header ${headers[HttpHeaders.Host]}" }
            logger.debug { "date header ${headers[HttpHeaders.Date]}" }
            throw RuntimeException("unable to verify signature")
        } catch (re: RuntimeException) {
            logger.debug { re }
            re.printStackTrace()
            return Result.failure(re)
        }
    }

    fun requiredHeaders(headers: Headers, vararg headerName: String) {
        headerName.forEach { name ->
            if (!headers.contains(name)) {
                throw RuntimeException("$name is a required header.")
            }
        }
    }

    fun headerValueParams(headerValue: String) =
        headerValue
            .split(HEADER_PARAM_SPLIT_REGX)
            .associate {
                val a = it.split(PARAM_VALUE_SPLIT_REGEX, 2)
                val v = a[1].drop(1).dropLast(1)
                Pair(a[0], v)
            }

    fun requiredHeaderParams(params: Map<String, String>, vararg paramName: String) {
        paramName.forEach { name ->
            if (!params.containsKey(name)) {
                throw RuntimeException("$name header value is required")
            }
        }
    }

    fun requiredValues(list: List<String>, vararg value: String) {
        value.forEach {
            if (!list.contains(it)) {
                throw RuntimeException("$it is required")
            }
        }
    }

    fun toBeSignedFromHeaders(headers: Headers, pseudoHeaders: Map<String, String>, signedHeaders: List<String>) =
        signedHeaders.map { "$it: ${headerValue(headers, pseudoHeaders, it)}" }.joinToString("\n")

    fun headerValue(headers: Headers, pseudoHeaders: Map<String,String>, key: String): String {
        val headerName = headerNameFromCanonical(key)
        return headers[headerName] ?: pseudoHeaders[headerName] ?: throw RuntimeException("header value $headerName is not valid")
    }

    fun headerNameFromCanonical(canonical: String): String {
        if (canonical.startsWith('(')) {
            return canonical
        }
        //else
        return canonical.split('-').map { capitalize(it) }.joinToString("-")
    }

    fun capitalize(s: String) = s.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
}
