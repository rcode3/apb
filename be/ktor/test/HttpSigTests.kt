package com.rcode3.apb.ktor

import com.rcode3.apb.core.constants.PSUEDO_HEADER_REQUEST_TARGET
import com.rcode3.apb.core.constants.Signature
import com.rcode3.apb.core.util.HTTP_DATE_TIME_FORMATTER
import com.rcode3.apb.core.util.genRsa2048
import com.rcode3.apb.core.util.parseRsaPrivateKeyPem
import io.kotest.matchers.shouldBe
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.security.Signature
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.*

class HttpSigTests {

    val httpSig = HttpSig()

    @Test
    fun `parse header with parameters`(){
        val m = httpSig.headerValueParams("p1=\"bar\", p2=\"foo\", p3= \"faz\",p4 =\"fuzz\"")
        m["p1"] shouldBe "bar"
        m["p2"] shouldBe "foo"
        m["p3"] shouldBe "faz"
        m["p4"] shouldBe "fuzz"
    }

    @Test
    fun `get real header name from canonical name`() {
        httpSig.headerNameFromCanonical("date") shouldBe "Date"
        httpSig.headerNameFromCanonical("content-type") shouldBe "Content-Type"
        httpSig.headerNameFromCanonical(PSUEDO_HEADER_REQUEST_TARGET) shouldBe PSUEDO_HEADER_REQUEST_TARGET
    }

    @Test
    fun `create a toBeSigned string`() {
        val headers = headersOf(Pair("p1", listOf("foo")), Pair("p2", listOf("bar")), Pair("p3", listOf("faz")))
        val pseudoHeaders = mapOf<String, String>(PSUEDO_HEADER_REQUEST_TARGET to "post /test")
        val tbs = httpSig.toBeSignedFromHeaders(headers, pseudoHeaders, listOf(PSUEDO_HEADER_REQUEST_TARGET, "p1", "p3"))
        tbs shouldBe "${PSUEDO_HEADER_REQUEST_TARGET}: post /test\np1: foo\np3: faz"
    }

    @Test
    fun `verify an http signature`() {
        val nowString = HTTP_DATE_TIME_FORMATTER.format(OffsetDateTime.now(ZoneOffset.UTC))
        val toBeSigned = "(request-target): post /test\nhost: example.com\ndate: $nowString"
        val pemKeyPair = genRsa2048()
        val signer = Signature.getInstance("SHA256withRSA")
        signer.initSign(parseRsaPrivateKeyPem(pemKeyPair.privateKey))
        signer.update(toBeSigned.toByteArray())
        val signature = Base64.getEncoder().encodeToString(signer.sign())
        val headers = headersOf(
            Pair(HttpHeaders.Date, listOf(nowString)),
            Pair(HttpHeaders.Host, listOf("example.com")),
            Pair(HttpHeaders.Signature, listOf("""keyId="foo",algorithm="rsa-sha256",headers="(request-target) host date",signature="$signature""""))
        )
        val getPublicKey: (suspend (String) -> String? ) = { pemKeyPair.publicKey }
        val result = runBlocking { httpSig.verifyHttpSignature(headers, "/test", "post", getPublicKey) }
        result.exceptionOrNull() shouldBe null
        result.isSuccess shouldBe true
    }
}