package com.rcode3.apb.ktor.modules

import com.google.gson.JsonParser
import com.rcode3.apb.core.actions.CreateActivityPubPost
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.ARTICLES_SEGMENT
import com.rcode3.apb.core.dto.ArticlePost
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.persistence.ArticlePersistence
import com.rcode3.apb.ktor.MOCK_SITE_INFO_PERSISTENCE
import com.rcode3.apb.ktor.MOCK_USERS
import com.rcode3.apb.ktor.TEST_ACTOR_ID
import com.rcode3.apb.ktor.loginTestUser
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.any
import org.mockito.kotlin.anyOrNull
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import kotlin.test.assertEquals

class ArticleModuleTests {

    @Test
    fun testPostLocalArticle() {
        val articlePost = ArticlePost(
            actorId = TEST_ACTOR_ID,
            localId = "foo-bar",
            html = "<html>foo</html>",
            fileIds = arrayOf("1", "2"),
            postingAddresses = PostingAddresses(arrayOf(TEST_ACTOR_ID.toString()), arrayOf(), arrayOf(), arrayOf())
        )
        val articleId = "testArticleId"
        val postResponse = """{"actorId":"https://example.com/actor", "id":"$articleId"}"""
        val myMockArticles = mock<ArticlePersistence> {
        }
        val myMockCreate = mock<CreateActivityPubPost> {
            on { execute(any(), Username(anyString()), anyOrNull()) } doReturn postResponse
        }
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            articleModule(myMockArticles, myMockCreate)
        }) {
            cookiesSession {
                loginTestUser()
                handleRequest(HttpMethod.Post, "/$API_SEGMENT/$ARTICLES_SEGMENT") {
                    addHeader("Content-Type", "application/json")
                    addHeader("Accept", "application/json")
                    setBody(Json.encodeToString(articlePost))
                }.apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    val data = JsonParser.parseString(response.content!!).asJsonObject
                    assertEquals(articleId, data["id"].asString)
                }
            }
        }
    }

}