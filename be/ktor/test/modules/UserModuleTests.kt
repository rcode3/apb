package com.rcode3.apb.ktor.modules

import com.google.gson.JsonParser
import com.rcode3.apb.core.persistence.UserPersistence
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.LOGIN_SEGMENT
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.SiteRegistrationType
import com.rcode3.apb.jooq.tables.pojos.SiteInfo
import com.rcode3.apb.ktor.MOCK_SITE_INFO_PERSISTENCE
import com.rcode3.apb.ktor.MOCK_USERS
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.time.OffsetDateTime
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class UserModuleTests {

    @Test
    fun testCheckNameAvailability() {
        val mockAction = mock<UserPersistence> {
            on { checkUserNameAvailability(Username("foo")) } doReturn true
        }

        withTestApplication({
            mainModule(mockAction, MOCK_SITE_INFO_PERSISTENCE) //all the features are installed here so we need to run it
            userModule(mockAction, MOCK_SITE_INFO_PERSISTENCE)
        }) {
            handleRequest(HttpMethod.Get, "/$API_SEGMENT/$LOGIN_SEGMENT/available/foo").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertNotNull(response.content)
                val data = Json.decodeFromString<UserRegistrationResult>(response.content!!)
                assertTrue(data.result)
            }
        }
    }

    @Test
    fun testRegisterNewUser() {

        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
        }) {
            handleRequest(HttpMethod.Post, "/$API_SEGMENT/$LOGIN_SEGMENT/register") {
                addHeader("Content-Type", "application/json")
                addHeader("Accept", "application/json")
                val newUser = NewUserRegistration("test", "test@example.com", "secretpassword")
                setBody(Json.encodeToString(newUser))
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertNotNull(response.content)
                val data = Json.decodeFromString<UserRegistrationResult>(response.content!!)
                assertTrue(data.result)
            }
        }
    }

    @Test
    fun testRegisterNewUserWithClosedRegistration() {
        val myMockSiteInfoActions = mock<SiteInfoPersistence> {
            on { fetch() } doReturn SiteInfo(
                0,
                "test",
                "test long name",
                "testTheme",
                SiteRegistrationType.`Closed Registration`,
                OffsetDateTime.now(),
                OffsetDateTime.now()
            )
        }

        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, myMockSiteInfoActions)
        }) {
            handleRequest(HttpMethod.Post, "/$API_SEGMENT/$LOGIN_SEGMENT/register") {
                addHeader("Content-Type", "application/json")
                addHeader("Accept", "application/json")
                val newUser = NewUserRegistration("test", "test@example.com", "secret")
                setBody(Json.encodeToString(newUser))
            }.apply {
                assertEquals(HttpStatusCode.Unauthorized, response.status())
            }
        }
    }

    @Test
    fun testLoginUser() {
        val mockAction = mock<UserPersistence> {
            on { checkPassword(Username(anyString()), any(), any()) } doReturn """
                {
                    "name": "foo",
                    "theme": "darkmood"
                }
            """.trimIndent()
        }

        withTestApplication({
            mainModule(mockAction, MOCK_SITE_INFO_PERSISTENCE) //all the features are installed here so we need to run it
            userModule(mockAction, MOCK_SITE_INFO_PERSISTENCE)
        }) {
            handleRequest(HttpMethod.Post, "/$API_SEGMENT/$LOGIN_SEGMENT/user") {
                addHeader("Content-Type", "application/json")
                addHeader("Accept", "application/json")
                val login = UserPasswordLogin("foo", "secretpassword")
                setBody(Json.encodeToString(login))
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertNotNull(response.content)
                val data = JsonParser.parseString(response.content!!).asJsonObject
                assertEquals(true, data["result"].asBoolean)
                assertNotNull(data["user"])
                assertEquals("foo", data["user"].asJsonObject["name"].asString)
            }
        }
    }
}