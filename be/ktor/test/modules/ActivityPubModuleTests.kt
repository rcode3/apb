package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.constants.*
import com.rcode3.apb.ktor.ActivityPubService
import com.rcode3.apb.ktor.MOCK_SITE_INFO_PERSISTENCE
import com.rcode3.apb.ktor.MOCK_USERS
import com.rcode3.apb.ktor.TEST_ACTOR_DOC_ALICE
import io.kotest.matchers.shouldBe
import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class ActivityPubModuleTests {

    @Test
    fun `when an actor is fetched with browser content types`() {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            activityPubModule(mockService())
        }) {
            handleRequest(HttpMethod.Get, "/$AP_SEGMENT/$AP_PERSON_SEGMENT/alice") {
                addHeader("Accept", "text/html")
            }.apply {
                response.status() shouldBe HttpStatusCode.MovedPermanently
            }
        }
    }

    @Test
    fun `when an actor is fetched using the activity pub content type`() {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            activityPubModule(mockService())
        }) {
            handleRequest(HttpMethod.Get, "/$AP_SEGMENT/$AP_PERSON_SEGMENT/alice") {
                addHeader("Accept", ACTIVITY_PUB_CONTENT_TYPE.toString())
            }.apply {
                response.status() shouldBe HttpStatusCode.OK
            }
        }
    }

    @Test
    fun `when an actor is fetched using activity streams content type`() {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            activityPubModule(mockService())
        }) {
            handleRequest(HttpMethod.Get, "/$AP_SEGMENT/$AP_PERSON_SEGMENT/alice") {
                addHeader("Accept", ACTIVITY_STREAMS_TYPE.toString())
            }.apply {
                response.status() shouldBe HttpStatusCode.OK
            }
        }
    }

    @Test
    fun `when posting to actors inbox using activity pub content type`() {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            activityPubModule(mockService())
        }) {
            handleRequest(HttpMethod.Post, "/$AP_SEGMENT/$AP_PERSON_SEGMENT/alice/$AP_INBOX_SEGMENT") {
                addHeader("Content-Type", ACTIVITY_PUB_CONTENT_TYPE.toString())
                addHeader("Accept", ACTIVITY_PUB_CONTENT_TYPE.toString())
                setBody(TEST_ACTOR_DOC_ALICE) //technically not the right thing, but the mock does not care
            }.apply {
                response.status() shouldBe HttpStatusCode.OK
            }
        }
    }

    @Test
    fun `when posting to actors inbox using activity streams content type`() {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            activityPubModule(mockService())
        }) {
            handleRequest(HttpMethod.Post, "/$AP_SEGMENT/$AP_PERSON_SEGMENT/alice/$AP_INBOX_SEGMENT") {
                addHeader("Content-Type", ACTIVITY_STREAMS_TYPE.toString())
                addHeader("Accept", ACTIVITY_STREAMS_TYPE.toString())
                setBody(TEST_ACTOR_DOC_ALICE) //technically not the right thing, but the mock does not care
            }.apply {
                response.status() shouldBe HttpStatusCode.OK
            }
        }
    }

    @Test
    fun `when posting to shared inbox using activity pub content type`() {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            activityPubModule(mockService())
        }) {
            handleRequest(HttpMethod.Post, "/$AP_SEGMENT/$AP_SHARED_INBOX_SEGMENT") {
                addHeader("Content-Type", ACTIVITY_PUB_CONTENT_TYPE.toString())
                addHeader("Accept", ACTIVITY_PUB_CONTENT_TYPE.toString())
                setBody(TEST_ACTOR_DOC_ALICE) //technically not the right thing, but the mock does not care
            }.apply {
                response.status() shouldBe HttpStatusCode.OK
            }
        }
    }

    @Test
    fun `when posting to shared inbox using activity streams content type`() {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            activityPubModule(mockService())
        }) {
            handleRequest(HttpMethod.Post, "/$AP_SEGMENT/$AP_SHARED_INBOX_SEGMENT") {
                addHeader("Content-Type", ACTIVITY_STREAMS_TYPE.toString())
                addHeader("Accept", ACTIVITY_STREAMS_TYPE.toString())
                setBody(TEST_ACTOR_DOC_ALICE) //technically not the right thing, but the mock does not care
            }.apply {
                response.status() shouldBe HttpStatusCode.OK
            }
        }
    }

    fun mockService(): ActivityPubService {
        val service = mock<ActivityPubService>() {
            on { getActorByPun(anyString(), any()) } doReturn TEST_ACTOR_DOC_ALICE
            onBlocking { doPostInbox(any(), anyString(), anyString()) } doReturn Result.success("success")
        }
        return service
    }

}