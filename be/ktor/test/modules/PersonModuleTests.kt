package com.rcode3.apb.ktor.modules

import com.google.gson.JsonParser
import com.lectra.koson.obj
import com.rcode3.apb.core.constants.ACTIVITY_PUB_JSON
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.PERSONAS_SEGMENT
import com.rcode3.apb.core.dto.ActorInfoResponse
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.persistence.PersonPersistence
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.ktor.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class PersonModuleTests {

    @Test
    fun `test if persona name is available`() {
        val myMockPersonActions = mock<PersonPersistence> {
            on { checkPersonaNameAvailability(PreferredUsername("foo")) } doReturn true
        }

        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            personModule(myMockPersonActions)
        }) {
            cookiesSession {
                loginTestUser()
                handleRequest(HttpMethod.Get, "/$API_SEGMENT/$PERSONAS_SEGMENT/foo/check").apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    val data = JsonParser.parseString(response.content!!).asJsonObject
                    assertEquals(true, data["result"].asBoolean)
                }
            }
        }
    }

    @Test
    fun `test getting all user's actors`() {
        val myMockPersonPersistence = mock<PersonPersistence> {
            on { fetchAllPersonsByOwner(Username(anyString())) } doReturn listOf(
                ActorInfoResponse(
                    actorId = TEST_ACTOR_ID.toString(),
                    owners = arrayOf(TEST_USER.toString()),
                    visibility = ActorVisibilityType.Visible,
                    returnedJson = obj { ACTIVITY_PUB_JSON to "test" }.pretty(),
                    defaultActor = false
                )
            )
        }

        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            personModule(myMockPersonPersistence)
        }) {
            cookiesSession {
                loginTestUser()
                handleRequest(HttpMethod.Get, "/$API_SEGMENT/$PERSONAS_SEGMENT").apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    println(response.content)
                    val data = JsonParser.parseString(response.content!!).asJsonArray
                    assertEquals("test", data[0].asJsonObject[ACTIVITY_PUB_JSON].asString)
                }
            }
        }
    }
}