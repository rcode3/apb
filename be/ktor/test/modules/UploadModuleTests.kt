package com.rcode3.apb.ktor.modules

import com.rcode3.apb.core.persistence.FilePersistence
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.UPLOAD_SEGMENT
import com.rcode3.apb.core.logic.LocalFileUpload
import com.rcode3.apb.ktor.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.testing.*
import io.ktor.utils.io.streams.*
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import kotlin.io.path.ExperimentalPathApi

@ExperimentalPathApi
class UploadModuleTests {

    val myMockFileActions = mock<FilePersistence> {
        on { addFile(any()) } doReturn 1
        on { cleanFiles(any(), any()) } doReturn true
    }

    val tempFile = kotlin.io.path.createTempFile("uploadRoutesTests", "test")
    val myLocalUploadMock = mock<LocalFileUpload> {
        on { getLocalFilePath(any(), any()) } doReturn
                LocalFileUpload.LocalFileInfo(tempFile.toString(), "https://example.com/$tempFile")
    }

    @Test
    fun testUploadFile() {
        uploadTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/general/")
    }

    @Test
    fun testUploadFileWithUsage() {
        uploadTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/general/test")
    }

    @Test
    fun testUploadFileForActorResize() {
        uploadTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/actor/resize/icon/$TEST_USER")
    }

    @Test
    fun testUploadFileForActorNoResize() {
        uploadTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/actor/noResize/icon/$TEST_USER")
    }

    private fun uploadTestFile(path: String) {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            uploadModule(myMockFileActions, MOCK_ACTORS, myLocalUploadMock)
        }) {
            cookiesSession {
                loginTestUser()
                handleRequest(HttpMethod.Post, path) {
                    val boundary = "-----------CAFEBABEXXXXXXXXXXXXXXXX---"
                    addHeader(
                        "Content-Type",
                        ContentType.MultiPart.FormData.withParameter("boundary", boundary).toString()
                    )
                    addHeader("Accept", ContentType.MultiPart.FormData.toString())
                    val fileItem = PartData.FileItem(
                        { byteArrayOf(1, 2, 3).inputStream().asInput() },
                        {},
                        headersOf(
                            HttpHeaders.ContentDisposition,
                            ContentDisposition.File
                                .withParameter(ContentDisposition.Parameters.Name, "file")
                                .withParameter(ContentDisposition.Parameters.FileName, "file.txt")
                                .toString()
                        )
                    )
                    setBody(boundary, listOf(fileItem))
                }
                    .apply {
                        assertEquals(HttpStatusCode.OK, response.status())
                    }
            }
        }
    }

    @Test
    fun testDeleteUpload() {
        deleteTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/general/")
    }

    @Test
    fun testDeleteUploadWithUsage() {
        deleteTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/general/test")
    }

    @Test
    fun testDeleteUploadForActorResize() {
        deleteTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/actor/resize/icon/$TEST_USER")
    }

    @Test
    fun testDeleteUploadForActorNoResize() {
        deleteTestFile("/$API_SEGMENT/$UPLOAD_SEGMENT/actor/noResize/icon/$TEST_USER")
    }

    private fun deleteTestFile(path: String) {
        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            uploadModule(myMockFileActions, MOCK_ACTORS, myLocalUploadMock)
        }) {
            cookiesSession {
                loginTestUser()
                handleRequest(HttpMethod.Delete, path) {
                    setBody("23")
                }
                    .apply {
                        assertEquals(HttpStatusCode.OK, response.status())
                    }
            }
        }
    }
}