package com.rcode3.apb.ktor.modules

import com.google.gson.JsonParser
import com.lectra.koson.arr
import com.lectra.koson.obj
import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.dto.ActorInfoResponse
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.ktor.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.time.OffsetDateTime

class ActorsModuleTests {

    @Test
    fun `test get actor by actor ID`() {
        val myMockActorActions = mock<ActorPersistence> {
            on { fetchActorById(ActorId(anyString())) } doReturn (
                ActorInfoResponse(
                    actorId = TEST_ACTOR_ID.toString(),
                    owners = arrayOf(TEST_USER.toString()),
                    visibility = ActorVisibilityType.Visible,
                    returnedJson = obj { ACTIVITY_PUB_JSON to "test" }.pretty(),
                    defaultActor = false
                )
            )
        }

        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            actorsModule(actorPersistence = myMockActorActions)
        }) {
            cookiesSession {
                loginTestUser()
                val uri = makeUri("/$API_SEGMENT/$ACTORS_SEGMENT", mapOf(ACTOR_ID to TEST_ACTOR_ID.toString()))
                handleRequest(HttpMethod.Get, uri).apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    val data = JsonParser.parseString(response.content!!).asJsonObject
                    assertEquals("test", data[ACTIVITY_PUB_JSON].asString)
                }
            }
        }
    }

    @Test
    fun `test get actors with pagination`() {
        val myMockActorActions = mock<ActorPersistence> {
            on { fetchActors(any(), any(), any(), any()) } doReturn arr[
                obj { "test1" to 1 },
                obj { "test2" to 2 }
            ].pretty()
        }

        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            actorsModule(actorPersistence = myMockActorActions)
        }) {
            cookiesSession {
                loginTestUser()
                val uri = makeUri("/$API_SEGMENT/$ACTORS_SEGMENT", mapOf(
                    NEXT_DATE to OffsetDateTime.now().toString(),
                    LIMIT to "1"
                ))
                handleRequest(HttpMethod.Get, uri).apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    val data = JsonParser.parseString(response.content!!).asJsonArray
                    assertEquals(1, data[0].asJsonObject["test1"].asInt)
                    assertEquals(2, data[1].asJsonObject["test2"].asInt)
                }
            }
        }
    }
}