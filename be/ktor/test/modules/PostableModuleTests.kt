package com.rcode3.apb.ktor.modules

import com.google.gson.JsonParser
import com.rcode3.apb.core.persistence.PostablePersistence
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.POSTABLES_SEGMENT
import com.rcode3.apb.core.logic.PostableId
import com.rcode3.apb.ktor.MOCK_SITE_INFO_PERSISTENCE
import com.rcode3.apb.ktor.MOCK_USERS
import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class PostableModuleTests {

    @Test
    fun testGetPostable() {
        val id = "bar"
        val fetchResponse = """{"test_json":"test"}"""
        val myMockPostableActions = mock<PostablePersistence> {
            on { fetchPostable(PostableId(id)) } doReturn fetchResponse
        }

        withTestApplication({
            mainModule(
                MOCK_USERS,
                MOCK_SITE_INFO_PERSISTENCE
            ) //all the features are installed here so we need to run it
            userModule(MOCK_USERS, MOCK_SITE_INFO_PERSISTENCE)
            postableModule(myMockPostableActions)
        }) {
            handleRequest(HttpMethod.Get, "/$API_SEGMENT/$POSTABLES_SEGMENT?id=$id")
                .apply {
                    assertEquals(HttpStatusCode.OK, response.status())
                    assertNotNull(response.content)
                    val data = JsonParser.parseString(response.content!!).asJsonObject
                    assertNotNull(data)
                }
        }
    }

}