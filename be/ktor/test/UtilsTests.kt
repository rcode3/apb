package com.rcode3.apb.ktor

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class UtilsTests {

    @Test
    fun `test getting the local parts of a webfinger acct url`() {
        assertEquals(Pair("foo", "example.com"), webfingerAcctParts("acct:foo@example.com"))
        assertEquals(Pair("foo*g", "example.com"), webfingerAcctParts("acct:foo*g@example.com"))
        assertNull(webfingerAcctParts("http://foo.example.com"))
    }
}