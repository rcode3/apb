package com.rcode3.apb.ktor

import com.google.gson.JsonParser
import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.LOGIN_SEGMENT
import com.rcode3.apb.core.dto.ActorInfoResponse
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.ktor.modules.UserPasswordLogin
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.Assert
import java.net.URLEncoder
import java.nio.charset.Charset

val TEST_ACTOR_ID = ActorId("https://example.com/actor")

val TEST_ACTOR_DOC_ALICE = """
        {
        	"@context": [
        		"https://www.w3.org/ns/activitystreams",
        		"https://w3id.org/security/v1"
        	],

        	"id": "$TEST_ACTOR_ID",
        	"type": "Person",
        	"preferredUsername": "alice",
        	"inbox": "https://my-example.com/inbox",

        	"publicKey": {
        		"id": "https://my-example.com/actor#main-key",
        		"owner": "https://my-example.com/actor",
        		"publicKeyPem": "-----BEGIN PUBLIC KEY-----...-----END PUBLIC KEY-----"
        	}
        }
    """.trimIndent()

val TEST_USER = Username("testuser")
val TEST_PASSWORD = "testpassword"
val TEST_JIG = "testjig"
val TEST_THEME = "testtheme"
val TEST_LAST_SEEN_FROM = "20210611T12:07:00Z"

val TEST_ACTOR_OWNER_RESPONSE = ActorInfoResponse(
    visibility = ActorVisibilityType.Visible,
    returnedJson = TEST_ACTOR_DOC_ALICE,
    actorId = TEST_ACTOR_ID.toString(),
    owners = arrayOf(TEST_USER.toString()),
    defaultActor = false
)


fun TestApplicationEngine.loginTestUser() {
    handleRequest(HttpMethod.Post, "/$API_SEGMENT/$LOGIN_SEGMENT/user") {
        addHeader("Content-Type", "application/json")
        addHeader("Accept", "application/json")
        val login = UserPasswordLogin(TEST_USER.toString(), TEST_PASSWORD)
        setBody(Json.encodeToString(login))
    }.apply {
        Assert.assertEquals(HttpStatusCode.OK, response.status())
        Assert.assertNotNull(response.content)
        val data = JsonParser.parseString(response.content!!).asJsonObject
        Assert.assertEquals(true, data["result"].asBoolean)
        Assert.assertNotNull(data["user"])
        Assert.assertEquals(TEST_USER.toString(), data["user"].asJsonObject["name"].asString)
    }
}

fun makeUri(path: String, queryParams: Map<String, String>): String {
    val uri = StringBuilder(path)
    uri.append('?')
    var appendAmpersand = false
    queryParams.forEach { (k, v) ->
        if (appendAmpersand) {
            uri.append('&')
        }
        uri.append("$k=")
        uri.append(URLEncoder.encode(v, Charset.defaultCharset()))
        appendAmpersand = true
    }
    return uri.toString()
}