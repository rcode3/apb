package com.rcode3.apb.ktor

import com.rcode3.apb.core.actions.FetchRemoteActor
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.core.dto.PostingKeyInfo
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.ParsedActor
import com.rcode3.apb.core.logic.PostableId
import com.rcode3.apb.core.logic.postableJsonString
import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.persistence.PostablePersistence
import com.rcode3.apb.jooq.enums.ActorType
import com.rcode3.apb.jooq.enums.PostableType
import com.rcode3.apb.jooq.tables.pojos.File
import com.rcode3.apb.ktor.ActivityPubService.Companion.actorIdFromKeyId
import io.kotest.matchers.shouldBe
import io.ktor.http.*
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Test

class ActivityPubServiceTests {
    val actor1 = "https://example.com/actor1"
    val actor2 = "https://example.com/actor2"
    val postingAddresses = PostingAddresses(
        to = arrayOf(actor1), cc = arrayOf(actor2), bto = arrayOf(), bcc = arrayOf())
    val attributedTo = ActorId("https://example.com/actor3")
    val postableId = PostableId("https://example.xyz")
    val file1 = File(originalFilename = "foo.txt", url = "https://example.com/foo", resizeUrl = "https://example.com/resizedFoo")
    val file2 = File(originalFilename = "bar.txt", url = "https://example.com/bar", resizeUrl = "https://example.com/resizedBar")
    val postable = postableJsonString(postableId, "<p>test</p>", null, listOf(file1, file2), PostableType.Note, postingAddresses, attributedTo)

    val mockHttpSig = mockk<HttpSig>()
    val mockPostables = mockk<PostablePersistence>()

    init {
        coEvery { mockHttpSig.verifyHttpSignature(any(), any(), any(), any()) } returns Result.success("success")
        every { mockPostables.createRemotePostable(ActorId(any()), PostableId(any()), any(), any(), any()) } returns "success"
    }

    @Test
    fun `given a key id URL, get an actor Id`() {
        actorIdFromKeyId("https://example.com/actor#mainkey") shouldBe ActorId("https://example.com/actor")
    }

    @Test
    fun `post inbox`() {
        val mockActors = mockk<ActorPersistence>()
        val mockFetchRemoteActor = mockk<FetchRemoteActor>()
        val service = ActivityPubService(mockFetchRemoteActor, mockHttpSig, mockActors, mockPostables)
        val mockHeaders = mockk<Headers>()
        runBlocking { service.doPostInbox(mockHeaders, "/test", postable) }
        coVerify {
            mockPostables.createRemotePostable(ActorId(any()), PostableId(any()), any(), any(), any())
        }
    }

    @Test
    fun `get public key for known actor`() {
        val mockActors = mockk<ActorPersistence>()
        every { mockActors.fetchKeyInfo(ActorId(any())) } returns PostingKeyInfo(
            "https://example.com/actor#mainkey",
            "-----BEGIN PRIVATE KEY----- -----END PRIVATE KEY-----",
            "-----BEGIN PUBLIC KEY----- -----END PUBLIC KEY-----"
        )
        val mockFetchRemoteActor = mockk<FetchRemoteActor>()
        val service = ActivityPubService(mockFetchRemoteActor, mockHttpSig, mockActors, mockPostables)
        runBlocking { service.getPublicKey("foo") }
        coVerify {
            mockFetchRemoteActor wasNot Called
        }
    }

    @Test
    fun `get public key for unknown actor`() {
        val mockActors = mockk<ActorPersistence>()
        every { mockActors.fetchKeyInfo(ActorId(any())) } returns null
        val mockFetchRemoteActor = mockk<FetchRemoteActor>()
        every { mockFetchRemoteActor.fetchRemoteActor(ActorId(any())) } returns ParsedActor(
            "-----BEGIN PUBLIC KEY----- -----END PUBLIC KEY-----",
            ActorType.Person,
            TEST_ACTOR_DOC_ALICE,
            null,
            null
        )
        val service = ActivityPubService(mockFetchRemoteActor, mockHttpSig, mockActors, mockPostables)
        runBlocking { service.getPublicKey("foo") }
        coVerify {
            mockFetchRemoteActor.fetchRemoteActor(ActorId(any()))
        }
    }
}