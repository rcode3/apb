package com.rcode3.apb.ktor

import com.lectra.koson.obj
import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.persistence.UserPersistence
import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.PersistentJobQueue
import com.rcode3.apb.core.app.ScheduledJobs
import com.rcode3.apb.core.persistence.SiteInfoPersistence
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.SiteRegistrationType
import com.rcode3.apb.jooq.tables.pojos.SiteInfo
import com.rcode3.apb.jooq.tables.records.UserPrincipalRecord
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.time.OffsetDateTime

val MOCK_PERSISTENT_JOB_QUEUE = mock<PersistentJobQueue> {
}

val MOCK_SCHEDULED_JOBS = mock<ScheduledJobs> {}

val MOCK_APP_CONTEXT = mock<AppContext> {
    on { persistenJobQueue } doReturn MOCK_PERSISTENT_JOB_QUEUE
    on { scheduledJobs } doReturn MOCK_SCHEDULED_JOBS
}

val MOCK_USERS = mock<UserPersistence> {
    on { checkPassword(Username(anyString()), any(), any()) } doReturn obj {
        "name" to TEST_USER
        "theme" to TEST_THEME
    }.pretty()

    on { isUserPrincipalActive(Username(anyString()), any()) } doReturn UserPrincipalRecord(
        name = TEST_USER.toString(),
        theme = TEST_THEME,
        systemRoles = arrayOf()
    )
}

val MOCK_ACTORS = mock<ActorPersistence> {
    on { fetchActorById(ActorId(anyString())) } doReturn TEST_ACTOR_OWNER_RESPONSE
    on { fetchActivityPub(ActorId(anyString())) } doReturn TEST_ACTOR_DOC_ALICE
    on { checkActorAvailability(ActorId(anyString())) } doReturn true
    on { isKnownActor(ActorId(anyString()))} doReturn true
    on { reconstituteActorId(PreferredUsername(anyString())) } doReturn TEST_ACTOR_ID
    on { reconstituteActorId(PreferredUsername(anyString()), any()) } doReturn TEST_ACTOR_ID
}

val MOCK_SITE_INFO_PERSISTENCE = mock<SiteInfoPersistence> {
    on { fetch() } doReturn SiteInfo(
        0,
        "test",
        "test long name",
        "testTheme",
        SiteRegistrationType.`Open Registration`,
        OffsetDateTime.now(),
        OffsetDateTime.now()
    )

    on { appContext } doReturn MOCK_APP_CONTEXT
}

