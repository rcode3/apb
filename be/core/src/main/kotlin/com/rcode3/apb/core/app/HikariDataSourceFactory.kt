package com.rcode3.apb.core.app

import com.rcode3.apb.core.constants.DB_DBPASSWORD
import com.rcode3.apb.core.constants.DB_DBUSER
import com.rcode3.apb.core.constants.DB_JDBCURL
import com.rcode3.apb.core.constants.DB_MAXIMUMPOOLSIZE
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import javax.sql.DataSource

class HikariDataSourceFactory(val hikariConfig: HikariConfig = DEFAULT_HIKARI_CONFIG) : DataSourceFactory {
    override fun getInstance(): DataSource {
        if (hikariConfig.equals(DEFAULT_HIKARI_CONFIG)) {
            return dataSource
        }
        //else
        return HikariDataSource(hikariConfig)
    }

    companion object {
        val dataSource = HikariDataSource(DEFAULT_HIKARI_CONFIG)
    }
}

val DEFAULT_HIKARI_CONFIG = hikariConfigFromApplicationConfig()

fun hikariConfigFromApplicationConfig(applicationConfig: Config = ConfigFactory.load().resolve()): HikariConfig {
    val hikariConfig = HikariConfig()
    hikariConfig.jdbcUrl = applicationConfig.getString(DB_JDBCURL)
    hikariConfig.username = applicationConfig.getString(DB_DBUSER)
    hikariConfig.password = applicationConfig.getString(DB_DBPASSWORD)
    hikariConfig.maximumPoolSize = applicationConfig.getString(DB_MAXIMUMPOOLSIZE).toInt()
    hikariConfig.isAutoCommit = true
    hikariConfig.validate()
    return hikariConfig
}