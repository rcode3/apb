package com.rcode3.apb.core.constants

const val DB_JDBCURL = "db.jdbcUrl"
const val DB_DBUSER = "db.dbUser"
const val DB_DBPASSWORD = "db.dbPassword"
const val DB_MAXIMUMPOOLSIZE = "db.maximumPoolSize"

const val PG_JDBC_DRIVER_CLASSNAME = "org.postgresql.Driver"

const val JWT_SECRET = "jwt.secret"
const val JWT_ISSUER = "jwt.issuer"
const val JWT_VALIDITY = "jwt.validity"
const val JWT_REALM = "jwt.realm"

const val AP_HOST = "activityPub.host"
const val AP_URL_SCHEME = "activityPub.urlScheme"

const val FILESTORAGE_DIRECTORY = "fileStorage.directory"
const val FILESTORAGE_URLPATH = "fileStorage.urlPath"

const val KRON_SENDACTIVITYPUB = "kron.sendActivityPub"