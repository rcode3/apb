package com.rcode3.apb.core.logic

import java.net.FileNameMap
import java.net.URLConnection


class SystemAsset(val fileName: String, val url: String) {
    private var mimeType: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SystemAsset

        if (fileName != other.fileName) return false
        if (url != other.url) return false

        return true
    }

    override fun hashCode(): Int {
        var result = fileName.hashCode()
        result = 31 * result + url.hashCode()
        return result
    }

    fun withMimeType(mimeType: String): SystemAsset {
        this.mimeType = mimeType
        return this
    }

    fun mimeType(): String {
        if (mimeType != null) {
            return mimeType!!
        }
        //else
        val fileNameMap: FileNameMap = URLConnection.getFileNameMap()
        return fileNameMap.getContentTypeFor(fileName)
    }
}