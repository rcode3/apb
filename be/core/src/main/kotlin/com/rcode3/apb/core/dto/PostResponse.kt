package com.rcode3.apb.core.dto

import com.rcode3.apb.core.logic.ActivityPubJson

data class PostResponse(
    val returnedJson: String,
    val activityPubJson: ActivityPubJson
)
