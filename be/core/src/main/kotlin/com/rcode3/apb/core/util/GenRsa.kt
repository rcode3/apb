package com.rcode3.apb.core.util

import com.rcode3.apb.core.logic.PemKeyPair
import org.bouncycastle.openssl.jcajce.JcaPEMWriter
import org.bouncycastle.util.io.pem.PemObject
import java.io.ByteArrayOutputStream
import java.io.OutputStreamWriter
import java.security.Key
import java.security.KeyPairGenerator
import java.security.PrivateKey

object RsaGen {
    val kpg = KeyPairGenerator.getInstance("RSA")

    init {
        kpg.initialize(2048)
    }
}

fun genRsa2048(): PemKeyPair {
    val kp = RsaGen.kpg.generateKeyPair()
    val publicPem = writePemObject(kp.public)
    val privatePem = writePemObject(kp.private)
    return PemKeyPair(publicKey = publicPem, privateKey = privatePem)
}

fun writePemObject(key: Key): String {
    val description = if (key is PrivateKey) {
        "PRIVATE KEY"
    } else {
        "PUBLIC KEY"
    }
    val pemObject = PemObject(description, key.encoded)
    val byteStream = ByteArrayOutputStream()
    val pemWriter = JcaPEMWriter(OutputStreamWriter(byteStream))
    pemWriter.writeObject(pemObject)
    pemWriter.close();
    return String(byteStream.toByteArray())
}