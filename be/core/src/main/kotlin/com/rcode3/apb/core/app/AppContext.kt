package com.rcode3.apb.core.app

import com.typesafe.config.Config

interface AppContext {
    val config: Config
    val dsf: DataSourceFactory
    val persistenJobQueue: PersistentJobQueue
    val scheduledJobs: ScheduledJobs
    val common: Common
}