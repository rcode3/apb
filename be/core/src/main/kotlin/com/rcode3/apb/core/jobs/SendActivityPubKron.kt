package com.rcode3.apb.core.jobs

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.constants.KRON_SENDACTIVITYPUB
import com.rcode3.apb.jooq.enums.JobType
import dev.inmo.krontab.doWhile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

class SendActivityPubKron(val appContext: AppContext = StandardAppContext()) {
    val tab = appContext.config.getString(KRON_SENDACTIVITYPUB)
        ?: throw RuntimeException("$KRON_SENDACTIVITYPUB is not specified in configuration.")

    init {
        logger.debug { "Kron Send Activity Pub job create with $tab" }
    }

    suspend fun start() {
        logger.debug { "Starting Send Activity Pub Kron with tab: $tab" }
        doWhile(tab) {
            withContext(Dispatchers.IO) {
                appContext.persistenJobQueue.notify(JobType.`Send Activity Pub`)
            }
            true
        }
    }
}
