package com.rcode3.apb.core.app

import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.jooq.impl.DefaultConfiguration
import javax.sql.DataSource

interface DataSourceFactory {
    fun getInstance(): DataSource
    fun jooqDsl(): DSLContext {
        return DSL.using(getInstance(), SQLDialect.POSTGRES)
    }

    fun jooqConfiguration(): Configuration {
        return DefaultConfiguration().set(getInstance()).set(SQLDialect.POSTGRES)
    }
}