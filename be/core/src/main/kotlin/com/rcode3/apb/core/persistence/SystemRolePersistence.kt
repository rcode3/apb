package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.constants.CREATED_AT
import com.rcode3.apb.core.constants.MODIFIED_AT
import com.rcode3.apb.core.constants.ROLE_TYPE
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.util.objectString
import com.rcode3.apb.jooq.enums.SystemRoleType
import com.rcode3.apb.jooq.tables.references.SYSTEM_ROLE

class SystemRolePersistence(val appContext: AppContext = StandardAppContext()) {

    fun addToRole(member: Username, role: SystemRoleType): String? {
        return appContext.dsf.jooqDsl()
            .insertInto(
                SYSTEM_ROLE,
                SYSTEM_ROLE.MEMBER,
                SYSTEM_ROLE.ROLE_TYPE
            )
            .values(
                member.toString(),
                role
            )
            .returningResult(SYSTEM_ROLE.MEMBER, SYSTEM_ROLE.ROLE_TYPE.`as`(ROLE_TYPE))
            .fetchOne()
            ?.formatJSON(objectString())
    }

    fun removeFromRole(member: Username, role: SystemRoleType): String? {
        return appContext.dsf.jooqDsl()
            .deleteFrom(SYSTEM_ROLE)
            .where(
                SYSTEM_ROLE.MEMBER.eq(member.toString())
                    .and(SYSTEM_ROLE.ROLE_TYPE.eq(role))
            )
            .returningResult(SYSTEM_ROLE.MEMBER, SYSTEM_ROLE.ROLE_TYPE.`as`(ROLE_TYPE))
            .fetchOne()
            ?.formatJSON(objectString())
    }


    fun getUsersInRole(role: SystemRoleType): String {
        return appContext.dsf.jooqDsl()
            .select(
                SYSTEM_ROLE.ROLE_TYPE.`as`(ROLE_TYPE),
                SYSTEM_ROLE.MEMBER,
                SYSTEM_ROLE.CREATED_AT.`as`(CREATED_AT),
                SYSTEM_ROLE.MODIFIED_AT.`as`(MODIFIED_AT)
            )
            .from(SYSTEM_ROLE)
            .where(SYSTEM_ROLE.ROLE_TYPE.eq(role))
            .fetch()
            .formatJSON(objectString())
    }
}