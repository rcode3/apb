package com.rcode3.apb.core.client

import com.rcode3.apb.core.app.ClientFactory
import com.rcode3.apb.core.app.StandardClientFactory
import com.rcode3.apb.core.util.copyToSuspend
import io.ktor.client.request.*
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

suspend fun httpGetToFile(
    url: String,
    file: File,
    clientFactory: ClientFactory = StandardClientFactory()
) {
    val output = FileOutputStream(file)
    val input = clientFactory.getInstance()
        .use {
            it.get<InputStream>(url)
        }
    input.copyToSuspend(output)
}