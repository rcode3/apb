package com.rcode3.apb.core.jobs

import com.rcode3.apb.core.client.httpPostActivityPub
import com.rcode3.apb.core.app.*
import com.rcode3.apb.core.util.parseRsaPrivateKeyPem
import com.rcode3.apb.jooq.enums.JobType
import com.rcode3.apb.jooq.tables.records.JobQueueRecord
import io.ktor.client.features.*
import io.ktor.utils.io.errors.*
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import org.jooq.Configuration
import java.time.OffsetDateTime

@Serializable
data class SendActivityPubJobInfo(
    val publicKeyId: String,
    val privateKeyPem: String,
    val targetUri: String,
    val content: String
)

private val logger = KotlinLogging.logger {  }

class SendActivityPubJob(val appContext: AppContext = StandardAppContext()) {

    val SEND_ATTEMPTS = 4

    fun makeFor(info: SendActivityPubJobInfo) {
        val queue = appContext.persistenJobQueue
        queue.insert(JobType.`Send Activity Pub`, Json.encodeToString(info), SEND_ATTEMPTS, OffsetDateTime.now())
    }

    fun makeFor(tx: Configuration, info: SendActivityPubJobInfo) {
        PersistentJobQueue.insert(tx, JobType.`Send Activity Pub`, Json.encodeToString(info), SEND_ATTEMPTS, OffsetDateTime.now())
    }

    companion object {
        fun registerConsumer(persistentJobQueue: PersistentJobQueue) {
            persistentJobQueue.registerConsumer(JobType.`Send Activity Pub`, consumer)
        }

        val consumer: JobConsumer = { tx: Configuration, jobqueueRecord: JobQueueRecord ->
            logger.info { "Send Activity Pub Job Id: ${jobqueueRecord.id}" }
            val info = Json.decodeFromString<SendActivityPubJobInfo>(jobqueueRecord.details.toString())
            try {
                val privateKey = parseRsaPrivateKeyPem(info.privateKeyPem)
                runBlocking {
                    httpPostActivityPub(
                        publicKeyId = info.publicKeyId,
                        privateKey = privateKey,
                        targetUri = info.targetUri,
                        content = info.content
                    )
                }
            } catch (ioe: IOException) {
                requeue(ioe, info, jobqueueRecord, tx)
            } catch (cre: ClientRequestException) {
                requeue(cre, info, jobqueueRecord, tx)
            } catch (tce: TimeoutCancellationException) {
                requeue(tce, info, jobqueueRecord, tx)
            }
        }

        private fun requeue(
            ex: Exception,
            info: SendActivityPubJobInfo,
            jobqueueRecord: JobQueueRecord,
            tx: Configuration
        ) {
            logger.warn { "unable to send activity pub to ${info.targetUri} on job ${jobqueueRecord.id} with attempt ${jobqueueRecord.attemptsRemaining} because ${ex.message}. Requeuing" }
            basicActivityPubRequeue(jobqueueRecord, JobType.`Send Activity Pub`, tx)
        }
    }

}