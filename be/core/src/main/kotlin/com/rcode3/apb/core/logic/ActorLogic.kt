package com.rcode3.apb.core.logic

import com.google.gson.Gson
import com.google.gson.JsonParser
import com.lectra.koson.arr
import com.lectra.koson.obj
import com.rcode3.apb.core.constants.ACTIVITY_PUB_CONTENT_TYPE
import com.rcode3.apb.core.constants.ACTIVITY_STREAMS_TYPE
import com.rcode3.apb.core.constants.AP_SHARED_INBOX_SEGMENT
import com.rcode3.apb.core.constants.ID
import com.rcode3.apb.core.jobs.ResizeIconJob
import com.rcode3.apb.core.util.SUMMARY_ALLOWLIST
import com.rcode3.apb.jooq.enums.ActorType
import io.ktor.http.*
import io.ktor.util.*
import mu.KotlinLogging
import mu.KotlinLogging.logger
import org.jsoup.Jsoup


fun punFromActorId(actorId: ActorId): PreferredUsername =
    PreferredUsername(actorId.toString().split("/").last())


fun iconProperties(width: Int, height: Int, url: String) =
    obj {
        "type" to "Image"
        "name" to "Note (${width}x${height})"
        "url" to url
        "width" to width
        "height" to height
    }

fun iconObj(width: Int, height: Int, url: String) =
    obj {
        "icon" to iconProperties(width, height, url)
    }

fun bannerProperties(url: String) =
    obj {
        "type" to "Image"
        "name" to "Banner Image"
        "url" to url
    }

fun bannerObj(url: String) =
    obj {
        "image" to bannerProperties(url)
    }

fun avatarProperties(url: String, type: String) =
    obj {
        "rel" to "http://webfinger.net/rel/avatar"
        "type" to type
        "href" to url
    }

fun webfingerJson(
    actorId: ActorId,
    webfingerAcct: WebfingerAcct,
    profileUrl: String,
    avatarUrl: String?,
    avatarMimeType: String?
) =
    obj {
        "subject" to webfingerAcct
        "links" to arr[
                obj {
                    "rel" to "self"
                    "type" to ACTIVITY_PUB_CONTENT_TYPE.toString()
                    "href" to actorId
                },
                obj {
                    "rel" to "self"
                    "type" to ACTIVITY_STREAMS_TYPE.toString()
                    "href" to actorId
                },
                obj {
                    "rel" to "http://webfinger.net/rel/profile-page"
                    "type" to ContentType.Text.Html
                    "href" to profileUrl
                },
                if (avatarUrl != null && avatarMimeType != null) {
                    obj {
                        "rel" to "http://webfinger.net/rel/avatar"
                        "type" to avatarUrl
                        "href" to avatarMimeType
                    }
                } else {
                    //nothing
                }
        ]
    }

fun actorJson(
    actorId: ActorId,
    preferredUserName: PreferredUsername,
    inboxUrl: String,
    publicKeyId: String,
    publicKeyPem: String,
    iconUrl: String?,
    bannerUrl: String?,
    name: String? = null,
    actorType: ActorType,
    apBsseUrl: String
) =
    obj {
        "@context" to arr["https://www.w3.org/ns/activitystreams", "https://w3id.org/security/v1"]
        "id" to actorId
        "type" to actorType.literal
        "preferredUsername" to preferredUserName
        "inbox" to inboxUrl
        "publicKey" to obj {
            "id" to publicKeyId
            "owner" to actorId
            "publicKeyPem" to publicKeyPem
        }
        "endpoints" to obj {
            "sharedInbox" to "$apBsseUrl/$AP_SHARED_INBOX_SEGMENT"
        }
        if (name != null) {
            "name" to name
        }
        if (iconUrl != null) {
            "icon" to iconProperties(ResizeIconJob.WIDTH, ResizeIconJob.HEIGHT, iconUrl)
        }
        if (bannerUrl != null) {
            "image" to bannerProperties(bannerUrl)
        }
    }

fun parseWebfingerJson(
    jsonString: String,
    acctParts: Pair<String, String>
): Pair<ActorId, WebfingerAcct>
{
    val logger = KotlinLogging.logger {}
    logger.debug { jsonString }
    val json = JsonParser.parseString(jsonString).asJsonObject
    val subject = json["subject"].asString
    if (! "acct:${acctParts.first}@${acctParts.second}".lowercase().equals(subject.lowercase())) {
        throw IllegalStateException("webfinger subject $subject does not match acct $acctParts")
    }
    val links = json["links"].asJsonArray
        ?: throw IllegalStateException("webfinger is missing links")
    var actorId: String? = null
    links.forEach { link ->
        val linkObj = link.asJsonObject
        val rel = linkObj["rel"].asString
            ?: throw IllegalStateException("no 'rel' value in link")
        when(rel) {
            "self" -> {
                val type = linkObj["type"].asString
                if (type == null || !
                    (type.equals(ACTIVITY_STREAMS_TYPE.toString()) || type.equals(
                        ACTIVITY_PUB_CONTENT_TYPE.toString())
                            )
                ) {
                    throw IllegalStateException("self type is inccorect as $type")
                }
                actorId = linkObj["href"].asString
                    ?: throw IllegalStateException("actorId href is null")
            }
        }
    }
    return Pair(ActorId(actorId!!), WebfingerAcct(subject))
}

data class ParsedActor(
    val publicKeyPem: String,
    val actorType: ActorType,
    val cleanedJson: String,
    val imageUrl: String?,
    val iconUrl: String?
)

fun parseActorActivityPub(
    actorId: ActorId,
    apString: String
) : ParsedActor {
    val logger = KotlinLogging.logger {}
    logger { apString }
    val json = JsonParser.parseString(apString).asJsonObject
    val id = json[ID].asString
        ?: throw IllegalStateException("activityPub JSON has no ID")
    if (!id.lowercase().equals(actorId.toString().lowercase())) {
        throw IllegalStateException("ActivityPub ID of $actorId fetched doc with ID $id")
    }
    val publicKeyPem = json["publicKey"].asJsonObject["publicKeyPem"].asString
        ?: throw IllegalStateException("No public key found in activitypub actor")
    val actorType = ActorType.valueOf(json["type"].asString)
    val imageUrl = json["image"]?.asJsonObject?.get("url")?.asString
    val iconUrl = json["icon"]?.asJsonObject?.get("url")?.asString
    json["summary"].asString?.let {
        json.addProperty("summary", Jsoup.clean(it, SUMMARY_ALLOWLIST))
    }
    json["name"].asString?.let {
        json.addProperty("name", it.escapeHTML())
    }
    json["preferredUsername"].asString?.let {
        json.addProperty("preferredUsername", it.escapeHTML())
    }
    return ParsedActor(publicKeyPem, actorType, Gson().toJson(json), imageUrl, iconUrl)
}
