package com.rcode3.apb.core.app

import com.rcode3.apb.core.constants.*
import com.typesafe.config.Config

class Common(val config: Config) {
    val apHost: String
        get() = apHost(config)
    val apUrlScheme: String
        get() = apUrlScheme(config)
    val apBaseUrl: String
        get() = apBaseUrl(config)
    val apNoteBaseUrl: String
        get() = apNoteBaseUrl(config)
    val apArticleBaseUrl: String
        get() = apArticleBaseUrl(config)
    val apPersonBaseUrl: String
        get() = apPersonBaseUrl(config)
    val apGroupBaseUrl: String
        get() = apGroupBaseUrl(config)
    val apServiceBaseUrl: String
        get() = apServiceBaseUrl(config)
}

fun apHost(config: Config) = config.getString(AP_HOST)
fun apUrlScheme(config: Config) = config.getString(AP_URL_SCHEME)
fun apBaseUrl(config: Config) = "${apUrlScheme(config)}://${apHost(config)}"
fun apNoteBaseUrl(config: Config) = "${apBaseUrl(config)}/$AP_SEGMENT/$AP_NOTE_SEGMENT"
fun apArticleBaseUrl(config: Config) = "${apBaseUrl(config)}/$AP_SEGMENT/$AP_ARTICLE_SEGMENT"
fun apPersonBaseUrl(config: Config) = "${apBaseUrl(config)}/$AP_SEGMENT/$AP_PERSON_SEGMENT"
fun apGroupBaseUrl(config: Config) = "${apBaseUrl(config)}/$AP_SEGMENT/$AP_GROUP_SEGMENT"
fun apServiceBaseUrl(config: Config) = "${apBaseUrl(config)}/$AP_SEGMENT/$AP_SERVICE_SEGMENT"
