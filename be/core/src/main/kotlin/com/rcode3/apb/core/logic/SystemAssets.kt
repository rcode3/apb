package com.rcode3.apb.core.logic

import com.rcode3.apb.core.util.fileStorageDir
import com.rcode3.apb.core.util.fileStorageUrl
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import java.io.File
import kotlin.random.Random

class SystemAssets(val config: Config = ConfigFactory.load().resolve()) {
    val random = Random

    val SYSTEM_DIR = "system"
    val BANNER_DIR = "$SYSTEM_DIR/banners"
    val ICON_DIR = "$SYSTEM_DIR/icons"

    fun mkdirs() {
        File(fileStorageDir(config, BANNER_DIR)).mkdirs()
        File(fileStorageDir(config, ICON_DIR)).mkdirs()
    }

    fun getSystemAssets(dir: String): Set<SystemAsset> {
        val urls = HashSet<SystemAsset>()
        val fileDir = File(fileStorageDir(config, dir))
        fileDir.walk().maxDepth(1).forEach {
            if (it.isFile) {
                val url = fileStorageUrl(config, dir, it.name)
                urls.add(SystemAsset(it.name, url))
            }
        }
        return urls
    }

    fun getBannerAssets(): Set<SystemAsset> = getSystemAssets(BANNER_DIR)
    fun getIconAssets(): Set<SystemAsset> = getSystemAssets(ICON_DIR)

    fun makeUrl(fileName: String, dir: String): String {
        //sanitize
        val assetName = fileName.replace("/", "")
        return fileStorageUrl(config, dir, assetName)
    }

    fun makeBannerUrl(fileName: String): String = makeUrl(fileName, BANNER_DIR)
    fun makeIconUrl(fileName: String): String = makeUrl(fileName, ICON_DIR)

    fun getRandomAsset(assets: Set<SystemAsset>): SystemAsset? {
        if (assets.size == 0) {
            return null
        }
        val r = random.nextInt(assets.size)
        var i = 0
        var retval: SystemAsset? = null
        assets.forEach { v ->
            if (i == r) {
                retval = v
            }
            i++
        }
        return retval
    }

    fun getRandomBannerAsset() = getRandomAsset(getBannerAssets())
    fun getRandomIconAsset() = getRandomAsset(getIconAssets())
}