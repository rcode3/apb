package com.rcode3.apb.core.util

import org.jsoup.safety.Whitelist

val SUMMARY_ALLOWLIST = Whitelist.basic().removeTags("a")