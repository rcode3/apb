package com.rcode3.apb.core.dto

import kotlinx.serialization.Serializable

@Serializable
data class PostingAddresses(
    val to: Array<String?>,
    val bto: Array<String?>,
    val cc: Array<String?>,
    val bcc: Array<String?>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PostingAddresses

        if (!to.contentEquals(other.to)) return false
        if (!bto.contentEquals(other.bto)) return false
        if (!cc.contentEquals(other.cc)) return false
        if (!bcc.contentEquals(other.bcc)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = to.contentHashCode()
        result = 31 * result + bto.contentHashCode()
        result = 31 * result + cc.contentHashCode()
        result = 31 * result + bcc.contentHashCode()
        return result
    }
}
