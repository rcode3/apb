package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.dto.SiteNames
import com.rcode3.apb.jooq.enums.SiteRegistrationType
import com.rcode3.apb.jooq.tables.pojos.SiteInfo
import com.rcode3.apb.jooq.tables.references.SITE_INFO
import org.jooq.impl.DSL

class SiteInfoPersistence(val appContext: AppContext = StandardAppContext()) {

    /**
     * Always 1. Should only be one row in the database.
     */
    val SITE_INFO_ID = 1

    fun fetch(): SiteInfo? {
        val result = appContext.dsf.jooqDsl()
            .selectFrom(SITE_INFO)
            .where(SITE_INFO.ID.eq(SITE_INFO_ID))
            .limit(1)
            .fetchOneInto(SiteInfo::class.java)
        return result
    }

    fun updateSiteNames(siteNames: SiteNames) {
        siteNames.validate()
        appContext.dsf.jooqDsl().transaction { it ->
            DSL.using(it)
                .update(SITE_INFO)
                .set(SITE_INFO.SHORT_NAME, siteNames.shortName)
                .set(SITE_INFO.LONG_NAME, siteNames.longName)
                .where(SITE_INFO.ID.eq(SITE_INFO_ID))
                .execute()
        }
    }

    fun changeRegistration(registrationType: SiteRegistrationType) {
        appContext.dsf.jooqDsl().transaction { it ->
            DSL.using(it)
                .update(SITE_INFO)
                .set(SITE_INFO.REGISTRATION_TYPE, registrationType)
                .where(SITE_INFO.ID.eq(SITE_INFO_ID))
                .execute()
        }
    }

}


