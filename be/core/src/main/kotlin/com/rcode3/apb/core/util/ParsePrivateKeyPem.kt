package com.rcode3.apb.core.util

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.openssl.PEMKeyPair
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import java.io.StringReader
import java.security.PrivateKey
import java.security.PublicKey

fun parseRsaPrivateKeyPem(privateKeyPem: String): PrivateKey {
    val stringReader = StringReader(privateKeyPem)
    val pemParser = PEMParser(stringReader)
    val o = pemParser.readObject()
    val keyInfo = if (o is PEMKeyPair) {
        o.privateKeyInfo
    } else {
        o as PrivateKeyInfo
    }
    val converter = JcaPEMKeyConverter()
    return converter.getPrivateKey(keyInfo)
}

fun parseRsaPublicKeyPem(publicKeyPem: String): PublicKey {
    val stringReader = StringReader(publicKeyPem)
    val pemParser = PEMParser(stringReader)
    val o = pemParser.readObject()
    val keyInfo = if (o is PEMKeyPair) {
        o.publicKeyInfo
    } else {
        o as SubjectPublicKeyInfo
    }
    val converter = JcaPEMKeyConverter()
    return converter.getPublicKey(keyInfo)
}
