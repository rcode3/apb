package com.rcode3.apb.core.app

import com.rcode3.apb.jooq.enums.JobType
import com.rcode3.apb.jooq.tables.records.JobQueueRecord
import mu.KotlinLogging
import org.jooq.Configuration
import java.time.OffsetDateTime

private val logger = KotlinLogging.logger {}

fun basicActivityPubRequeue(
    jobQueueRecord: JobQueueRecord,
    jobType: JobType,
    tx: Configuration
) {
    val noSoonerThan = when {
        jobQueueRecord.attemptsRemaining!! >= 3 -> OffsetDateTime.now().plusHours(1)
        jobQueueRecord.attemptsRemaining!! == 2 -> OffsetDateTime.now().plusHours(6)
        jobQueueRecord.attemptsRemaining!! == 1 -> OffsetDateTime.now().plusDays(1)
        else -> null
    }
    noSoonerThan?.let {
        logger.info { "Requeuing ${jobQueueRecord.id} for another attempt no sooner than $noSoonerThan" }
        PersistentJobQueue.insert(
            tx,
            jobType,
            jobQueueRecord.details.toString(),
            jobQueueRecord.attemptsRemaining!! - 1,
            noSoonerThan)
    }
}