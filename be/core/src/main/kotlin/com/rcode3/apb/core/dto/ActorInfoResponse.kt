package com.rcode3.apb.core.dto

import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.ActorVisibilityType

data class ActorInfoResponse(
    val actorId: String,
    val owners: Array<String>,
    val visibility: ActorVisibilityType,
    val defaultActor: Boolean,
    val returnedJson: String
) {
    fun actorId() = ActorId(actorId)
    fun owners() = owners.map { Username(it) }
}
