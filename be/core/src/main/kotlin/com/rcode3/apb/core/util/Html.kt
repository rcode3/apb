package com.rcode3.apb.core.util

import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist

class Html {
    companion object {

        fun clean(html: String) =
            Jsoup.clean(html, Whitelist.relaxed())

        fun findTitle(html: String): String? {
            val doc = Jsoup.parse(html)

            //if there is a title, get it
            val titleElement = doc.head().getElementsByTag("title").first()
            titleElement?.let { return titleElement.text() }

            //get the first element from the body and use it if possible
            val body = doc.body()
            body.children().forEach { element ->
                if (element.hasText()) {
                    return element.text()
                }
            }

            //otherwise there is no obvious title
            return null
        }
    }
}