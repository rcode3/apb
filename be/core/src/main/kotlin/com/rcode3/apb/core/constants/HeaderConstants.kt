package com.rcode3.apb.core.constants

import io.ktor.http.*

val HttpHeaders.Signature: String
    get() = "Signature"

val HttpHeaders.Digest: String
    get() = "Digest"

const val HEADER_PARAM_SIGNATURE = "signature"
const val HEADER_PARAM_ALGORITHM = "algorithm"
const val HEADER_PARAM_KEYID = "keyId"
const val HEADER_PARAM_HEADERS = "headers"

const val PSUEDO_HEADER_REQUEST_TARGET = "(request-target)"