package com.rcode3.apb.core.actions

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.dto.BasePost
import com.rcode3.apb.core.dto.PostResponse
import com.rcode3.apb.core.jobs.SendActivityPubJob
import com.rcode3.apb.core.jobs.SendActivityPubJobInfo
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.core.persistence.ActorPersistence
import org.jooq.Configuration

class CreateActivityPubPost(val appContext: AppContext = StandardAppContext()) {

    fun execute(
        post: BasePost,
        owner: Username,
        createPost: (post: BasePost, owner: Username, tx: Configuration) -> PostResponse?
    ): String? {
        val actors = ActorPersistence(appContext)

        val postToActorIds = ArrayList<ActorId>()
        filterOutLocalActors(postToActorIds, post.postingAddresses.to)
        filterOutLocalActors(postToActorIds, post.postingAddresses.bto)
        filterOutLocalActors(postToActorIds, post.postingAddresses.cc)
        filterOutLocalActors(postToActorIds, post.postingAddresses.bcc)
        val inboxes = actors.fetchInboxes(postToActorIds.toTypedArray())

        val keyInfo = actors.fetchKeyInfo(post.actorId)
        keyInfo?: return null
        keyInfo.privateKeyPem?: return null

        val tx = appContext.dsf.jooqConfiguration()
        val retval = createPost(post, owner, tx)
        retval?: return null

        val sendActivityPubJob = SendActivityPubJob(appContext)
        val content = activity(ActivityType.CREATE, post.actorId, ActivityPubJson(retval.activityPubJson.toString())).pretty()
        inboxes.forEach { inbox ->
            val jobInfo = SendActivityPubJobInfo(
                privateKeyPem = keyInfo.privateKeyPem,
                publicKeyId = keyInfo.publicKeyId,
                targetUri = inbox,
                content = content
            )
            sendActivityPubJob.makeFor(tx, jobInfo)
        }

        return retval.returnedJson
    }

    private fun filterOutLocalActors(postToActorIds: ArrayList<ActorId>, postingTos: Array<String?>) {
        postingTos.forEach {
            if (it != null && !it.startsWith(appContext.common.apBaseUrl)) {
                postToActorIds.add(ActorId(it))
            }
        }
    }
}