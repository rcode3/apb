package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.jobs.*
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.tables.pojos.File
import com.rcode3.apb.jooq.tables.references.FILE
import io.ktor.http.ContentType.Image.GIF
import io.ktor.http.ContentType.Image.JPEG
import io.ktor.http.ContentType.Image.PNG
import org.jooq.Configuration
import org.jooq.impl.DSL.using
import kotlin.reflect.KFunction4

class FilePersistence(val appContext: AppContext = StandardAppContext()) {

    /**
     * Adds a database entry for a file.
     */
    fun addFile(
        collaborator: Collaborator
    ): Int? {
        var fileId: Int? = null
        appContext.dsf.jooqDsl()
            .transaction { tx ->
                fileId = using(tx)
                    .insertInto(
                        FILE,
                        FILE.URL,
                        FILE.LOCAL_PATH,
                        FILE.ORIGINAL_FILENAME,
                        FILE.USAGE,
                        FILE.CONTENT_TYPE,
                        FILE.USER_OWNER
                    )
                    .values(
                        collaborator.url,
                        collaborator.localPath,
                        collaborator.originalFilename,
                        collaborator.usage,
                        collaborator.contentType,
                        collaborator.owner.toString()
                    )
                    .returning(FILE.ID)
                    .fetchOne()?.value1()
                collaborator.collaborate(
                    appContext, tx, fileId!!
                )
            }
        return fileId
    }

    /**
     * Removes a database entry for a file.
     */
    fun removeFile(id: Int) {
        appContext.dsf.jooqDsl()
            .deleteFrom(FILE)
            .where(FILE.ID.eq(id))
            .execute()
    }

    /**
     * Removes a database entry for a file.
     */
    fun removeFile(id: Int, owner: String) {
        appContext.dsf.jooqDsl()
            .deleteFrom(FILE)
            .where(FILE.ID.eq(id).and(FILE.USER_OWNER.eq(owner)))
            .execute()
    }

    /**
     * Gets the database information for the file.
     */
    fun getFile(id: Int): File? {
        return appContext.dsf.jooqDsl()
            .selectFrom(FILE)
            .where(FILE.ID.eq(id))
            .fetchOne()?.into(File::class.java)
    }

    /**
     * Gets the database information for the file.
     */
    fun getFile(id: Int, owner: String): File? {
        return appContext.dsf.jooqDsl()
            .selectFrom(FILE)
            .where(FILE.ID.eq(id).and(FILE.USER_OWNER.eq(owner)))
            .fetchOne()?.into(File::class.java)
    }

    fun getFiles(ids: Array<String>, owner: String): List<File> {
        val fileIds = ArrayList<Int>()
        ids.forEach { id -> fileIds.add(id.toInt()) }
        return appContext.dsf.jooqDsl()
            .selectFrom(FILE)
            .where(FILE.USER_OWNER.eq(owner).and(FILE.ID.`in`(fileIds)))
            .fetch().into(File::class.java)
    }

    /**
     * This method deletes the files from the file system as well as
     * removing the database entry for them.
     */
    fun cleanFiles(id: Int): Boolean {
        val fileInfo = getFile(id)
        if (fileInfo != null) {
            java.io.File(fileInfo.localPath!!).delete()
            if (fileInfo.resizeLocalPath != null) {
                java.io.File(fileInfo.resizeLocalPath!!).delete()
            }
            removeFile(id)
            return true
        }
        //else
        return false
    }

    /**
     * This method deletes the files from the file system as well as
     * removing the database entry for them.
     */
    fun cleanFiles(id: Int, owner: String): Boolean {
        val fileInfo = getFile(id, owner)
        if (fileInfo != null) {
            java.io.File(fileInfo.localPath!!).delete()
            if (fileInfo.resizeLocalPath != null) {
                java.io.File(fileInfo.resizeLocalPath!!).delete()
            }
            removeFile(id, owner)
            return true
        }
        //else
        return false
    }

    companion object {
        fun addResizedFile(tx: Configuration, id: Int, resizeUrl: String, resizeLocalPath: String) {
            using(tx)
                .update(FILE)
                .set(FILE.RESIZE_URL, resizeUrl)
                .set(FILE.RESIZE_LOCAL_PATH, resizeLocalPath)
                .where(FILE.ID.eq(id))
                .execute()
        }
    }

    interface Collaborator {
        val url: String
        val localPath: String
        val originalFilename: String
        val usage: String?
        val contentType: String?
        val owner: Username?

        fun collaborate(appContext: AppContext, tx: Configuration, fileId: Int)
    }

    class General(
        override val url: String,
        override val localPath: String,
        override val originalFilename: String,
        override val usage: String?,
        override val contentType: String?,
        override val owner: Username?
    ) : Collaborator {
        override fun collaborate(appContext: AppContext, tx: Configuration, fileId: Int) {
            contentType?.let {
                if (GIF.match(it) || JPEG.match(it) || PNG.match(it)) {
                    val thumbnailJob = ThumbnailJob(appContext)
                    thumbnailJob.makeFor(tx, ThumbnailJobInfo(fileId, contentType, localPath, url))
                }
            }
        }
    }

    class NoReize(
        override val url: String,
        override val localPath: String,
        override val originalFilename: String,
        override val usage: String?,
        override val contentType: String?,
        override val owner: Username?,
        val actorId: ActorId,
        val actorActions: KFunction4<Configuration, ActorId, Username, String, Boolean>
    ) : Collaborator {
        override fun collaborate(appContext: AppContext, tx: Configuration, fileId: Int) {
            actorActions(tx, actorId, owner!!, url)
        }

    }

    class ReizeIcon(
        override val url: String,
        override val localPath: String,
        override val originalFilename: String,
        override val usage: String?,
        override val contentType: String?,
        override val owner: Username,
        val actorId: ActorId
    ) : Collaborator {
        override fun collaborate(appContext: AppContext, tx: Configuration, fileId: Int) {
            contentType?.let {
                if (GIF.match(it) || JPEG.match(it) || PNG.match(it)) {
                    val reiszeIconJob = ResizeIconJob(appContext)
                    reiszeIconJob.makeFor(
                        tx, ResizeIconJobInfo(
                            fileId,
                            contentType,
                            localPath,
                            url,
                            actorId
                        )
                    )
                }
            }
        }
    }

    class ReizeBanner(
        override val url: String,
        override val localPath: String,
        override val originalFilename: String,
        override val usage: String?,
        override val contentType: String?,
        override val owner: Username,
        val actorId: ActorId
    ) : Collaborator {
        override fun collaborate(appContext: AppContext, tx: Configuration, fileId: Int) {
            contentType?.let {
                if (GIF.match(it) || JPEG.match(it) || PNG.match(it)) {
                    val reiszeBannerJob = ResizeBannerJob(appContext)
                    reiszeBannerJob.makeFor(
                        tx, ResizeBannerJobInfo(
                            fileId,
                            contentType,
                            localPath,
                            url,
                            actorId
                        )
                    )
                }
            }
        }
    }
}