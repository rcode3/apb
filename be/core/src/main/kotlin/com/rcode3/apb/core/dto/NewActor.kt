package com.rcode3.apb.core.dto

import com.rcode3.apb.core.logic.PreferredUsername
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import kotlinx.serialization.Serializable

@Serializable
data class NewActor(
    val preferredUsername: String,
    val useRandomSystemIcon: Boolean = true,
    val useRandomSystemBanner: Boolean = true
) {
    val pun = PreferredUsername(preferredUsername)
    fun validate() = Validation<NewActor> {
        NewActor::preferredUsername required {
            minLength(3)
            maxLength(50)
            pattern(singleTokenRegex)
            pattern(noSubDelimsRegex)
        }
    }.validateAndThrowOnError(this)
}
