package com.rcode3.apb.core.dto

import com.rcode3.apb.core.logic.ActivityPubJson
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import java.time.OffsetDateTime

@Serializable
data class PageItem(
    val activityPubJson: String,
    @Contextual
    val modifiedAt: OffsetDateTime
) {
    fun activityPubJson() = ActivityPubJson(activityPubJson)
}
