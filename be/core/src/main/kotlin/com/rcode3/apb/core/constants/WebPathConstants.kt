package com.rcode3.apb.core.constants

const val API_SEGMENT = "api"
const val LOGIN_SEGMENT = "login"
const val USER_SEGMENT = "user"
const val NOTES_SEGMENT = "notes"
const val ARTICLES_SEGMENT = "articles"
const val POSTABLES_SEGMENT = "postables"
const val PERSONAS_SEGMENT = "personas" //relative to the person actors owned by a user
const val PERSONS_SEGMENT = "persons" //relative to the person actors seen by the public
const val ACTORS_SEGMENT = "actors"
const val UPLOAD_SEGMENT = "upload"
const val SITE_SEGMENT = "site"

const val WELL_KNOWN_SEGMENT = ".well-known"
const val WEBFINGER_SEGMENT = "webfinger"
const val HOST_META_SEGMENT = "host-meta"

const val PERSON_PROFILE_SEGMENT = "p"
const val GROUP_PROFILE_SEGMENT = "g"
const val NOTE_PROFILE_SEGMENT = "n"
const val ARTICLE_PROFILE_SEGMENT = "a"

const val AP_SEGMENT = "pub"
const val AP_PERSON_SEGMENT = "p"
const val AP_GROUP_SEGMENT = "g"
const val AP_SERVICE_SEGMENT = "s"
const val AP_INBOX_SEGMENT = "inbox"
const val AP_SHARED_INBOX_SEGMENT = "sharedInbox"
const val AP_OUTBOX_SEGMENT = "outbox"
const val AP_NOTE_SEGMENT = "note"
const val AP_ARTICLE_SEGMENT = "article"
