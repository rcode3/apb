package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.dto.NewActor
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.util.objectString
import com.rcode3.apb.jooq.enums.PasswordType
import com.rcode3.apb.jooq.enums.SiteRegistrationType
import com.rcode3.apb.jooq.enums.UserPrincipalEmailVerifiedType
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.rcode3.apb.jooq.tables.records.UserPrincipalRecord
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import org.jooq.SelectFieldOrAsterisk
import org.jooq.impl.DSL.*
import org.mindrot.jbcrypt.BCrypt

class UserPersistence(val appContext: AppContext = StandardAppContext()) {

    companion object {
        fun defaultUserPrincipalStatus(registrationType: SiteRegistrationType?): UserPrincipalStatusType {
            return when (registrationType) {
                SiteRegistrationType.`Open Registration` -> UserPrincipalStatusType.Active
                else -> UserPrincipalStatusType.`Awaiting Approval`
            }
        }

        val publicFields = listOf<SelectFieldOrAsterisk>(
            USER_PRINCIPAL.NAME,
            USER_PRINCIPAL.LAST_SEEN_AT.`as`(LAST_SEEN_AT),
            USER_PRINCIPAL.LAST_SEEN_FROM.`as`(LAST_SEEN_FROM),
            USER_PRINCIPAL.LAST_LOGIN.`as`(LAST_LOGIN),
            USER_PRINCIPAL.PREVIOUS_LOGIN.`as`(PREVIOUS_LOGIN),
            USER_PRINCIPAL.EMAIL,
            USER_PRINCIPAL.EMAIL_VERIFIED.`as`(EMAIL_VERIFIED),
            USER_PRINCIPAL.EMAIL_VERIFIED_AT.`as`(EMAIL_VERIFIED_AT),
            USER_PRINCIPAL.THEME,
            USER_PRINCIPAL.STATUS,
            USER_PRINCIPAL.SYSTEM_ROLES.`as`(SYSTEM_ROLES),
            USER_PRINCIPAL.CREATED_AT.`as`(CREATED_AT),
            USER_PRINCIPAL.MODIFIED_AT.`as`(MODIFIED_AT)
        )
    }

    fun registerNewUser(newUserRegistration: NewUserRegistration): Pair<Username, ActorId> {
        newUserRegistration.validate()
        val newPersonActor = NewActor(newUserRegistration.name)
        newPersonActor.validate()

        val userprincipalstatustype = if (newUserRegistration.status == null) {
            val siteInfoActions = SiteInfoPersistence(appContext)
            val siteInfo = siteInfoActions.fetch()
            defaultUserPrincipalStatus(siteInfo?.registrationType)
        } else {
            newUserRegistration.status
        }

        val personActions = PersonPersistence(appContext)
        var actorId: ActorId? = null
        var userName: Username? = null

        appContext.dsf.jooqDsl().transaction { tx ->
            userName = Username(newUserRegistration.name)
            val cryptedPassword = BCrypt.hashpw(newUserRegistration.plainTextPassword.trim(), BCrypt.gensalt())


            using(tx)
                .insertInto(
                    USER_PRINCIPAL,
                    USER_PRINCIPAL.NAME,
                    USER_PRINCIPAL.PASSWORD,
                    USER_PRINCIPAL.PASSWORD_TYPE,
                    USER_PRINCIPAL.EMAIL,
                    USER_PRINCIPAL.EMAIL_VERIFIED,
                    USER_PRINCIPAL.STATUS
                )
                .values(
                    userName.toString(),
                    cryptedPassword,
                    PasswordType.bcrypt,
                    newUserRegistration.email,
                    UserPrincipalEmailVerifiedType.`Awaiting Verification`, //TODO revisit when we deal with email
                    userprincipalstatustype
                )
                .execute()

            actorId = personActions.createNewPersonActor(newPersonActor, userName!!, true, tx)
        }

        return Pair(userName!!, actorId!!)
    }

    fun checkUserNameAvailability(username: Username): Boolean {
        val actorIds = ArrayList<String>()
        actorIds.add(PersonPersistence.makePersonId(PreferredUsername(username.toString()), appContext.config).toString())
        val dsl = appContext.dsf.jooqDsl()
        val result = dsl
            .selectOne().from(USER_PRINCIPAL)
            .where(USER_PRINCIPAL.NAME.eq(username.toString()))
            .limit(1)
            .unionAll(
                dsl
                    .selectOne()
                    .from(ACTOR_OWNER)
                    .where(ACTOR_OWNER.ACTOR_ID.`in`(actorIds))
                    .limit(1)
            )
            .limit(1)
            .fetchOne()
        if (result != null && result.value1() == 1) {
            return false
        }
        //else
        return true
    }

    fun checkPassword(username: Username, password: String, lastSeenFrom: String): String? {
        val returningFields = ArrayList<SelectFieldOrAsterisk>()
        returningFields.addAll(publicFields)
        returningFields.add(USER_PRINCIPAL.PASSWORD)
        val result = appContext.dsf.jooqDsl()
            .update(USER_PRINCIPAL)
            .set(USER_PRINCIPAL.LAST_SEEN_FROM, lastSeenFrom)
            .set(USER_PRINCIPAL.LAST_SEEN_AT, currentOffsetDateTime())
            .set(USER_PRINCIPAL.LAST_LOGIN, currentOffsetDateTime())
            .set(
                USER_PRINCIPAL.PREVIOUS_LOGIN,
                select(USER_PRINCIPAL.LAST_LOGIN)
                    .from(USER_PRINCIPAL)
                    .where(USER_PRINCIPAL.NAME.eq(username.toString()))
            )
            .where(USER_PRINCIPAL.NAME.eq(username.toString()))
            .returningResult(
                returningFields
            )
            .fetchOne()
        if (result != null) {
            val cryptedPassword = result[USER_PRINCIPAL.PASSWORD]
            if (BCrypt.checkpw(
                    password.trim(),
                    cryptedPassword
                ) && result[USER_PRINCIPAL.STATUS] == UserPrincipalStatusType.Active
            ) {
                result[USER_PRINCIPAL.PASSWORD] = null
                return result.formatJSON(objectString())
            }
        }
        return null
    }

    fun isUserPrincipalActive(username: Username, lastSeenFrom: String): UserPrincipalRecord? {
        val result = appContext.dsf.jooqDsl()
            .update(USER_PRINCIPAL)
            .set(USER_PRINCIPAL.LAST_SEEN_FROM, lastSeenFrom)
            .set(USER_PRINCIPAL.LAST_SEEN_AT, currentOffsetDateTime())
            .where(USER_PRINCIPAL.NAME.eq(username.toString()))
            .returning(USER_PRINCIPAL.NAME, USER_PRINCIPAL.SYSTEM_ROLES, USER_PRINCIPAL.STATUS)
            .fetchOne()
        if (result != null && result.status == UserPrincipalStatusType.Active) {
            return result
        }
        return null
    }

    fun setUserTheme(username: Username, theme: String) {
        appContext.dsf.jooqDsl()
            .update(USER_PRINCIPAL)
            .set(USER_PRINCIPAL.THEME, theme)
            .where(USER_PRINCIPAL.NAME.eq(username.toString()))
            .execute()
    }

    fun getUserInfo(username: Username): String? {
        return appContext.dsf.jooqDsl()
            .select(
                publicFields
            )
            .from(USER_PRINCIPAL)
            .where(USER_PRINCIPAL.NAME.eq(username.toString()))
            .fetchOne()
            ?.formatJSON(objectString())
    }

    fun suspendUser(username: Username) {
        appContext.dsf.jooqDsl()
            .update(
                USER_PRINCIPAL
            )
            .set(USER_PRINCIPAL.STATUS, UserPrincipalStatusType.Suspended)
            .where(
                USER_PRINCIPAL.NAME.eq(username.toString())
            )
            .execute()
    }

}