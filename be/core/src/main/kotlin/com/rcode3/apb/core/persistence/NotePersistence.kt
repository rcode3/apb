package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.dto.BasePost
import com.rcode3.apb.core.dto.NotePost
import com.rcode3.apb.core.dto.PostResponse
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.core.util.Html
import com.rcode3.apb.jooq.enums.PostableType
import org.jooq.Configuration
import java.time.OffsetDateTime
import java.time.temporal.ChronoField

class NotePersistence(val appContext: AppContext = StandardAppContext()) {

    companion object {
        /**
         * Note that is is not a global ID. (i.e. it is not a URL)
         */
        fun makeNoteId(actorId: ActorId): String {
            val now = OffsetDateTime.now()
            val pun = punFromActorId(actorId)
            return "${pun},${now.year}-${now.month}-${now.dayOfMonth}-${now.get(ChronoField.MILLI_OF_DAY)}"
        }

    }

    /**
     * Creates a note from a local user.
     */
    val createNote = fun(post: BasePost, owner: Username, tx: Configuration): PostResponse? {
        val note = post as NotePost
        val postableActions = PostablePersistence(appContext)
        val ownedImages = postableActions.ownedImages(note.fileIds, owner)

        val cleaned = Html.clean(note.html)
        val title = Html.findTitle(cleaned)
        val postableId = PostableId("${appContext.common.apNoteBaseUrl}/${makeNoteId(note.actorId)}")
        val noteJson = postableJsonString(postableId, cleaned, title, ownedImages, PostableType.Note, note.postingAddresses, post.actorId)

        val returnedJson = postableActions.createPostable(
            note.actorId,
            owner,
            postableId,
            noteJson,
            PostableType.Note,
            note.postingAddresses,
            tx
        )
        returnedJson?:return null
        return PostResponse(returnedJson = returnedJson, activityPubJson = ActivityPubJson(noteJson))
    }
}