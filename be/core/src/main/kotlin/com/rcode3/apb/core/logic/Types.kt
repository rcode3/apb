package com.rcode3.apb.core.logic

import kotlinx.serialization.Serializable

@JvmInline @Serializable
value class ActorId(private val s: String) {
    override fun toString(): String {
        return s.trim().lowercase()
    }
}

@JvmInline @Serializable
value class PostableId(private val s: String) {
    override fun toString(): String {
        return s.trim().lowercase()
    }
}

@JvmInline @Serializable
value class WebfingerAcct(private val s: String) {
    override fun toString(): String {
        return s.trim().lowercase()
    }
}

@JvmInline @Serializable
value class Username(private val s: String) {
    override fun toString(): String {
        return s.trim().lowercase()
    }
}

@JvmInline
value class ActivityPubJson(private val s: String) {
    override fun toString(): String {
        return s
    }
}

@JvmInline
value class WebfingerJson(private val s: String) {
    override fun toString(): String {
        return s
    }
}

@JvmInline @Serializable
value class PreferredUsername(private val s: String) {
    override fun toString(): String {
        return s.trim().lowercase()
    }
}

data class PemKeyPair(val publicKey: String, val privateKey: String)