package com.rcode3.apb.core.persistence

import com.lectra.koson.obj
import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.dto.ActorInfoResponse
import com.rcode3.apb.core.dto.ActorNameSummary
import com.rcode3.apb.core.dto.PageItem
import com.rcode3.apb.core.dto.PostingKeyInfo
import com.rcode3.apb.core.jobs.ResizeIconJob
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.core.util.SUMMARY_ALLOWLIST
import com.rcode3.apb.core.util.objectString
import com.rcode3.apb.jooq.enums.ActorAccessibilityStatusType
import com.rcode3.apb.jooq.enums.ActorType
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import io.ktor.util.*
import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.JSONB
import org.jooq.impl.DSL.*
import org.jsoup.Jsoup
import org.postgresql.PGStatement
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId

class ActorPersistence(val appContext: AppContext = StandardAppContext()) {

    companion object {
        fun setFixedSizedIcon(tx: Configuration, actorId: ActorId, owner: Username, url: String) : Boolean {
            val json = iconObj(ResizeIconJob.WIDTH, ResizeIconJob.HEIGHT, url)
            return concatActivityPubJson(tx, json.toString(), actorId, owner)
        }

        fun setIcon(tx: Configuration, actorId: ActorId, url: String, width: Int, height: Int): Boolean {
            val json = iconObj(width, height, url)
            return concatActivityPubJson(tx, json.toString(), actorId)
        }

        fun setBanner(tx: Configuration, actorId: ActorId, url: String): Boolean {
            val json = bannerObj(url)
            return concatActivityPubJson(tx, json.toString(), actorId)
        }

        /**
         * Caution: this function does not check against ownership.
         */
        fun concatActivityPubJson(
            tx: Configuration,
            json: String,
            actorId: ActorId,
        ): Boolean {
            val result = using(tx)
                .update(ACTOR)
                .set(ACTOR.ACTIVITY_PUB_JSON, field("activity_pub_json || '$json'", JSONB::class.java))
                .where(
                    ACTOR.ID.eq(actorId.toString())
                )
                .returningResult(ACTOR.ID)
                .fetchOne()
            result ?: return false
            return true
        }

        /**
         * Will only update the ActivityPub JSON if the actorId is owned by the owner.
         */
        fun concatActivityPubJson(
            tx: Configuration,
            json: String,
            actorId: ActorId,
            owner: Username
        ): Boolean {
            val result = using(tx)
                .update(ACTOR)
                .set(ACTOR.ACTIVITY_PUB_JSON, field("activity_pub_json || '$json'", JSONB::class.java))
                .from(ACTOR_OWNER)
                .where(
                    ACTOR.ID.eq(actorId.toString())
                        .and(
                            ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID)
                                .and(ACTOR_OWNER.USER_OWNER.eq(owner.toString()))
                        )
                )
                .returningResult(ACTOR.ID)
                .fetchOne()
            result ?: return false
            return true
        }

        fun setAvatar(tx: Configuration, actorId: ActorId, url: String, type: String): Boolean {
            val json = avatarProperties(url, type)
            return concatWebfingerJson(tx, json.toString(), actorId)
        }

        private fun concatWebfingerJson(
            tx: Configuration,
            json: String,
            actorId: ActorId,
        ): Boolean {
            val result = using(tx)
                .update(ACTOR)
                .set(ACTOR.WEBFINGER_JSON, field("webfinger_json->'links' || '$json'", JSONB::class.java))
                .where(
                    ACTOR.ID.eq(actorId.toString())
                )
                .returningResult(ACTOR.ID)
                .fetchOne()
            result ?: return false
            return true
        }
    }

    fun reconstituteActorId(preferredUsername: PreferredUsername, type: ActorType) =
        when(type) {
            ActorType.Person -> ActorId("${appContext.common.apPersonBaseUrl}/${preferredUsername}")
            ActorType.Group -> ActorId("${appContext.common.apGroupBaseUrl}/${preferredUsername}")
            ActorType.Service -> ActorId("${appContext.common.apServiceBaseUrl}/${preferredUsername}")
        }

    fun reconstituteActorId(preferredUsername: PreferredUsername) =
        when {
            preferredUsername.toString().endsWith(GROUP_ACTOR_SUFFIX) -> reconstituteActorId(preferredUsername, ActorType.Group)
            preferredUsername.toString().endsWith(SERVICE_ACTOR_SUFFIX) -> reconstituteActorId(preferredUsername, ActorType.Service)
            else -> reconstituteActorId(preferredUsername, ActorType.Person)
        }

    fun fetchWebfinger(acct: String): String? {
        val searchAcct = if (acct.startsWith("acct:")) {
            acct
        } else {
            "acct:$acct"
        }
        val qr = appContext.dsf.jooqDsl()
            .select(
                ACTOR.WEBFINGER_JSON
            )
            .from(ACTOR)
            .where(
                ACTOR.ACCT.eq(searchAcct)
                    .and(ACTOR.VISIBILITY.eq(ActorVisibilityType.Visible))
            )
            .fetchOne()
        qr ?: return null
        return qr.value1().toString()
    }

    fun fetchActivityPub(actorId: ActorId): String? {
        val qr = appContext.dsf.jooqDsl()
            .select(
                ACTOR.ACTIVITY_PUB_JSON
            )
            .from(ACTOR)
            .where(
                ACTOR.ID.eq(actorId.toString())
                    .and(ACTOR.VISIBILITY.eq(ActorVisibilityType.Visible))
            )
            .fetchOne()
        qr ?: return null
        return qr.value1().toString()
    }

    /**
     * Checks to see if an actor ID is available.
     */
    fun checkActorAvailability(id: ActorId): Boolean {
        val result = appContext.dsf.jooqDsl()
            .selectOne()
            .from(ACTOR)
            .where(ACTOR.ID.eq(id.toString()))
            .limit(1)
            .fetchOne()
        if (result != null && result.value1() == 1) {
            return false
        }
        //else
        return true
    }

    fun isKnownActor(id: ActorId) = !checkActorAvailability(id)

    /**
     * @param owner narrows the actor list to those of the owner
     * @param actorType if null, gets all actors otherwise fetches all by actor type.
     */
    fun fetchAllActorsByOwner(owner: Username, actorType: ActorType? = null): List<ActorInfoResponse> {
        val condition = ACTOR_OWNER.USER_OWNER.eq(owner.toString())
            .and(ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID))
            .let { condition ->
                actorType?.let { condition.and(ACTOR.TYPE.eq(actorType)) } ?: condition
            }

        return appContext.dsf.jooqDsl()
            .selectFromActorOwner(includeOwnerData = true)
            .where(condition)
            .groupBy(
                selectFromActorOwnerGroupBy
            )
            .fetch()
            .into(ActorInfoResponse::class.java)
    }

    fun fetchActorByIdAndOwner(actorId: ActorId, owner: Username): ActorInfoResponse? {
        val result = appContext.dsf.jooqDsl()
            .selectFromActorOwner(includeOwnerData = true)
            .where(
                ACTOR_OWNER.ACTOR_ID.eq(actorId.toString())
                    .and(ACTOR_OWNER.USER_OWNER.eq(owner.toString()))
                    .and(ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID))
            )
            .groupBy(
                selectFromActorOwnerGroupBy
            )
            .fetchOne()
        result ?: return null
        return result.into(ActorInfoResponse::class.java)
    }


    fun fetchActorById(actorId: ActorId): ActorInfoResponse? {
        val result = appContext.dsf.jooqDsl()
            .selectFromActorOwner()
            .where(
                ACTOR_OWNER.ACTOR_ID.eq(actorId.toString())
                    .and(ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID))
            )
            .groupBy(
                selectFromActorOwnerGroupBy
            )
            .fetchOne()
        result ?: return null
        return result.into(ActorInfoResponse::class.java)
    }

    fun fetchActorByAcct(acct: WebfingerAcct): ActorInfoResponse? {
        val searchAcct = if (!acct.toString().startsWith("acct:")) {
            "acct:$acct"
        } else {
            acct.toString()
        }
        val result = appContext.dsf.jooqDsl()
            .selectActorOwner()
            .from(ACTOR).leftOuterJoin(ACTOR_OWNER).on(ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID))
            .where(
                ACTOR.ACCT.eq(searchAcct)
            )
            .groupBy(
                selectFromActorOwnerGroupBy
            )
            .fetchOne()
        result ?: return null
        return result.into(ActorInfoResponse::class.java)
    }

    private fun DSLContext.selectActorOwner(includeOwnerData: Boolean = false) =
        this
            .select(
                ACTOR.ID.`as`(ACTOR_ID),
                ACTOR.VISIBILITY,
                arrayAgg(ACTOR_OWNER.USER_OWNER).`as`(OWNERS),
                ACTOR_OWNER.DEFAULT_ACTOR,
                jsonObject(
                    key(ACTIVITY_PUB_JSON).value(ACTOR.ACTIVITY_PUB_JSON),
                    key(WEBFINGER_JSON).value(ACTOR.WEBFINGER_JSON),
                    key(ACCESSIBILITY_STATUS).value(ACTOR.ACCESSIBILITY_STATUS),
                    key(LAST_HEARD_AT).value(ACTOR.LAST_HEARD_AT),
                    key(LAST_CHECKED_AT).value(ACTOR.LAST_CHECKED_AT),
                    key(CREATED_AT).value(ACTOR.CREATED_AT),
                    key(MODIFIED_AT).value(ACTOR.MODIFIED_AT),
                    key(VISIBILITY).value(ACTOR.VISIBILITY).takeIf { includeOwnerData },
                    key(DEFAULT_ACTOR).value(ACTOR_OWNER.DEFAULT_ACTOR).takeIf { includeOwnerData }
                ).`as`(RETURNED_JSON)
            )

    private fun DSLContext.selectFromActorOwner(includeOwnerData: Boolean = false) =
        this
            .selectActorOwner(includeOwnerData = includeOwnerData)
            .from(ACTOR_OWNER, ACTOR)

    private val selectFromActorOwnerGroupBy = listOf(
        ACTOR.ID,
        ACTOR.VISIBILITY,
        ACTOR_OWNER.DEFAULT_ACTOR,
        ACTOR.ACTIVITY_PUB_JSON,
        ACTOR.WEBFINGER_JSON,
        ACTOR.ACCESSIBILITY_STATUS,
        ACTOR.LAST_HEARD_AT,
        ACTOR.LAST_CHECKED_AT,
        ACTOR.CREATED_AT,
        ACTOR.MODIFIED_AT
    )

    fun fetchActors(
        nextDate: OffsetDateTime?,
        limit: Int,
        actorType: ActorType?,
        accessibilityStatus: ActorAccessibilityStatusType = ActorAccessibilityStatusType.Accessible
    ) : String {
        val _nextDate = nextDate ?: OffsetDateTime.ofInstant(Instant.ofEpochMilli(PGStatement.DATE_POSITIVE_INFINITY), ZoneId.systemDefault())
        val condition = ACTOR.VISIBILITY.eq(ActorVisibilityType.Visible)
            .and(ACTOR.ACCESSIBILITY_STATUS.eq(accessibilityStatus))
            .and(ACTOR.MODIFIED_AT.lt(_nextDate))
            .let { condition ->
                actorType?.let { condition.and(ACTOR.TYPE.eq(actorType)) } ?: condition
            }
        return appContext.dsf.jooqDsl()
            .select(
                ACTOR.ACTIVITY_PUB_JSON.`as`(ACTIVITY_PUB_JSON),
                ACTOR.WEBFINGER_JSON.`as`(WEBFINGER_JSON),
                ACTOR.ACCESSIBILITY_STATUS.`as`(ACCESSIBILITY_STATUS),
                ACTOR.LAST_HEARD_AT.`as`(LAST_HEARD_AT),
                ACTOR.LAST_CHECKED_AT.`as`(LAST_CHECKED_AT),
                ACTOR.CREATED_AT.`as`(CREATED_AT),
                ACTOR.MODIFIED_AT.`as`(MODIFIED_AT)
            )
            .from(ACTOR)
            .where(
                condition
            )
            .orderBy(ACTOR.MODIFIED_AT.desc())
            .limit(limit)
            .fetch()
            .formatJSON(objectString())
    }

    fun setVisibility(actorId: ActorId, owner: Username, visibility: ActorVisibilityType): Boolean {
        val result = appContext.dsf.jooqDsl()
            .update(ACTOR)
            .set(ACTOR.VISIBILITY, visibility)
            .from(ACTOR_OWNER)
            .where(
                ACTOR.ID.eq(actorId.toString())
                    .and(
                        ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID)
                            .and(ACTOR_OWNER.USER_OWNER.eq(owner.toString()))
                    )
            )
            .returningResult(ACTOR.VISIBILITY)
            .fetchOne()
        result ?: return false
        return true
    }

    fun setNameSummary(actorId: ActorId, owner: Username, actorNameSummary: ActorNameSummary): Boolean {
        val json = obj {
            "name" to actorNameSummary.name.escapeHTML()
            "summary" to Jsoup.clean(actorNameSummary.summary, SUMMARY_ALLOWLIST)
        }.toString()
        return concatActivityPubJson(appContext.dsf.jooqConfiguration(), json, actorId, owner)
    }

    fun setAsDefaultActor(actorId: ActorId, owner: Username): ActorId {
        var retval: String? = null
        appContext.dsf.jooqDsl()
            .transaction { tx ->
                using(tx)
                    .update(
                        ACTOR_OWNER
                    )
                    .set(
                        ACTOR_OWNER.DEFAULT_ACTOR, false
                    )
                    .where(
                        ACTOR_OWNER.USER_OWNER.eq(owner.toString())
                    )
                    .execute()

                retval = using(tx)
                    .update(
                        ACTOR_OWNER
                    )
                    .set(
                        ACTOR_OWNER.DEFAULT_ACTOR, true
                    )
                    .where(
                        ACTOR_OWNER.USER_OWNER.eq(owner.toString())
                            .and(
                                ACTOR_OWNER.ACTOR_ID.eq(actorId.toString())
                            )
                    )
                    .returningResult(ACTOR_OWNER.ACTOR_ID)
                    .fetchOne()
                    .toString()
            }
        return ActorId(retval!!)
    }

    /**
     * This can be used inside a transaction by passing in the DSL that is the transaction
     */
    fun createActor(
        actorId: ActorId,
        webfingerAcct: WebfingerAcct,
        webfingerJson: WebfingerJson,
        activityPubJson: ActivityPubJson,
        owner: Username,
        pemKeyPair: PemKeyPair,
        actorType: ActorType,
        defaultActor: Boolean = false,
        tx: Configuration
    ) {
        using(tx)
            .insertInto(
                ACTOR,
                ACTOR.TYPE,
                ACTOR.ID,
                ACTOR.ACCT,
                ACTOR.WEBFINGER_JSON,
                ACTOR.ACTIVITY_PUB_JSON,
                ACTOR.PUBLIC_KEY_PEM,
                ACTOR.PRIVATE_KEY_PEM
            )
            .values(
                actorType,
                actorId.toString(),
                webfingerAcct.toString(),
                JSONB.jsonb(webfingerJson.toString()),
                JSONB.jsonb(activityPubJson.toString()),
                pemKeyPair.publicKey,
                pemKeyPair.privateKey
            )
            .execute()

        using(tx)
            .insertInto(
                ACTOR_OWNER,
                ACTOR_OWNER.ACTOR_ID,
                ACTOR_OWNER.USER_OWNER,
                ACTOR_OWNER.DEFAULT_ACTOR
            )
            .values(
                actorId.toString(),
                owner.toString(),
                defaultActor
            )
            .execute()
    }

    fun saveRemoteActor(
        actorId: ActorId,
        webfingerAcct: WebfingerAcct,
        webfingerJson: WebfingerJson,
        activityPubJson: ActivityPubJson,
        publicKeyPem: String,
        actorType: ActorType,
        visibility: ActorVisibilityType
    ): ActorInfoResponse {
        val result = appContext.dsf.jooqDsl()
            .insertInto(
                ACTOR,
                ACTOR.TYPE,
                ACTOR.ID,
                ACTOR.ACCT,
                ACTOR.WEBFINGER_JSON,
                ACTOR.ACTIVITY_PUB_JSON,
                ACTOR.PUBLIC_KEY_PEM,
                ACTOR.VISIBILITY,
                ACTOR.LAST_CHECKED_AT,
                ACTOR.LAST_HEARD_AT
            )
            .values(
                actorType,
                actorId.toString(),
                webfingerAcct.toString(),
                JSONB.jsonb(webfingerJson.toString()),
                JSONB.jsonb(activityPubJson.toString()),
                publicKeyPem,
                visibility,
                OffsetDateTime.now(),
                OffsetDateTime.now()
            )
            .returningResult(
                jsonObject(
                    key(ACTIVITY_PUB_JSON).value(ACTOR.ACTIVITY_PUB_JSON),
                    key(WEBFINGER_JSON).value(ACTOR.WEBFINGER_JSON),
                    key(ACCESSIBILITY_STATUS).value(ACTOR.ACCESSIBILITY_STATUS),
                    key(LAST_HEARD_AT).value(ACTOR.LAST_HEARD_AT),
                    key(LAST_CHECKED_AT).value(ACTOR.LAST_CHECKED_AT),
                    key(CREATED_AT).value(ACTOR.CREATED_AT),
                    key(MODIFIED_AT).value(ACTOR.MODIFIED_AT),
                    key(VISIBILITY).value(ACTOR.VISIBILITY)
                ).`as`(RETURNED_JSON)
            )
            .fetchOne()
        return ActorInfoResponse(
            actorId = actorId.toString(),
            owners = arrayOf(),
            visibility = visibility,
            defaultActor = false,
            returnedJson = result!![0].toString()
        )
    }

    fun saveRemoteActor(
        actorId: ActorId,
        activityPubJson: ActivityPubJson,
        publicKeyPem: String,
        actorType: ActorType,
        visibility: ActorVisibilityType
    ): ActorInfoResponse {
        val result = appContext.dsf.jooqDsl()
            .insertInto(
                ACTOR,
                ACTOR.TYPE,
                ACTOR.ID,
                ACTOR.ACTIVITY_PUB_JSON,
                ACTOR.PUBLIC_KEY_PEM,
                ACTOR.VISIBILITY,
                ACTOR.LAST_CHECKED_AT,
                ACTOR.LAST_HEARD_AT
            )
            .values(
                actorType,
                actorId.toString(),
                JSONB.jsonb(activityPubJson.toString()),
                publicKeyPem,
                visibility,
                OffsetDateTime.now(),
                OffsetDateTime.now()
            )
            .returningResult(
                jsonObject(
                    key(ACTIVITY_PUB_JSON).value(ACTOR.ACTIVITY_PUB_JSON),
                    key(WEBFINGER_JSON).value(ACTOR.WEBFINGER_JSON),
                    key(ACCESSIBILITY_STATUS).value(ACTOR.ACCESSIBILITY_STATUS),
                    key(LAST_HEARD_AT).value(ACTOR.LAST_HEARD_AT),
                    key(LAST_CHECKED_AT).value(ACTOR.LAST_CHECKED_AT),
                    key(CREATED_AT).value(ACTOR.CREATED_AT),
                    key(MODIFIED_AT).value(ACTOR.MODIFIED_AT),
                    key(VISIBILITY).value(ACTOR.VISIBILITY)
                ).`as`(RETURNED_JSON)
            )
            .fetchOne()
        return ActorInfoResponse(
            actorId = actorId.toString(),
            owners = arrayOf(),
            visibility = visibility,
            defaultActor = false,
            returnedJson = result!![0].toString()
        )
    }

    fun fetchInboxes(actorIds: Array<ActorId>): Array<String> {
        return appContext.dsf.jooqDsl()
            .select(
                coalesce(
                    field("""activity_pub_json::json#>>'{"endpoints", "sharedInbox"}'""", String::class.java),
                    field("""activity_pub_json::json->>'inbox'""", String::class.java)
                )
            )
            .from(ACTOR)
            .where(
                ACTOR.ID.`in`(actorIds.map { it.toString() })
            )
            .fetchArray(0, String::class.java)
    }

    fun fetchKeyInfo(actorId: ActorId): PostingKeyInfo? {
        val result = appContext.dsf.jooqDsl()
            .select(
                ACTOR.PRIVATE_KEY_PEM,
                ACTOR.PUBLIC_KEY_PEM,
                field("""activity_pub_json::json#>>'{"publicKey","id"}'""", String::class.java)
            )
            .from(ACTOR)
            .where(ACTOR.ID.eq(actorId.toString()))
            .fetchOne()
        result?: return null
        return PostingKeyInfo(privateKeyPem = result.value1(), publicKeyPem = result.value2()!!, publicKeyId = result.value3())
    }

    fun fetchActorPage(
        prevDate: OffsetDateTime?,
        nextDate: OffsetDateTime?,
        limit: Int,
        actorType: ActorType?
    ) : List<PageItem> {
        if (prevDate == null && nextDate == null) {
            throw RuntimeException("both prevDate and nextDate are null")
        }
        if (prevDate != null && nextDate != null) {
            throw RuntimeException("one of prevDate or nextDate must be null")
        }
        val condition = noCondition()
            .let { condition ->
                prevDate?.let { condition.and(ACTOR.MODIFIED_AT.gt(prevDate)) } ?: condition
            }
            .let { condition ->
                nextDate?.let { condition.and(ACTOR.MODIFIED_AT.lt(nextDate)) } ?: condition
            }
            .let { condition ->
                actorType?.let { condition.and(ACTOR.TYPE.eq(actorType)) } ?: condition
            }
        return appContext.dsf.jooqDsl()
            .select(
                ACTOR.ACTIVITY_PUB_JSON,
                ACTOR.MODIFIED_AT
            )
            .from(ACTOR)
            .where(condition)
            .orderBy(ACTOR.MODIFIED_AT.desc())
            .limit(limit)
            .fetch()
            .into(PageItem::class.java)
    }
}