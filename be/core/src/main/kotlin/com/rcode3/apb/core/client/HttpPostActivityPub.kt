package com.rcode3.apb.core.client

import com.rcode3.apb.core.app.ClientFactory
import com.rcode3.apb.core.app.StandardClientFactory
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.util.HTTP_DATE_TIME_FORMATTER
import io.ktor.client.request.*
import io.ktor.http.*
import mu.KotlinLogging
import java.security.MessageDigest
import java.security.PrivateKey
import java.security.Signature
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*

private val logger = KotlinLogging.logger {}

suspend fun httpPostActivityPub(
    publicKeyId: String,
    privateKey: PrivateKey,
    targetUri: String,
    content: String,
    clientFactory: ClientFactory = StandardClientFactory()
) {
    val url = Url(targetUri)
    val nowString = HTTP_DATE_TIME_FORMATTER.format(OffsetDateTime.now(ZoneOffset.UTC))
    logger.debug { "content=$content" }
    val digest = MessageDigest.getInstance("SHA-256").digest(content.toByteArray())
    val digestHeaderValue = "sha-256=${Base64.getEncoder().encodeToString(digest)}"
    val toBeSigned = "(request-target): post ${url.encodedPath}\nhost: ${url.host}\ndate: $nowString\ndigest: $digestHeaderValue"
    logger.debug { "tobeSigned=$toBeSigned" }
    val signer = Signature.getInstance("SHA256withRSA")
    signer.initSign(privateKey)
    signer.update(toBeSigned.toByteArray())
    val signature = Base64.getEncoder().encodeToString(signer.sign())
    return clientFactory.getInstance()
        .use {
            it.post(url) {
                accept(ACTIVITY_PUB_CONTENT_TYPE)
                contentType(ACTIVITY_PUB_CONTENT_TYPE)
                headers {
                    append(HttpHeaders.Host, url.host)
                    append(HttpHeaders.Date, nowString)
                    append(HttpHeaders.Digest, digestHeaderValue)
                    append(
                        HttpHeaders.Signature,
                        """keyId="$publicKeyId",algorithm="rsa-sha256",headers="(request-target) host date digest",signature="$signature""""
                    )
                }
                body = content
            }
        }
}