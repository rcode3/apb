package com.rcode3.apb.core.jobs

import com.rcode3.apb.core.persistence.FilePersistence
import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.JobConsumer
import com.rcode3.apb.core.app.PersistentJobQueue
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.logic.LocalFileUpload
import com.rcode3.apb.jooq.enums.JobType
import com.rcode3.apb.jooq.tables.records.JobQueueRecord
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import net.coobird.thumbnailator.Thumbnails
import org.jooq.Configuration
import java.io.FileOutputStream

@Serializable
data class ThumbnailJobInfo(
    val fileId: Int,
    val contentType: String,
    val localPath: String,
    val url: String
)

private val logger = KotlinLogging.logger {}

class ThumbnailJob(val appContext: AppContext = StandardAppContext()) {

    fun makeFor(info: ThumbnailJobInfo) {
        val queue = appContext.persistenJobQueue
        queue.insert(JobType.`Make Thumbnail`, Json.encodeToString(info))
    }

    fun makeFor(tx: Configuration, info: ThumbnailJobInfo) {
        PersistentJobQueue.insert(tx, JobType.`Make Thumbnail`, Json.encodeToString(info))
    }

    companion object {
        fun registerConsumer(persistentJobQueue: PersistentJobQueue) {
            persistentJobQueue.registerConsumer(JobType.`Make Thumbnail`, consumer)
        }

        val consumer: JobConsumer = { tx: Configuration, jobqueueRecord: JobQueueRecord ->
            logger.info { "Thumbnail Job Id: ${jobqueueRecord.id}" }
            val info = Json.decodeFromString<ThumbnailJobInfo>(jobqueueRecord.details.toString())
            val toFile = LocalFileUpload.getLocalResizePath(info.localPath)
            try {
                Thumbnails
                    .of(info.localPath)
                    .useOriginalFormat()
                    .size(400, 400)
                    .toOutputStream(FileOutputStream(toFile))
                FilePersistence.addResizedFile(tx, info.fileId, LocalFileUpload.getResizeUrl(info.url), toFile)
            } catch (ex: Exception) {
                logger.error { "error creating thumbnail for job ${jobqueueRecord.id}" }
            }
        }
    }
}