package com.rcode3.apb.core.util

import org.jooq.JSONFormat

fun objectString() = JSONFormat().header(false).recordFormat(JSONFormat.RecordFormat.OBJECT)

fun arrayString() = JSONFormat().header(false).recordFormat(JSONFormat.RecordFormat.ARRAY)

fun singleColumnArrayString() = JSONFormat()
    .header(false)
    .wrapSingleColumnRecords(false)
    .recordFormat(JSONFormat.RecordFormat.ARRAY)
