package com.rcode3.apb.core.logic

import com.rcode3.apb.core.constants.API_SEGMENT
import com.rcode3.apb.core.constants.UPLOAD_SEGMENT
import com.rcode3.apb.core.util.fileStorageDir
import com.rcode3.apb.core.util.fileStorageUrl
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import java.io.File
import java.time.OffsetDateTime
import kotlin.random.Random

class LocalFileUpload(val config: Config = ConfigFactory.load().resolve()) : UploadManager {

    val random = Random

    data class LocalFileInfo(val path: String, val url: String)

    override fun getAuthorizedUploadUrl(principalId: String): UploadManager.UploadInfo {
        // principalId is of no concern for fileupload type
        return UploadManager.UploadInfo(
            UploadManager.UploadType.FORM_POST,
            "/$API_SEGMENT/$UPLOAD_SEGMENT" //relative URL
        )
    }

    fun getLocalFilePath(principalId: String, extension: String): LocalFileInfo {
        val now = OffsetDateTime.now()
        val localPath = "local/${now.year}/${now.dayOfYear}"
        val destDir = fileStorageDir(config, localPath)
        File(destDir).mkdirs()
        val fileName = "${now.minute}-${random.nextInt(10000)}.${principalId.hashCode()}.$extension"
        return LocalFileInfo(path = "$destDir/$fileName", url = fileStorageUrl(config, localPath, fileName))
    }

    companion object {
        fun getLocalResizePath(localFilePath: String) = getResizeString(localFilePath)
        fun getResizeUrl(originalThumbnail: String) = getResizeString(originalThumbnail)

        fun getResizeString(original: String): String {
            val RESIZED = "--resized"
            val DELIM = '.'
            val before = original.substringBeforeLast(DELIM)
            if (before == original) {
                return "$original$RESIZED"
            }
            //else
            val after = original.substringAfterLast(DELIM)
            return "$before$RESIZED$DELIM$after"
        }
    }
}