package com.rcode3.apb.core.app

import com.rcode3.apb.core.jobs.*
import com.rcode3.apb.jooq.enums.JobType
import com.rcode3.apb.jooq.tables.records.JobQueueRecord
import com.rcode3.apb.jooq.tables.references.JOB_QUEUE
import mu.KotlinLogging
import org.jooq.Configuration
import org.jooq.JSONB
import org.jooq.impl.DSL.*
import org.postgresql.PGConnection
import java.sql.SQLException
import java.sql.Statement
import java.time.OffsetDateTime


typealias JobConsumer = (Configuration, JobQueueRecord) -> Unit
typealias JobRegistry = HashMap<JobType, JobConsumer>

private val logger = KotlinLogging.logger {}

class PersistentJobQueue(val appContext: AppContext = StandardAppContext()) {

    var started: Boolean = false

    private val registry = JobRegistry()

    fun registerConsumer(jobType: JobType, consumer: JobConsumer) {
        registry[jobType] = consumer
    }

    fun start() {
        if (!started) {
            logger.info { "Starting Persistent Job Queue" }

            ThumbnailJob.registerConsumer(this)
            ResizeIconJob.registerConsumer(this)
            ResizeBannerJob.registerConsumer(this)
            FetchRemoteBannerJob.registerConsumer(this)
            FetchRemoteIconJob.registerConsumer(this)
            SendActivityPubJob.registerConsumer(this)

            Listener(appContext, registry).start()
            started = true
        } else {
            logger.warn { "Attempt to start Persistent Job Queue after it has already started." }
        }
    }

    fun insert(jobType: JobType, jobDetails: String) {
        insert(appContext.dsf.jooqConfiguration(), jobType, jobDetails)
    }

    fun insert(jobType: JobType, jobDetails: String, attempts: Int, tryNoSoonerThan: OffsetDateTime) {
        insert(appContext.dsf.jooqConfiguration(), jobType, jobDetails, attempts, tryNoSoonerThan)
    }

    fun notify(jobType: JobType) {
        appContext.dsf.jooqDsl()
            .query("""NOTIFY "$jobType"""")
            .execute()
    }

    companion object {
        fun insert(tx: Configuration, jobType: JobType, jobDetails: String) {
            using(tx)
                .insertInto(JOB_QUEUE)
                .set(JOB_QUEUE.JOB_TYPE, jobType)
                .set(JOB_QUEUE.DETAILS, JSONB.jsonb(jobDetails))
                .execute()

            using(tx)
                .query("""NOTIFY "$jobType"""")
                .execute()
        }

        fun insert(tx: Configuration, jobType: JobType, jobDetails: String, attempts: Int, tryNoSoonerThan: OffsetDateTime) {
            using(tx)
                .insertInto(JOB_QUEUE)
                .set(JOB_QUEUE.JOB_TYPE, jobType)
                .set(JOB_QUEUE.DETAILS, JSONB.jsonb(jobDetails))
                .set(JOB_QUEUE.ATTEMPTS_REMAINING, attempts)
                .set(JOB_QUEUE.NO_SOONER_THAN, tryNoSoonerThan)
                .execute()

            using(tx)
                .query("""NOTIFY "$jobType"""")
                .execute()
        }
    }

    class Listener(val appContext: AppContext, val jobRegistry: JobRegistry) : Thread() {

        private val psqlConnection = appContext.dsf.getInstance().connection

        override fun run() {
            super.run()
            jobRegistry.forEach { jobType, consumer ->
                val stmt: Statement = psqlConnection.createStatement()
                logger.info { "Listening for $jobType in job queue." }
                stmt.execute("""LISTEN "$jobType"""")
                stmt.close()
                processJobs(jobType, consumer) //drain the queue of this type
            }
            while (true) {
                try {
                    // issue a dummy query to contact the backend
                    // and receive any pending notifications.
                    val stmt: Statement = psqlConnection.createStatement()
                    val rs = stmt.executeQuery("SELECT 1")
                    rs.close()
                    stmt.close()
                    val notifications = psqlConnection.unwrap(PGConnection::class.java).notifications
                    notifications?.forEach { notification ->
                        logger.debug { "Processing notifiction for ${notification.name}" }
                        val jobtype = JobType.valueOf(notification.name)
                        jobRegistry[jobtype]?.let { processJobs(jobtype, it) }
                    }

                    // wait a while before checking again for new
                    // notifications
                    sleep(500)
                } catch (sqle: SQLException) {
                    sqle.printStackTrace()
                } catch (ie: InterruptedException) {
                    ie.printStackTrace()
                }
            }
        }

        private fun processJobs(jobType: JobType, consumer: JobConsumer) {
            while (processJob(jobType, consumer));
        }

        private fun processJob(jobType: JobType, consumer: JobConsumer): Boolean {
            val dsl = appContext.dsf.jooqDsl()
            var retval = false
            dsl.transaction { tx ->
                val jobDetails = using(tx)
                    .deleteFrom(JOB_QUEUE)
                    .where(
                        JOB_QUEUE.ID.eq(
                            dsl
                                .select(JOB_QUEUE.ID)
                                .from(JOB_QUEUE)
                                .where(
                                    JOB_QUEUE.JOB_TYPE.eq(jobType)
                                        .and(JOB_QUEUE.ATTEMPTS_REMAINING.gt(0))
                                        .and(JOB_QUEUE.NO_SOONER_THAN.le(currentOffsetDateTime()))
                                )
                                .orderBy(JOB_QUEUE.ID)
                                .limit(1)
                                .forUpdate().skipLocked()
                        )
                    )
                    .returning()
                    .fetchOne()
                if (jobDetails != null) {
                    consumer(tx, jobDetails)
                    retval = true
                }
            }
            return retval
        }
    }
}
