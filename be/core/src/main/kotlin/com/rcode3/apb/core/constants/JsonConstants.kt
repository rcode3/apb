package com.rcode3.apb.core.constants

const val ACTOR = "actor"
const val POSTABLE = "postable"

const val ACCT = "acct"
const val ACTOR_ID = "actorId"
const val CREATED_AT = "createdAt"
const val MODIFIED_AT = "modifiedAt"

const val ACTIVITY_PUB_JSON = "activityPubJson"
const val WEBFINGER_JSON = "webfingerJson"
const val ACCESSIBILITY_STATUS = "accessibilityStatus"
const val LAST_HEARD_AT = "lastHeardAt"
const val LAST_CHECKED_AT = "lastCheckedAt"

const val LAST_SEEN_AT = "lastSeenAt"
const val LAST_SEEN_FROM = "lastSeenFrom"
const val LAST_LOGIN = "lastLogin"
const val PREVIOUS_LOGIN = "previousLogin"
const val EMAIL_VERIFIED = "emailVerified"
const val EMAIL_VERIFIED_AT = "emailVerifiedAt"
const val SYSTEM_ROLES = "systemRoles"

const val ROLE_TYPE = "roleType"
const val OWNERS = "owners"
const val VISIBILITY = "visibility"
const val DEFAULT_ACTOR = "defaultActor"
const val RETURNED_JSON = "returnedJson"