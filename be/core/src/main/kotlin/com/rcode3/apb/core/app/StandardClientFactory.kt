package com.rcode3.apb.core.app

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.logging.*

class StandardClientFactory : ClientFactory {
    override fun getInstance(): HttpClient {
        val client = HttpClient(CIO) {
            install(HttpTimeout) {
            }
            install(Logging) {
                level = LogLevel.HEADERS
            }
            install(UserAgent) {
                agent = "ActivityPub BBS"
            }
        }
        return client
    }
}