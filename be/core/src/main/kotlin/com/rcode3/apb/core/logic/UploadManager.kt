package com.rcode3.apb.core.logic

/**
 * An interface for dealing with upload of files. This provides the scaffolding for abstraction of where files
 * are stored (e.g. local file system, cloud object stores, etc...)
 */
interface UploadManager {

    enum class UploadType {
        /**
         * Signifies multi-part form upload
         */
        FORM_POST,

        /**
         * Signifies HTTP restful PUT
         */
        REST_PUT
    }

    data class UploadInfo(
        val uploadType: UploadType,

        /**
         * The URL to be presented to the user for doing the upload.
         */
        val uploadUrl: String
    )

    /**
     * Returns a URL which can be used to upload a file to an authorized source given an id of the principal to be authorized.
     */
    fun getAuthorizedUploadUrl(principalId: String): UploadInfo

}