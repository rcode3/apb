package com.rcode3.apb.core.client

import com.rcode3.apb.core.app.ClientFactory
import com.rcode3.apb.core.app.StandardClientFactory
import com.rcode3.apb.core.constants.ACTIVITY_PUB_CONTENT_TYPE
import io.ktor.client.request.*

suspend fun httpGetActivityPub(
    activityPubId: String,
    clientFactory: ClientFactory = StandardClientFactory()
): String {
    return clientFactory.getInstance()
        .use {
            it.get(activityPubId) {
                accept(ACTIVITY_PUB_CONTENT_TYPE)
            }
        }
}