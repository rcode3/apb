package com.rcode3.apb.core.constants

const val PREFERRED_USER_NAME = "preferredUsername"
const val ID = "id"
const val NEXT_DATE = "nextDate"
const val LIMIT = "limit"
