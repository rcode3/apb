package com.rcode3.apb.core.logic

import com.rcode3.apb.core.util.fileStorageDir
import com.rcode3.apb.core.util.fileStorageUrl
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import java.io.File
import java.net.URL
import java.net.URLEncoder
import java.nio.charset.Charset
import java.time.OffsetDateTime
import kotlin.random.Random

class RemoteFileManagement(val config: Config = ConfigFactory.load().resolve()) {
    val random = Random

    data class RemoteFileInfo(val path: String, val url: String)

    fun getRemoteFilePath(activityPubId: String): RemoteFileInfo {
        val now = OffsetDateTime.now()
        val localPath = "remote/${now.year}/${now.dayOfYear}"
        val destDir = fileStorageDir(config, localPath)
        File(destDir).mkdirs()
        val url = URL(activityPubId)
        val ext = url.path.split('.').let {
            if (it.last().equals(url.path)) {
                "unknown"
            } else {
                it.last()
            }
        }.replace('/','-')
        val idComponent = "${url.host}-${url.path.hashCode()}-$ext"
        val fileName = "${now.minute}-${now.second}-${random.nextInt(10000)},${idComponent}"
        return RemoteFileInfo(
            path = "$destDir/$fileName",
            url = fileStorageUrl(config, localPath, fileName)
        )
    }
}