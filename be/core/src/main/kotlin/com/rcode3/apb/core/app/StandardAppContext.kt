package com.rcode3.apb.core.app

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory

class StandardAppContext : AppContext {
    override val config: Config
        get() = ConfigFactory.load().resolve()
    override val dsf: DataSourceFactory
        get() = HikariDataSourceFactory()
    override val persistenJobQueue: PersistentJobQueue
        get() = PersistentJobQueue(this)
    override val scheduledJobs: ScheduledJobs
        get() = ScheduledJobs(this)
    override val common: Common
        get() = Common(config)

}