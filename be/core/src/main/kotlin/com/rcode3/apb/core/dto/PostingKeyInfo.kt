package com.rcode3.apb.core.dto

data class PostingKeyInfo(
    val publicKeyId: String,
    val privateKeyPem: String?,
    val publicKeyPem: String
)
