package com.rcode3.apb.core.dto

import io.konform.validation.Invalid
import io.konform.validation.Validation

val singleTokenRegex = Regex("^\\w*\$")
val noSubDelimsRegex = Regex("^[^!\$&'()*+,;=]+\$")
val emailRegex = Regex("^[a-zA-Z0-9.!#\$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*\$")

fun <V> Validation<V>.validateAndThrowOnError(value: V) {
    val r = validate(value)
    if (r is Invalid<V>) {
        throw IllegalArgumentException(r.errors.toString())
    }
}