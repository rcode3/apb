package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.dto.ArticlePost
import com.rcode3.apb.core.dto.BasePost
import com.rcode3.apb.core.dto.PostResponse
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.core.util.Html
import com.rcode3.apb.jooq.enums.PostableType
import org.jooq.Configuration
import java.time.OffsetDateTime
import java.time.temporal.ChronoField

class ArticlePersistence(val appContext: AppContext = StandardAppContext()) {

    companion object {
        /**
         * htmlTitle should come from Html.findTitle.
         * Note that this is not a global ID. (i.e. it is not a URL).
         */
        fun makeArticleId(suggestedLocalId: String?, htmlTitle: String?, actorId: ActorId?) : String {
            val secondPart = if (suggestedLocalId != null && suggestedLocalId.isNotEmpty()) {
                suggestedLocalId.replace(Regex("[\\W_]"), "-").lowercase()
            } else if (htmlTitle != null) {
                htmlTitle
                    .split(Regex("\\s"), 10)
                    .joinToString("-")
                    .replace(Regex("[\\W_]"), "-")
                    .lowercase()
            } else {
                val now = OffsetDateTime.now()
                "${now.year}-${now.month}-${now.dayOfMonth}-${now.get(ChronoField.MILLI_OF_DAY)}"
            }

            if (actorId != null) {
                val pun = punFromActorId(actorId)
                return "${pun},${secondPart}"
            } // else
            return secondPart
        }
    }

    /**
     * Creates an article from a local user.
     */
    val createArticle = fun(post: BasePost, owner: Username, tx: Configuration): PostResponse? {
        val article = post as ArticlePost
        val postableActions = PostablePersistence(appContext)
        val ownedImages = postableActions.ownedImages(article.fileIds, owner)

        val cleaned = Html.clean(article.html)
        val title = Html.findTitle(cleaned)
        val localId = makeArticleId(article.localId, title, article.actorId)
        val postableId = PostableId("${appContext.common.apArticleBaseUrl}/$localId")
        val articleJson = postableJsonString(postableId, cleaned, title, ownedImages, PostableType.Article, article.postingAddresses, post.actorId)
        val returnedJson = postableActions.createPostable(
            article.actorId,
            owner,
            postableId,
            articleJson,
            PostableType.Article,
            article.postingAddresses,
            tx
        )
        returnedJson?: return null
        return PostResponse(returnedJson = returnedJson, activityPubJson = ActivityPubJson(articleJson))
    }
}