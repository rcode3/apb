package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.app.apPersonBaseUrl
import com.rcode3.apb.core.constants.AP_INBOX_SEGMENT
import com.rcode3.apb.core.constants.PERSON_PROFILE_SEGMENT
import com.rcode3.apb.core.dto.ActorInfoResponse
import com.rcode3.apb.core.dto.ActorNameSummary
import com.rcode3.apb.core.dto.NewActor
import com.rcode3.apb.core.jobs.ResizeIconJob
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.core.util.genRsa2048
import com.rcode3.apb.jooq.enums.ActorType
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.typesafe.config.Config
import mu.KotlinLogging
import org.jooq.Configuration

private val logger = KotlinLogging.logger {}

class PersonPersistence(val appContext: AppContext = StandardAppContext()) {

    val systemAssets: SystemAssets = SystemAssets(appContext.config)
    val actorActions = ActorPersistence(appContext)

    companion object {
        fun setIcon(tx: Configuration, actorId: ActorId, owner: Username, url: String): Boolean {
            val json = iconObj(ResizeIconJob.WIDTH, ResizeIconJob.HEIGHT, url)
            return ActorPersistence.concatActivityPubJson(tx, json.toString(), actorId, owner)
        }

        fun setBanner(tx: Configuration, actorId: ActorId, owner: Username, url: String): Boolean {
            val json = bannerObj(url)
            return ActorPersistence.concatActivityPubJson(tx, json.toString(), actorId, owner)
        }

        fun makePersonId(preferredUserName: PreferredUsername, config: Config) =
            ActorId("${apPersonBaseUrl(config)}/${preferredUserName}")
    }

    fun makePersonId(preferredUserName: PreferredUsername) = Companion.makePersonId(preferredUserName, appContext.config)

    fun checkPersonaNameAvailability(preferredUsername: PreferredUsername): Boolean {
        return actorActions.checkActorAvailability(makePersonId(preferredUsername))
    }

    fun fetchPersonByPunAndOwner(preferredUserName: PreferredUsername, owner: Username): ActorInfoResponse? {
        return actorActions.fetchActorByIdAndOwner(makePersonId(preferredUserName), owner)
    }

    fun fetchAllPersonsByOwner(owner: Username) =
        actorActions.fetchAllActorsByOwner(owner, ActorType.Person)

    fun setVisibility(preferredUserName: PreferredUsername, owner: Username, visibility: ActorVisibilityType) =
        actorActions.setVisibility(makePersonId(preferredUserName), owner, visibility)

    fun setNameSummary(preferredUserName: PreferredUsername, owner: Username, actorNameSummary: ActorNameSummary) =
        actorActions.setNameSummary(makePersonId(preferredUserName), owner, actorNameSummary)

    fun setAsDefaultActor(preferredUserName: PreferredUsername, owner: Username) =
        actorActions.setAsDefaultActor(makePersonId(preferredUserName), owner)

    fun createNewPersonActor(newActor: NewActor, owner: Username, defaultActor: Boolean = false) =
        createNewPersonActor(newActor, owner, defaultActor, appContext.dsf.jooqConfiguration())

    /**
     * This can be used inside a transaction by passing in the DSL that is the transaction
     */
    fun createNewPersonActor(
        newActor: NewActor,
        owner: Username,
        defaultActor: Boolean = false,
        tx: Configuration
    ): ActorId {
        newActor.validate()

        val keyPair = genRsa2048()

        val iconAsset = if (newActor.useRandomSystemIcon) {
            val a = systemAssets.getRandomIconAsset()
            a ?: logger.warn { "random icon requested but no system icon assets available" }
            a
        } else {
            null
        }
        val bannerAsset = if (newActor.useRandomSystemBanner) {
            val a = systemAssets.getRandomBannerAsset()
            a ?: logger.warn { "random banner requested but no system banner assets available" }
            a
        } else {
            null
        }

        val actorId = makePersonId(PreferredUsername(newActor.preferredUsername))

        actorActions.createActor(
            actorId = actorId,
            webfingerAcct = WebfingerAcct(acct(newActor.pun)),
            webfingerJson = WebfingerJson(
                webfingerJson(
                    actorId = actorId,
                    webfingerAcct = WebfingerAcct(acct(newActor.pun)),
                    profileUrl = "${appContext.common.apHost}/${PERSON_PROFILE_SEGMENT}/${newActor.preferredUsername}",
                    avatarUrl = iconAsset?.url,
                    avatarMimeType = iconAsset?.mimeType()
                ).toString()
            ),
            activityPubJson = ActivityPubJson(
                actorJson(
                    actorId = actorId,
                    preferredUserName = newActor.pun,
                    inboxUrl = inboxUrl(newActor.pun),
                    publicKeyId = publicKeyId(newActor.pun),
                    publicKeyPem = keyPair.publicKey,
                    iconUrl = iconAsset?.url,
                    bannerUrl = bannerAsset?.url,
                    actorType = ActorType.Person,
                    apBsseUrl = appContext.common.apBaseUrl
                ).toString()
            ),
            owner = Username(owner.toString()),
            pemKeyPair = keyPair,
            actorType = ActorType.Person,
            defaultActor = defaultActor,
            tx = tx
        )

        return actorId
    }

    private fun acct(preferredUserName: PreferredUsername) = "acct:$preferredUserName@${appContext.common.apHost}"

    private fun inboxUrl(preferredUserName: PreferredUsername) = "${appContext.common.apPersonBaseUrl}/$preferredUserName/$AP_INBOX_SEGMENT"

    private fun publicKeyId(preferredUserName: PreferredUsername) = "${appContext.common.apPersonBaseUrl}/$preferredUserName#ap_key"
}
