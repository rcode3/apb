package com.rcode3.apb.core.dto

import com.rcode3.apb.core.logic.ActorId

interface BasePost {
    val actorId: ActorId
    val html: String
    val postingAddresses: PostingAddresses
}