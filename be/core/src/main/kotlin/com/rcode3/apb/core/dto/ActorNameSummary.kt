package com.rcode3.apb.core.dto

import kotlinx.serialization.Serializable

@Serializable
data class ActorNameSummary(val name: String, val summary: String)