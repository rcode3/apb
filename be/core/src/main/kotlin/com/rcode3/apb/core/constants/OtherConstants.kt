package com.rcode3.apb.core.constants

const val GROUP_ACTOR_SUFFIX = "*g"
const val SERVICE_ACTOR_SUFFIX = "*s"