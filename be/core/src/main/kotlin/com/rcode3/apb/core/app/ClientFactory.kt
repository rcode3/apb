package com.rcode3.apb.core.app

import io.ktor.client.*

interface ClientFactory {
    fun getInstance(): HttpClient
}