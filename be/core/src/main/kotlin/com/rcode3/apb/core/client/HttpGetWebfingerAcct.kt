package com.rcode3.apb.core.client

import com.rcode3.apb.core.app.ClientFactory
import com.rcode3.apb.core.app.StandardClientFactory
import com.rcode3.apb.core.constants.JRD_CONTENT_TYPE
import com.rcode3.apb.core.constants.WEBFINGER_SEGMENT
import com.rcode3.apb.core.constants.WELL_KNOWN_SEGMENT
import io.ktor.client.request.*
import io.ktor.http.*

suspend fun httpGetWebfingerAcct(
    localAcct: String,
    remoteHost: String,
    clientFactory: ClientFactory = StandardClientFactory()
): String {
    return clientFactory.getInstance()
        .use {
            it.get {
                url {
                    host = remoteHost
                    path(WELL_KNOWN_SEGMENT, WEBFINGER_SEGMENT)
                    protocol = URLProtocol.HTTPS
                    accept(JRD_CONTENT_TYPE)
                    parameter("resource", "acct:$localAcct@$remoteHost")
                }
            }
        }
}