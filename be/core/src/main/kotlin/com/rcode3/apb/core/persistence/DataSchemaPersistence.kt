package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.jooq.tables.references.FLYWAY_SCHEMA_HISTORY

class DataSchemaPersistence(val appContext: AppContext = StandardAppContext()) {

    fun fetchFlywaySchema(): String? {
        val result = appContext.dsf.jooqDsl()
            .selectFrom(FLYWAY_SCHEMA_HISTORY)
            .where(FLYWAY_SCHEMA_HISTORY.VERSION.isNotNull)
            .orderBy(FLYWAY_SCHEMA_HISTORY.VERSION.desc())
            .limit(1)
            .fetchOne()
        return result?.version
    }

}

