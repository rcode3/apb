package com.rcode3.apb.core.util

import com.rcode3.apb.core.app.apBaseUrl
import com.rcode3.apb.core.constants.FILESTORAGE_DIRECTORY
import com.rcode3.apb.core.constants.FILESTORAGE_URLPATH
import com.typesafe.config.Config
import java.io.File

fun fileStorageBaseUrl(config: Config) =
    "${apBaseUrl(config)}${config.getString(FILESTORAGE_URLPATH)}"

fun fileStorageUrl(config: Config, vararg pathSegments: String) : String {
    val sb = StringBuilder()
    sb.append(fileStorageBaseUrl(config))
    pathSegments.forEach {
        sb.append("/")
        sb.append(it)
    }
    return sb.toString()
}

fun fileStorageDir(config: Config, vararg fileComponents: String) : String {
    val sb = StringBuilder()
    sb.append(config.getString(FILESTORAGE_DIRECTORY))
    fileComponents.forEach {
        sb.append(File.separator)
        sb.append(it)
    }
    return sb.toString()
}