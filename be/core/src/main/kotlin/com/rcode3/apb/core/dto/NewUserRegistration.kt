package com.rcode3.apb.core.dto

import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import kotlinx.serialization.Serializable

@Serializable
data class NewUserRegistration(
    val name: String,
    val email: String,
    val plainTextPassword: String,
    val status: UserPrincipalStatusType? = null
) {
    val username = Username(name)
    fun validate() = Validation<NewUserRegistration> {
        NewUserRegistration::name required {
            minLength(3)
            maxLength(50)
            pattern(singleTokenRegex)
            pattern(noSubDelimsRegex)
        }
        NewUserRegistration::email required {
            pattern(emailRegex)
        }
        NewUserRegistration::plainTextPassword required {
            minLength(8)
            maxLength(50)
        }
    }.validateAndThrowOnError(this)
}

