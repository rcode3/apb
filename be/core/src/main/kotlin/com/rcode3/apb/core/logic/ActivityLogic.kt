package com.rcode3.apb.core.logic

import com.lectra.koson.obj
import com.lectra.koson.rawJson
import java.time.OffsetDateTime
import java.time.temporal.ChronoField

enum class ActivityType(val literal: String) {
    CREATE("Create")
}

fun activity(
    activityType: ActivityType,
    actorId: ActorId,
    activityPubJson: ActivityPubJson
) =
    obj {
        "@context" to "https://www.w3.org/ns/activitystreams"
        "id" to "$actorId,${activityType.literal},${OffsetDateTime.now().toInstant().toEpochMilli()}"
        "actor" to actorId
        "type" to activityType.literal
        "object" to rawJson(activityPubJson.toString())
    }
