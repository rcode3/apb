package com.rcode3.apb.core.jobs

import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.client.httpGetToFile
import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.JobConsumer
import com.rcode3.apb.core.app.PersistentJobQueue
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.RemoteFileManagement
import com.rcode3.apb.jooq.enums.JobType
import com.rcode3.apb.jooq.tables.records.JobQueueRecord
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.geometry.Positions
import org.jooq.Configuration
import java.io.File
import java.io.FileOutputStream
import java.net.URLEncoder
import java.nio.charset.Charset

@Serializable
data class FetchRemoteIconJobInfo(
    val url: String,
    val actorId: ActorId,
)

private val logger = KotlinLogging.logger {}

class FetchRemoteIconJob(val appContext: AppContext = StandardAppContext()) {

    fun makeFor(info: FetchRemoteIconJobInfo) {
        val queue = appContext.persistenJobQueue
        queue.insert(JobType.`Fetch Remote Icon`, Json.encodeToString(info))
    }

    fun makeFor(tx: Configuration, info: FetchRemoteIconJobInfo) {
        PersistentJobQueue.insert(tx, JobType.`Fetch Remote Icon`, Json.encodeToString(info))
    }

    companion object {
        val WIDTH = 400
        val HEIGHT = 400

        fun registerConsumer(persistentJobQueue: PersistentJobQueue) {
            persistentJobQueue.registerConsumer(JobType.`Fetch Remote Icon`, consumer)
        }

        val consumer: JobConsumer = { tx: Configuration, jobqueueRecord: JobQueueRecord ->
            logger.info { "Fetch Remote Banner Job Id: ${jobqueueRecord.id}" }
            val info = Json.decodeFromString<FetchRemoteIconJobInfo>(jobqueueRecord.details.toString())
            try {
                val tmp = File.createTempFile(URLEncoder.encode(info.url, Charset.defaultCharset()), "icn")
                runBlocking { httpGetToFile(info.url, tmp) }
                val remoteFile = RemoteFileManagement().getRemoteFilePath(info.url)
                Thumbnails
                    .of(tmp.absolutePath)
                    .useOriginalFormat()
                    .size(WIDTH, HEIGHT)
                    .crop(Positions.CENTER)
                    .toOutputStream(FileOutputStream(remoteFile.path))
                ActorPersistence.setIcon(tx, info.actorId, remoteFile.url, WIDTH, HEIGHT)
                tmp.delete()
            } catch (ex: Exception) {
                logger.error { "error fetching remote icon for job ${jobqueueRecord.id}: ${ex.message}" }
            }
        }
    }
}