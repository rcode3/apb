package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.constants.ACCT
import com.rcode3.apb.core.constants.ACTIVITY_PUB_JSON
import com.rcode3.apb.core.constants.ACTOR_ID
import com.rcode3.apb.core.dto.PageItem
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.PostableId
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.util.*
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.jooq.enums.PostableType
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.rcode3.apb.jooq.tables.pojos.File
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.POSTABLE
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import org.jooq.Configuration
import org.jooq.JSONB
import org.jooq.impl.DSL
import org.jooq.impl.DSL.*
import java.time.OffsetDateTime

class PostablePersistence(val appContext: AppContext = StandardAppContext()) {

    fun ownedImages(imageIds: Array<String>, owner: Username): List<File> {
        val fileActions = FilePersistence(appContext)
        return if (imageIds.isNotEmpty()) {
            fileActions.getFiles(imageIds, owner.toString())
        } else {
            listOf()
        }
    }

    /**
     * Creates a postable.
     */
    fun createPostable(
        actorId: ActorId,
        owner: Username,
        postableId: PostableId,
        stringJson: String,
        type: PostableType,
        postingAddresses: PostingAddresses,
        tx: Configuration
    ): String? {
        return using(tx)
            .insertInto(
                POSTABLE,
                POSTABLE.ID,
                POSTABLE.ACTOR_OWNER,
                POSTABLE.ACTIVITY_PUB_JSON,
                POSTABLE.TYPE,
                POSTABLE.ADDRESSED_TO,
                POSTABLE.ADDRESSED_BTO,
                POSTABLE.ADDRESSED_CC,
                POSTABLE.ADDRESSED_BCC
            )
            .select(
                select(
                    inline(postableId.toString()),
                    ACTOR.ID,
                    inline(JSONB.jsonb(stringJson)),
                    inline(type),
                    inline(postingAddresses.to),
                    inline(postingAddresses.bto),
                    inline(postingAddresses.cc),
                    inline(postingAddresses.bcc)
                )
                    .from(ACTOR, ACTOR_OWNER, USER_PRINCIPAL)
                    .where(
                        ACTOR.ID.eq(actorId.toString())
                            .and(
                                ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID)
                            )
                            .and(
                                ACTOR_OWNER.USER_OWNER.eq(owner.toString())
                            )
                            .and(
                                USER_PRINCIPAL.NAME.eq(ACTOR_OWNER.USER_OWNER)
                            )
                            .and(
                                USER_PRINCIPAL.STATUS.eq(UserPrincipalStatusType.Active)
                            )
                    )
            )
            .returningResult(
                POSTABLE.ACTOR_OWNER.`as`(ACTOR_ID),
                POSTABLE.ID
            )
            .fetchOne()
            ?.formatJSON(objectString())
    }

    /**
     * Creates a postable.
     */
    fun createRemotePostable(
        actorId: ActorId,
        postableId: PostableId,
        stringJson: String,
        type: PostableType,
        postingAddresses: PostingAddresses
    ): String? {
        return appContext.dsf.jooqDsl()
            .insertInto(
                POSTABLE,
                POSTABLE.ID,
                POSTABLE.ACTOR_OWNER,
                POSTABLE.ACTIVITY_PUB_JSON,
                POSTABLE.TYPE,
                POSTABLE.ADDRESSED_TO,
                POSTABLE.ADDRESSED_BTO,
                POSTABLE.ADDRESSED_CC,
                POSTABLE.ADDRESSED_BCC
            )
            .select(
                select(
                    inline(postableId.toString()),
                    ACTOR.ID,
                    inline(JSONB.jsonb(stringJson)),
                    inline(type),
                    inline(postingAddresses.to),
                    inline(postingAddresses.bto),
                    inline(postingAddresses.cc),
                    inline(postingAddresses.bcc)
                )
                    .from(ACTOR, ACTOR_OWNER, USER_PRINCIPAL)
                    .where(
                        ACTOR.ID.eq(actorId.toString())
                            .and(
                                ACTOR.ID.eq(ACTOR_OWNER.ACTOR_ID)
                            )
                            .and(
                                ACTOR.PRIVATE_KEY_PEM.isNull
                            )
                            .and(
                                ACTOR.VISIBILITY.ne(ActorVisibilityType.Banned)
                            )
                    )
            )
            .returningResult(
                POSTABLE.ACTOR_OWNER.`as`(ACTOR_ID),
                POSTABLE.ID
            )
            .fetchOne()
            ?.formatJSON(objectString())
    }

    fun fetchPostable(postableId: PostableId): String? {
        return appContext.dsf.jooqDsl()
            .select(
                jsonObject(
                    key(ACTIVITY_PUB_JSON).value(POSTABLE.ACTIVITY_PUB_JSON)
                ).`as`(com.rcode3.apb.core.constants.POSTABLE),
                jsonObject(
                    key(ACTIVITY_PUB_JSON).value(ACTOR.ACTIVITY_PUB_JSON),
                    key(ACCT).value(ACTOR.ACCT),
                ).`as`(com.rcode3.apb.core.constants.ACTOR)
            )
            .from(
                POSTABLE,
                ACTOR
            )
            .where(
                POSTABLE.ID.eq(postableId.toString())
                    .and(
                        POSTABLE.ACTOR_OWNER.eq(ACTOR.ID)
                    )
            )
            .fetchOne()
            ?.formatJSON(objectString())
    }

    fun fetchPostablePage(
        prevDate: OffsetDateTime?,
        nextDate: OffsetDateTime?,
        limit: Int,
        fromActorId: ActorId?,
        toActorId: ActorId?,
        postableType: PostableType?
    ) : List<PageItem> {
        if (prevDate == null && nextDate == null) {
            throw RuntimeException("both prevDate and nextDate are null")
        }
        if (prevDate != null && nextDate != null) {
            throw RuntimeException("one of prevDate or nextDate must be null")
        }
        val condition = noCondition()
            .let { condition ->
                prevDate?.let { condition.and(POSTABLE.MODIFIED_AT.gt(prevDate)) } ?: condition
            }
            .let { condition ->
                nextDate?.let { condition.and(POSTABLE.MODIFIED_AT.lt(nextDate)) } ?: condition
            }
            .let { condition ->
                fromActorId?.let { condition.and(POSTABLE.ACTOR_OWNER.eq(fromActorId.toString())) } ?: condition
            }
            .let { condition ->
                toActorId?.let {
                    condition.and(
                        POSTABLE.ADDRESSED_TO.contains(arrayOf(toActorId.toString()))
                        .or(POSTABLE.ADDRESSED_BTO.contains(arrayOf(toActorId.toString())))
                        .or(POSTABLE.ADDRESSED_CC.contains(arrayOf(toActorId.toString())))
                        .or(POSTABLE.ADDRESSED_BCC.contains(arrayOf(toActorId.toString())))
                    )
                } ?: condition
            }
            .let { condition ->
                postableType?.let { condition.and(POSTABLE.TYPE.eq(postableType)) } ?:condition
            }
        return appContext.dsf.jooqDsl()
            .select(
                POSTABLE.ACTIVITY_PUB_JSON,
                POSTABLE.MODIFIED_AT
            )
            .from(POSTABLE)
            .where(condition)
            .orderBy(POSTABLE.MODIFIED_AT.desc())
            .limit(limit)
            .fetch()
            .into(PageItem::class.java)
    }
}