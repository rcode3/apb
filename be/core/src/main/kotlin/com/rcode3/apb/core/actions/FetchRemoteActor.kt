package com.rcode3.apb.core.actions

import com.rcode3.apb.core.app.ClientFactory
import com.rcode3.apb.core.app.StandardClientFactory
import com.rcode3.apb.core.client.httpGetActivityPub
import com.rcode3.apb.core.client.httpGetWebfingerAcct
import com.rcode3.apb.core.dto.ActorInfoResponse
import com.rcode3.apb.core.jobs.FetchRemoteBannerJob
import com.rcode3.apb.core.jobs.FetchRemoteBannerJobInfo
import com.rcode3.apb.core.jobs.FetchRemoteIconJob
import com.rcode3.apb.core.jobs.FetchRemoteIconJobInfo
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import io.ktor.client.features.*
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.nio.channels.UnresolvedAddressException

private val logger = KotlinLogging.logger {}

class FetchRemoteActor(
    val clientFactory: ClientFactory = StandardClientFactory(),
    val actorPersistence: ActorPersistence = ActorPersistence(),
    val fetchRemoteIconJob: FetchRemoteIconJob = FetchRemoteIconJob(),
    val fetchRemoteBannerJob: FetchRemoteBannerJob = FetchRemoteBannerJob()
) {
    fun fetchRemoteActor(acctParts: Pair<String, String>): ActorInfoResponse? {
        try {
            val webfingerString = runBlocking {
                httpGetWebfingerAcct(acctParts.first, acctParts.second, clientFactory)
            }
            val webfingerData = parseWebfingerJson(webfingerString, acctParts)
            val activityPubString = runBlocking {
                httpGetActivityPub(webfingerData.first.toString())
            }
            val actorData = parseActorActivityPub(webfingerData.first, activityPubString)
            val actorInfoResponse = actorPersistence.saveRemoteActor(
                actorId = webfingerData.first,
                webfingerAcct = webfingerData.second,
                webfingerJson = WebfingerJson(webfingerString),
                activityPubJson = ActivityPubJson(actorData.cleanedJson),
                publicKeyPem = actorData.publicKeyPem,
                actorType = actorData.actorType,
                visibility = ActorVisibilityType.Visible
            )
            actorData.iconUrl?.let {
                fetchRemoteIconJob.makeFor(
                    FetchRemoteIconJobInfo(
                        url = it,
                        actorId = webfingerData.first
                    )
                )
            }
            actorData.imageUrl?.let {
                fetchRemoteBannerJob.makeFor(
                    FetchRemoteBannerJobInfo(
                        url = it,
                        actorId = webfingerData.first
                    )
                )
            }
            return actorInfoResponse
        } catch (ure: UnresolvedAddressException) {
            logger.info { "unable to fetch remote actor on ${acctParts.second}: ${ure.message}" }
        } catch (cre: ClientRequestException) {
            logger.info { "error fetching $acctParts: ${cre.response.status}" }
        } catch (ise: IllegalStateException) {
            logger.info { "error validating $acctParts: ${ise.message}" }
        }

        return null
    }

    fun fetchRemoteActor(actorId: ActorId): ParsedActor? {
        try {
            val activityPubString = runBlocking {
                httpGetActivityPub(actorId.toString())
            }
            val actorData = parseActorActivityPub(actorId, activityPubString)
            actorPersistence.saveRemoteActor(
                actorId,
                ActivityPubJson(actorData.cleanedJson),
                actorData.publicKeyPem,
                actorData.actorType,
                ActorVisibilityType.Visible
            )
            actorData.iconUrl?.let {
                fetchRemoteIconJob.makeFor(
                    FetchRemoteIconJobInfo(
                        url = it,
                        actorId = actorId
                    )
                )
            }
            actorData.imageUrl?.let {
                fetchRemoteBannerJob.makeFor(
                    FetchRemoteBannerJobInfo(
                        url = it,
                        actorId = actorId
                    )
                )
            }
            return actorData
        } catch (ure: UnresolvedAddressException) {
            logger.info { "unable to fetch remote actor on ${actorId}: ${ure.message}" }
        } catch (cre: ClientRequestException) {
            logger.info { "error fetching $actorId: ${cre.response.status}" }
        } catch (ise: IllegalStateException) {
            logger.info { "error validating $actorId: ${ise.message}" }
        }

        return null
    }
}