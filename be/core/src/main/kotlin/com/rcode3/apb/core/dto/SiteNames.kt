package com.rcode3.apb.core.dto

import io.konform.validation.Validation
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import kotlinx.serialization.Serializable

@Serializable
data class SiteNames(
    val shortName: String,
    val longName: String
) {
    fun validate() = Validation<SiteNames> {
        SiteNames::shortName required {
            minLength(1)
            pattern(singleTokenRegex)
        }
        SiteNames::longName required {
            minLength(1)
        }
    }.validateAndThrowOnError(this)
}
