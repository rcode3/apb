package com.rcode3.apb.core.logic

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.lectra.koson.arr
import com.lectra.koson.obj
import com.rcode3.apb.core.constants.ID
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.core.util.Html
import com.rcode3.apb.core.util.SUMMARY_ALLOWLIST
import com.rcode3.apb.core.util.isoNow
import com.rcode3.apb.jooq.enums.PostableType
import com.rcode3.apb.jooq.tables.pojos.File
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist

fun postableJson(
    postableId: PostableId,
    content: String,
    title: String?,
    images: List<File>,
    type: PostableType,
    postingAddresses: PostingAddresses,
    attributedTo: ActorId,
    published: String? = null
) =
    obj {
        val cleanHtml = Html.clean(content)
        "@context" to "https://www.w3.org/ns/activitystreams"
        "id" to postableId
        "type" to type.literal
        "attributedTo" to attributedTo
        if (title != null) {
            "name" to title
        } else {
            Html.findTitle(cleanHtml)?.let { "name" to it }
        }
        "to" to arr[postingAddresses.to.map { it }]
        if (postingAddresses.bto.isNotEmpty()) {
            "bto" to arr[postingAddresses.bto.map { it }]
        }
        if (postingAddresses.cc.isNotEmpty()) {
            "cc" to arr[postingAddresses.cc.map { it }]
        }
        if (postingAddresses.bcc.isNotEmpty()) {
            "bcc" to arr[postingAddresses.bcc.map { it }]
        }
        "content" to cleanHtml
        if (published != null) {
            "published" to published
        } else {
            "published" to isoNow()
        }
        if (images.size == 1) {
            "image" to obj {
                "type" to "Image"
                "name" to images.first().originalFilename
                "url" to images.first().url
            }
            "icon" to obj {
                "type" to "Image"
                "name" to "thumbnail of ${images.first().originalFilename}"
                "url" to images.first().resizeUrl
            }
        } else if (images.size > 1) {
            "image" to arr[
                    images.map { image ->
                        obj {
                            "type" to "Image"
                            "name" to image.originalFilename
                            "url" to image.url
                        }
                    }
            ]
            "icon" to arr[
                    images.map { image ->
                        obj {
                            "type" to "Image"
                            "name" to "thumbnail of ${image.originalFilename}"
                            "url" to image.resizeUrl
                        }
                    }
            ]
        }
    }

fun postableJsonString(
    postableId: PostableId,
    content: String,
    title: String?,
    images: List<File>,
    type: PostableType,
    postingAddresses: PostingAddresses,
    attributedTo: ActorId,
    published: String? = null
) =
    postableJson(
        postableId,
        content,
        title,
        images,
        type,
        postingAddresses,
        attributedTo,
        published
    ).pretty()

data class ParsedPostable(
    val postableType: PostableType,
    val cleanedJson: String,
    val owningActor: ActorId,
    val postingAddresses: PostingAddresses,
    val postableId: PostableId,
    val imageUrls: Array<String?>,
    val iconUrls: Array<String?>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ParsedPostable

        if (postableType != other.postableType) return false
        if (cleanedJson != other.cleanedJson) return false
        if (owningActor != other.owningActor) return false
        if (postingAddresses != other.postingAddresses) return false
        if (postableId != other.postableId) return false
        if (!imageUrls.contentEquals(other.imageUrls)) return false
        if (!iconUrls.contentEquals(other.iconUrls)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = postableType.hashCode()
        result = 31 * result + cleanedJson.hashCode()
        result = 31 * result + owningActor.hashCode()
        result = 31 * result + postingAddresses.hashCode()
        result = 31 * result + postableId.hashCode()
        result = 31 * result + imageUrls.contentHashCode()
        result = 31 * result + iconUrls.contentHashCode()
        return result
    }
}

fun parsePostableActivityPub(
    apString: String
): ParsedPostable {
    val json = JsonParser.parseString(apString).asJsonObject
    val id = json[ID].asString
        ?: throw IllegalStateException("activityPub JSON has no ID")
    val postableType = PostableType.valueOf(json["type"].asString)
    if (json.has("summary")) {
        json["summary"].asString?.let {
            json.addProperty("summary", Jsoup.clean(it, SUMMARY_ALLOWLIST))
        }
    }
    if (json.has("name")) {
        json["name"].asString?.let {
            json.addProperty("name", Jsoup.clean(it, Whitelist.simpleText()))
        }
    }
    if (json.has("content")) {
        json["content"].asString?.let {
            json.addProperty("content", Html.clean(it))
        }
    }
    val actorId = ActorId(
        json["attributedTo"].asString ?: throw java.lang.IllegalStateException("no `attributedTo` property found")
    )
    val to = getPropertyOrSubProperty(json, "to", "id")
    val bto = getPropertyOrSubProperty(json, "bto", "id")
    val cc = getPropertyOrSubProperty(json, "cc", "id")
    val bcc = getPropertyOrSubProperty(json, "bcc", "id")
    val imageUrls = getPropertyOrSubProperty(json, "image", "url")
    val iconUrls = getPropertyOrSubProperty(json, "icon", "url")
    return ParsedPostable(
        postableType = postableType,
        cleanedJson = Gson().toJson(json),
        owningActor = actorId,
        postingAddresses = PostingAddresses(
            to = to,
            cc = cc,
            bto = bto,
            bcc = bcc
        ),
        postableId = PostableId(id),
        imageUrls = imageUrls,
        iconUrls = iconUrls
    )
}

fun getPropertyOrSubProperty(json: JsonObject, property: String, subProperty: String): Array<String?> {
    val addresses = ArrayList<String?>()
    if (json.has(property)) {
        val l = json[property]
        if (l.isJsonArray) {
            l.asJsonArray.forEach { addresses.add(getAsPrimitiveOrProperty(it, subProperty)) }
        } else {
            addresses.add(getAsPrimitiveOrProperty(l, subProperty))
        }
    }
    return addresses.toTypedArray()
}

fun getAsPrimitiveOrProperty(json: JsonElement, name: String): String {
    if (json.isJsonPrimitive) {
        return json.asString
    } else if (json.isJsonObject) {
        return json.asJsonObject[name].asString
    }
    throw IllegalStateException("element is neither primitive nor object containing $name")
}