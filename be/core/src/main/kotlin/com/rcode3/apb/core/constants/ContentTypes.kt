package com.rcode3.apb.core.constants

import io.ktor.http.*

val ACTIVITY_PUB_CONTENT_TYPE = ContentType("application", "activity+json")
val ACTIVITY_STREAMS_TYPE = ContentType("application", "ld+json")
    .withParameter("profile", "https://www.w3.org/ns/activitystreams")
val JRD_CONTENT_TYPE = ContentType("application", "jrd+json")

val XRD_CONTENT_TYPE = ContentType("application", "xrd+xml")