package com.rcode3.apb.core.app

import com.rcode3.apb.core.jobs.SendActivityPubKron
import kotlinx.coroutines.*
import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

class ScheduledJobs(val appContext: AppContext = StandardAppContext()) {

    val sendActivityPubKron = SendActivityPubKron(appContext)

    var started: Boolean = false

    fun start() {
        if (!started) {
            Thread {
                runBlocking {
                    sendActivityPubKron.start()
                }
            }.start()
            logger.info { "Kron started." }
            started = true
        } else {
            logger.warn { "Attempt to start Scheduled Jobs after it has already been started." }
        }
    }
}