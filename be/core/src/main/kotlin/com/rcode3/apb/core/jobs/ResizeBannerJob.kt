package com.rcode3.apb.core.jobs

import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.persistence.FilePersistence
import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.JobConsumer
import com.rcode3.apb.core.app.PersistentJobQueue
import com.rcode3.apb.core.app.StandardAppContext
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.LocalFileUpload
import com.rcode3.apb.jooq.enums.JobType
import com.rcode3.apb.jooq.tables.records.JobQueueRecord
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.geometry.Positions
import org.jooq.Configuration
import java.io.FileOutputStream

@Serializable
data class ResizeBannerJobInfo(
    val fileId: Int,
    val contentType: String,
    val localPath: String,
    val url: String,
    val actorId: ActorId,
)

private val logger = KotlinLogging.logger {}

class ResizeBannerJob(val appContext: AppContext = StandardAppContext()) {

    fun makeFor(info: ResizeBannerJobInfo) {
        val queue = appContext.persistenJobQueue
        queue.insert(JobType.`Resize Actor Banner`, Json.encodeToString(info))
    }

    fun makeFor(tx: Configuration, info: ResizeBannerJobInfo) {
        PersistentJobQueue.insert(tx, JobType.`Resize Actor Banner`, Json.encodeToString(info))
    }

    companion object {
        val WIDTH = 1500
        val HEIGHT = 500

        fun registerConsumer(persistentJobQueue: PersistentJobQueue) {
            persistentJobQueue.registerConsumer(JobType.`Resize Actor Banner`, consumer)
        }

        val consumer: JobConsumer = { tx: Configuration, jobqueueRecord: JobQueueRecord ->
            logger.info { "Resize Banner Job Id: ${jobqueueRecord.id}" }
            val info = Json.decodeFromString<ResizeBannerJobInfo>(jobqueueRecord.details.toString())
            val toFile = LocalFileUpload.getLocalResizePath(info.localPath)
            try {
                Thumbnails
                    .of(info.localPath)
                    .useOriginalFormat()
                    .size(WIDTH, HEIGHT)
                    .crop(Positions.CENTER)
                    .toOutputStream(FileOutputStream(toFile))
                val resizedUrl = LocalFileUpload.getResizeUrl(info.url)
                FilePersistence.addResizedFile(tx, info.fileId, resizedUrl, toFile)
                ActorPersistence.setBanner(tx, info.actorId, resizedUrl)
            } catch (ex: Exception) {
                logger.error { "error resizing banner for job ${jobqueueRecord.id}" }
            }
        }
    }
}