package com.rcode3.apb.core.dto

import com.rcode3.apb.core.logic.ActorId
import kotlinx.serialization.Serializable

@Serializable
data class ArticlePost(
    override val actorId: ActorId,
    override val html: String,
    val localId: String?,
    val fileIds: Array<String>,
    override val postingAddresses: PostingAddresses
) : BasePost{
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArticlePost

        if (actorId != other.actorId) return false
        if (html != other.html) return false
        if (localId != other.localId) return false
        if (!fileIds.contentEquals(other.fileIds)) return false
        if (postingAddresses != other.postingAddresses) return false

        return true
    }

    override fun hashCode(): Int {
        var result = actorId.hashCode()
        result = 31 * result + html.hashCode()
        result = 31 * result + (localId?.hashCode() ?: 0)
        result = 31 * result + fileIds.contentHashCode()
        result = 31 * result + postingAddresses.hashCode()
        return result
    }
}