package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.logic.ActorId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests we don't want to be intermingled with the database stuff because those tests are more expensive.
 */
class NoteIdTests {

    @Test
    fun testMakeNoteId() {
        val id = NotePersistence.makeNoteId(ActorId("testuser"))
        assertEquals(4, id.split(Regex("-")).size)
        assertEquals(2, id.split(Regex(",")).size)
        Assertions.assertTrue(id.startsWith("testuser"))
    }

}