package com.rcode3.apb.core

import com.lectra.koson.obj
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class KosonTests {

    @Test
    fun testMultilineString() {
        val multiline = """
            ------ BEGIN
            blah
            ------ END
        """.trimIndent()
        val json = obj {
            "line" to multiline
        }.toString()
        println(json)
        assertFalse(json.contains("\n"))
    }

    @Test
    fun testNonPretty() {
        val json = obj {
            "1" to 1
            "lslslslsllslsllllslslsl" to "llslslsllsllsllslllslslsl"
            "obj2" to "llslslsllsllsllslllslslsl"
            "obj3" to "llslslsllsllsllslllslslsl"
            "obj4" to "llslslsllsllsllslllslslsl"
            "2" to 2
            "3" to "*******************************************************************************************"
            "4" to "*******************************************************************************************"
            "5" to "*******************************************************************************************"
            "6" to "*******************************************************************************************"
            "7" to "*******************************************************************************************"
        }.toString()
        println(json)
        assertFalse(json.contains("\n"))
    }

    @Test
    fun testOptional() {
        var n: Any? = null
        val json1 = obj {
            "required" to true
            n?.let { "optional" to false }
        }.toString()
        println(json1)
        assertFalse(json1.contains("optional"))
        n = ""
        val json2 = obj {
            "required" to true
            n.let { "optional" to false }
        }.toString()
        println(json2)
        assertTrue(json2.contains("optional"))
    }
}