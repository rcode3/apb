package com.rcode3.apb.core.persistence

import com.google.gson.JsonParser
import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.constants.ACTIVITY_PUB_JSON
import com.rcode3.apb.core.constants.FILESTORAGE_DIRECTORY
import com.rcode3.apb.core.constants.FILESTORAGE_URLPATH
import com.rcode3.apb.core.constants.MODIFIED_AT
import com.rcode3.apb.core.dto.ActorNameSummary
import com.rcode3.apb.core.dto.NewActor
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.core.util.PG_DATE_NEGATIVE_INFINITY
import com.rcode3.apb.core.util.PG_DATE_POSITIVE_INFINITY
import com.rcode3.apb.jooq.enums.ActorType
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.file.Path
import java.time.OffsetDateTime

class ActorPersistenceTests {
    private lateinit var appContext: AppContext
    private lateinit var tempDir: Path
    private lateinit var systemAssets: SystemAssets
    private val username1 = Username("testy_mc_test_alot")
    private val username2 = Username("the_fooz_bazzer")
    private val preferredUsername1 = PreferredUsername(username1.toString())
    private val preferredUsername2 = PreferredUsername(username2.toString())
    private val owner1 = Username(username1.toString())
    private val owner2 = Username(username2.toString())

    @BeforeEach
    fun setup() {
        val configMap = HashMap<String, Any>()
        tempDir = kotlin.io.path.createTempDirectory("system_assets_tests")
        configMap.put(FILESTORAGE_DIRECTORY, tempDir.toAbsolutePath().toString())
        configMap.put(FILESTORAGE_URLPATH, "/media")
        systemAssets = SystemAssets(TestAppContext(configMap = configMap).config)
        tempDir.toFile().mkdirs()
        systemAssets.mkdirs()
        tempDir.resolve(systemAssets.ICON_DIR).resolve("foo.png").toFile().createNewFile()
        tempDir.resolve(systemAssets.BANNER_DIR).resolve("foo.png").toFile().createNewFile()
        appContext = TestAppContext(configMap = configMap)

        val userActions = UserPersistence(appContext)
        var newUser = NewUserRegistration(
            name = username1.toString(),
            email = "testy@example.com",
            plainTextPassword = "a plain text password",
            status = UserPrincipalStatusType.Active
        )
        userActions.registerNewUser(newUser)
        newUser = NewUserRegistration(
            name = username2.toString(),
            email = "foo@example.com",
            plainTextPassword = "a plain text password",
            status = UserPrincipalStatusType.Active
        )
        userActions.registerNewUser(newUser)
    }

    @AfterEach
    fun tearDown() {
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
        tempDir.toFile().deleteRecursively()
    }

    @Test
    fun `test SetVisibility`() {
        val actions = ActorPersistence(appContext)
        val actorId = PersonPersistence.makePersonId(preferredUsername1, appContext.config)
        actions.setVisibility(actorId, owner1, ActorVisibilityType.Invisible)
        val person = actions.fetchActorByIdAndOwner(actorId, owner1)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        assertEquals(ActorVisibilityType.Invisible, person!!.visibility)
    }

    @Test
    fun `test SetNameSummary`() {
        val actions = ActorPersistence(appContext)
        val name = "Testy McTest-A-Lot"
        val summary = "I am a test user."
        val actorId = PersonPersistence.makePersonId(preferredUsername1, appContext.config)
        actions.setNameSummary(
            actorId, owner1,
            ActorNameSummary(name, summary)
        )
        val person = actions.fetchActorByIdAndOwner(actorId, owner1)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        val ap = json[ACTIVITY_PUB_JSON].asJsonObject
        assertNotNull(ap)
        assertEquals(name, ap["name"].asString)
        assertEquals(summary, ap["summary"].asString)
    }

    @Test
    fun `test SetBadNameSummary`() {
        val actions = ActorPersistence(appContext)
        val displayName = "Testy 'McTest-A-Lot'"
        val summary = "I am a test user<script>console.log(\"injection attack\")</script>."
        val actorId = PersonPersistence.makePersonId(preferredUsername1, appContext.config)
        actions.setNameSummary(
            actorId, owner1,
            ActorNameSummary(displayName, summary)
        )
        val person = actions.fetchActorByIdAndOwner(actorId, owner1)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        val ap = json[ACTIVITY_PUB_JSON].asJsonObject
        assertNotNull(ap)
        assertNotEquals(displayName, ap["name"].asString)
        assertNotEquals(summary, ap["summary"].asString)
    }

    @Test
    fun `test FetchPersonActor`() {
        val actions = ActorPersistence(appContext)
        val actorId = PersonPersistence.makePersonId(preferredUsername1, appContext.config)
        val personActor = actions.fetchActorById(actorId)
        assertEquals(1, personActor!!.owners.size)
        assertEquals(owner1.toString(), personActor.owners[0])
    }

    @Test
    fun `test FetchActors`() {
        val actorActions = ActorPersistence(appContext)
        val personActions = PersonPersistence(appContext)
        for (i in 1..20) {
            personActions.createNewPersonActor(NewActor("fetchActorsTest_$i"), owner1)
        }

        var nextDate: OffsetDateTime? = null
        var count = 0
        for (j in 1..6) {
            val persons = actorActions.fetchActors(nextDate, 5, ActorType.Person)
            val json = JsonParser.parseString(persons).asJsonArray
            count += json.size()
            if (json.size() > 0) {
                nextDate = OffsetDateTime.parse(json.last().asJsonObject[MODIFIED_AT].asString)
            }
        }
        // 20 personas plust 2 users
        assertEquals(22, count)
    }

    @Test
    fun `test FetchAllActorsByOwner`() {
        val actorActions = ActorPersistence(appContext)
        val personActions = PersonPersistence(appContext)

        for (i in 1..5) {
            personActions.createNewPersonActor(NewActor("fetchAllActorsByOwnerTest_1_$i"), owner1)
        }
        for (i in 1..7) {
            personActions.createNewPersonActor(NewActor("fetchAllActorsByOwnerTest_2_$i"), owner2)
        }

        var actors = actorActions.fetchAllActorsByOwner(username1)
        //5 actors + the one created in new user registration
        assertEquals(6, actors.size)

        actors = actorActions.fetchAllActorsByOwner(username2)
        //7 actors + the one created in new user registration
        assertEquals(8, actors.size)

        var persons = actorActions.fetchAllActorsByOwner(username1, ActorType.Person)
        //5 personas + the one created in new user registration
        assertEquals(6, persons.size)

        persons = actorActions.fetchAllActorsByOwner(username2, ActorType.Person)
        //7 personas + the one created in new user registration
        assertEquals(8, persons.size)
    }

    @Test
    fun `test saving an actor without webfinger`() {
        val id = "https://example.com/actor/example"
        val actorInfo = ActorPersistence(appContext).saveRemoteActor(
            ActorId(id),
            ActivityPubJson("""
                {"id":"$id"}
            """.trimIndent()),
            "-----BEGIN PUBLIC KEY-----aldfadoououou-----END PUBLIC KEY-----",
            ActorType.Person,
            ActorVisibilityType.Visible
        )
        actorInfo.actorId shouldBe id
    }

    @Test
    fun `test getting inboxes`() {
        val actorIds = arrayOf(
            PersonPersistence.makePersonId(preferredUsername1, appContext.config),
            PersonPersistence.makePersonId(preferredUsername2, appContext.config)
        )
        val inboxes = ActorPersistence(appContext).fetchInboxes(actorIds)
        assertEquals(2, inboxes.size)
        assertTrue(inboxes[0].endsWith("sharedInbox"))
        assertTrue(inboxes[1].endsWith("sharedInbox"))
    }

    @Test
    fun `test getting key info`() {
        val actorId = PersonPersistence.makePersonId(preferredUsername1, appContext.config)
        val keyInfo = ActorPersistence(appContext).fetchKeyInfo(actorId)
        assertTrue(keyInfo!!.privateKeyPem!!.startsWith("-----"))
        assertTrue(keyInfo!!.publicKeyPem.startsWith("-----"))
        assertTrue(keyInfo.publicKeyId.startsWith(appContext.common.apBaseUrl))
    }

    @Test
    fun `fetch actors in forward paging`() {
        val actors = ActorPersistence(appContext)
        val persons = PersonPersistence(appContext)
        for (i in 1..20) {
            persons.createNewPersonActor(NewActor("fetchActorsTest_$i"), owner1)
        }

        var nextDate = PG_DATE_POSITIVE_INFINITY
        var count = 0
        for (j in 1..6) {
            val actorPage = actors.fetchActorPage(
                prevDate = null,
                nextDate = nextDate,
                limit = 6,
                actorType = null
            )
            count += actorPage.size
            if (actorPage.size > 0) {
                nextDate = actorPage.last().modifiedAt
            } else {
                break
            }
        }

        count shouldBe 22
    }

    @Test
    fun `fetch actors paging backwards`() {
        val actors = ActorPersistence(appContext)
        val persons = PersonPersistence(appContext)
        for (i in 1..20) {
            persons.createNewPersonActor(NewActor("fetchActorsTest_$i"), owner1)
        }

        val firstPage = actors.fetchActorPage(
            prevDate = null,
            nextDate = PG_DATE_POSITIVE_INFINITY,
            limit = 6,
            actorType = null
        )
        firstPage.size shouldBe 6

        val secondPage = actors.fetchActorPage(
            prevDate = null,
            nextDate = firstPage.last().modifiedAt,
            limit = 6,
            actorType = null
        )
        secondPage.size shouldBe 6

        val firstPageAgain = actors.fetchActorPage(
            prevDate = secondPage.first().modifiedAt,
            nextDate = null,
            limit = 6,
            actorType = null
        )
        firstPageAgain.size shouldBe 6
        firstPageAgain shouldBe firstPage
    }
}