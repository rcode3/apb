package com.rcode3.apb.core.persistence

import com.google.gson.JsonParser
import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.constants.*
import com.rcode3.apb.core.dto.ActorNameSummary
import com.rcode3.apb.core.dto.NewActor
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.logic.SystemAssets
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.ActorVisibilityType
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import org.jooq.exception.DataAccessException
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.file.Path
import kotlin.io.path.ExperimentalPathApi

@ExperimentalPathApi
class PersonPersistenceTests {
    private lateinit var appContext: AppContext
    private lateinit var tempDir: Path
    private lateinit var systemAssets: SystemAssets
    private val username = Username("testy_mc_test_alot")
    private val preferredUsername = PreferredUsername(username.toString())
    private val owner = Username(username.toString())
    private var actorId: ActorId? = null

    @BeforeEach
    fun setup() {
        val configMap = HashMap<String, Any>()
        tempDir = kotlin.io.path.createTempDirectory("system_assets_tests")
        configMap[FILESTORAGE_DIRECTORY] = tempDir.toAbsolutePath().toString()
        configMap[FILESTORAGE_URLPATH] = "/media"
        systemAssets = SystemAssets(TestAppContext(configMap = configMap).config)
        tempDir.toFile().mkdirs()
        systemAssets.mkdirs()
        tempDir.resolve(systemAssets.ICON_DIR).resolve("foo.png").toFile().createNewFile()
        tempDir.resolve(systemAssets.BANNER_DIR).resolve("foo.png").toFile().createNewFile()
        appContext = TestAppContext(configMap = configMap)

        val userActions = UserPersistence(appContext)
        val newUser = NewUserRegistration(
            name = username.toString(),
            email = "testy@example.com",
            plainTextPassword = "a plain text password",
            status = UserPrincipalStatusType.Active
        )
        val result = userActions.registerNewUser(newUser)
        actorId = result.second
    }

    @AfterEach
    fun tearDown() {
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
        tempDir.toFile().deleteRecursively()
    }


    @Test
    fun `test fetching a person`() {
        val actions = PersonPersistence(appContext)
        val person = actions.fetchPersonByPunAndOwner(preferredUsername, owner)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        assertEquals("Visible", person!!.visibility.literal)
        assertNotNull(json[ACTIVITY_PUB_JSON].asJsonObject)
        assertEquals(actorId.toString(), json[ACTIVITY_PUB_JSON].asJsonObject[ID].asString)
    }

    @Test
    fun `test fetching a second person by PUN and owner`() {
        val actions = PersonPersistence(appContext)
        val persona = PreferredUsername("${preferredUsername}_other")
        val otherActorId = actions.createNewPersonActor(NewActor(persona.toString()), owner)
        val person = actions.fetchPersonByPunAndOwner(persona, owner)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        assertEquals(ActorVisibilityType.Visible, person!!.visibility)
        assertNotNull(json[ACTIVITY_PUB_JSON].asJsonObject)
        assertEquals(otherActorId.toString(), json[ACTIVITY_PUB_JSON].asJsonObject[ID].asString)
    }

    @Test
    fun `test cannot have more than one default actor`() {
        val actions = PersonPersistence(appContext)
        val persona = PreferredUsername("${preferredUsername}_otherdefault")
        assertThrows(DataAccessException::class.java) {
            actions.createNewPersonActor(NewActor(persona.toString()), owner, true)
        }
    }

    @Test
    fun `test set person as default`() {
        val actions = PersonPersistence(appContext)
        val persona = PreferredUsername("${preferredUsername}_othertobedefault")
        actions.createNewPersonActor(NewActor(persona.toString()), owner, false)
        actions.setAsDefaultActor(persona, username)
        assertFalse(actions.fetchPersonByPunAndOwner(preferredUsername, username)!!.defaultActor)
        assertTrue(actions.fetchPersonByPunAndOwner(persona, username)!!.defaultActor)

        val firstInfo = actions.fetchPersonByPunAndOwner(preferredUsername, username)
        val firstJson = JsonParser.parseString(firstInfo!!.returnedJson).asJsonObject
        assertFalse(firstJson[DEFAULT_ACTOR].asBoolean)

        val otherInfo = actions.fetchPersonByPunAndOwner(persona, username)
        val otherJson = JsonParser.parseString(otherInfo!!.returnedJson).asJsonObject
        assertTrue(otherJson[DEFAULT_ACTOR].asBoolean)
    }

    @Test
    fun `test setting visibility`() {
        val actions = PersonPersistence(appContext)
        actions.setVisibility(preferredUsername, owner, ActorVisibilityType.Invisible)
        val person = ActorPersistence(appContext).fetchActorByIdAndOwner(actions.makePersonId(preferredUsername), owner)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        assertEquals(ActorVisibilityType.Invisible, person!!.visibility)
        assertEquals(ActorVisibilityType.Invisible.literal, json[VISIBILITY].asString)
    }

    @Test
    fun `test setting name and summary`() {
        val actions = PersonPersistence(appContext)
        val name = "Testy McTest-A-Lot"
        val summary = "I am a test user."
        actions.setNameSummary(
            preferredUsername, owner,
            ActorNameSummary(name, summary)
        )
        val person = actions.fetchPersonByPunAndOwner(preferredUsername, owner)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        val ap = json[ACTIVITY_PUB_JSON].asJsonObject
        assertNotNull(ap)
        assertEquals(name, ap["name"].asString)
        assertEquals(summary, ap["summary"].asString)
    }

    @Test
    fun `test that a bad name or summary cannot be persisted`() {
        val actions = PersonPersistence(appContext)
        val name = "Testy 'McTest-A-Lot'"
        val summary = "I am a test user<script>console.log(\"injection attack\")</script>."
        actions.setNameSummary(
            preferredUsername, owner,
            ActorNameSummary(name, summary)
        )
        val person = actions.fetchPersonByPunAndOwner(preferredUsername, owner)
        val json = JsonParser.parseString(person?.returnedJson).asJsonObject
        val ap = json[ACTIVITY_PUB_JSON].asJsonObject
        assertNotNull(ap)
        assertNotEquals(name, ap["name"].asString)
        assertNotEquals(summary, ap["summary"].asString)
    }

}