package com.rcode3.apb.core.jooq

import com.rcode3.apb.core.app.EmbeddedPostgresDataSourceFactory
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.time.Instant.now

class UserPrincipalsTests {

    @Test
    fun testInsertUserPrincipal() {
        val testUserName = "testInsertUserPrincipal" + now().toString().hashCode().toString()

        val ctx = EmbeddedPostgresDataSourceFactory().jooqDsl()
        ctx
            .insertInto(USER_PRINCIPAL, USER_PRINCIPAL.NAME)
            .values(testUserName)
            .execute()

        val up1 = ctx.selectFrom(USER_PRINCIPAL).where(USER_PRINCIPAL.NAME.eq(testUserName)).fetch().first()
        assertNotNull(up1.createdAt)
        assertNotNull(up1.modifiedAt)

        ctx.update(USER_PRINCIPAL).set(USER_PRINCIPAL.NAME, testUserName).where(USER_PRINCIPAL.NAME.eq(testUserName))
            .execute()

        val up2 = ctx.selectFrom(USER_PRINCIPAL).where(USER_PRINCIPAL.NAME.eq(testUserName)).fetch().first()
        assertNotEquals(up2.createdAt, up2.modifiedAt)
    }
}