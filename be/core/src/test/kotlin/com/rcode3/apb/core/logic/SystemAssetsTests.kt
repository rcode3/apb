package com.rcode3.apb.core.logic

import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.constants.FILESTORAGE_DIRECTORY
import com.rcode3.apb.core.constants.FILESTORAGE_URLPATH
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import kotlin.io.path.ExperimentalPathApi

@ExperimentalPathApi
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class SystemAssetsTests {

    val configMap = HashMap<String, Any>()
    val tempDir = kotlin.io.path.createTempDirectory("system_assets_tests")
    lateinit var systemAssets: SystemAssets

    @BeforeAll
    fun beforeAll() {
        tempDir.toFile().mkdirs()
        configMap.put(FILESTORAGE_DIRECTORY, tempDir.toAbsolutePath().toString())
        configMap.put(FILESTORAGE_URLPATH, "/media")
        systemAssets = SystemAssets(TestAppContext(configMap = configMap).config)
    }

    @AfterAll
    fun afterAll() {
        tempDir.toFile().deleteRecursively()
    }

    @Test
    @Order(1)
    fun testMkDirs() {
        systemAssets.mkdirs()
    }

    @Test
    @Order(2)
    fun testMakeBannerUrlWithGoodInput() {
        assertEquals("https://testhost.xyz/media/system/banners/foo.png", systemAssets.makeBannerUrl("foo.png"))
    }

    @Test
    @Order(3)
    fun testMakeBannerUrlWithBadInput() {
        assertEquals("https://testhost.xyz/media/system/banners/..foo.png", systemAssets.makeBannerUrl("../foo.png"))
    }

    @Test
    @Order(4)
    fun testGetEmptyIconUrls() {
        assertEquals(0, systemAssets.getIconAssets().size)
    }

    @Test
    @Order(5)
    fun testGetEmptyBannerUrls() {
        assertEquals(0, systemAssets.getBannerAssets().size)
    }

    @Test
    @Order(6)
    fun testGetRandomIconUrlWhenEmpty() {
        assertNull(systemAssets.getRandomIconAsset())
    }

    @Test
    @Order(7)
    fun testGetRandomBannerUrlWhenEmpty() {
        assertNull(systemAssets.getRandomBannerAsset())
    }

    @Test
    @Order(8)
    fun testGetIconUrls() {
        val p = tempDir.resolve(systemAssets.ICON_DIR).resolve("foo.png")
        p.toFile().createNewFile()
        assertEquals(1, systemAssets.getIconAssets().size)
    }

    @Test
    @Order(9)
    fun testGetBannerUrls() {
        val p = tempDir.resolve(systemAssets.BANNER_DIR).resolve("foo.png")
        p.toFile().createNewFile()
        assertEquals(1, systemAssets.getBannerAssets().size)
    }

    @Test
    @Order(10)
    fun testGetRandomIconUrl() {
        assertNotNull(systemAssets.getRandomIconAsset())
    }

    @Test
    @Order(11)
    fun testGetRandomBannerUrl() {
        assertNotNull(systemAssets.getRandomBannerAsset())
    }
}