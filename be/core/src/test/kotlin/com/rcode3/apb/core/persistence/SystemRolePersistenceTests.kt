package com.rcode3.apb.core.persistence

import com.google.gson.JsonParser
import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.constants.ROLE_TYPE
import com.rcode3.apb.core.constants.SYSTEM_ROLES
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.SystemRoleType
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.SYSTEM_ROLE
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue

@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SystemRolePersistenceTests {
    private val userA = Username("userA")
    private val userB = Username("userB")
    private val userC = Username("userC")

    private val appContext = TestAppContext()
    private val userActions = UserPersistence(appContext)
    private val systemRoleActions = SystemRolePersistence(appContext)

    @BeforeAll
    fun beforeAll() {
        userActions.registerNewUser(
            NewUserRegistration(userA.toString(), "$userA@example.com", "secretpassword", UserPrincipalStatusType.Active)
        )
        userActions.registerNewUser(
            NewUserRegistration(userB.toString(), "$userB@example.com", "secretpassword", UserPrincipalStatusType.Active)
        )
        userActions.registerNewUser(
            NewUserRegistration(userC.toString(), "$userC@example.com", "secretpassword", UserPrincipalStatusType.Active)
        )
    }

    @AfterAll
    fun afterAll() {
        appContext.dsf.jooqDsl().truncate(SYSTEM_ROLE).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
    }

    @Test
    @Order(1)
    fun `test make userA a sysop`() {
        val json = systemRoleActions.addToRole(userA, SystemRoleType.`System Operator`)
        assertTrue(isMemberSysopInJson(json, userA))
        assertTrue(isInRole(userA, SystemRoleType.`System Operator`))

        val userJson = userActions.getUserInfo(userA)
        assertTrue(userIsInRole(userJson!!, SystemRoleType.`System Operator`))
    }

    @Test
    @Order(2)
    fun `test make userB a sysop then remove userB from sysop`() {
        systemRoleActions.addToRole(userB, SystemRoleType.`System Operator`)
        val json = systemRoleActions.removeFromRole(userB, SystemRoleType.`System Operator`)
        assertTrue(isMemberSysopInJson(json, userB))
        assertFalse(isInRole(userB, SystemRoleType.`System Operator`))

        val userJson = userActions.getUserInfo(userB)
        assertFalse(userIsInRole(userJson!!, SystemRoleType.`System Operator`))
    }

    @Test
    @Order(3)
    fun `test make userC a sysop`() {
        val json = systemRoleActions.addToRole(userC, SystemRoleType.`System Operator`)
        assertTrue(isMemberSysopInJson(json, userC))
        assertTrue(isInRole(userC, SystemRoleType.`System Operator`))
    }

    @Test
    @Order(4)
    fun `test make userC a moderator`() {
        systemRoleActions.addToRole(userC, SystemRoleType.`System Moderator`)
        assertTrue(isInRole(userC, SystemRoleType.`System Moderator`))
    }

    @Test
    @Order(5)
    fun `test remove userC as moderator`() {
        systemRoleActions.removeFromRole(userC, SystemRoleType.`System Moderator`)
        assertFalse(isInRole(userC, SystemRoleType.`System Moderator`))
        assertTrue(isInRole(userC, SystemRoleType.`System Operator`))
    }

    @Test
    @Order(6)
    fun `test remove userC as sysop`() {
        systemRoleActions.removeFromRole(userC, SystemRoleType.`System Operator`)
        assertFalse(isInRole(userC, SystemRoleType.`System Moderator`))
        assertFalse(isInRole(userC, SystemRoleType.`System Operator`))
    }

    private fun isInRole(user: Username, role: SystemRoleType): Boolean {
        val json = systemRoleActions.getUsersInRole(role)
        val userList = JsonParser.parseString(json).asJsonArray
        userList.forEach {
            val u = it.asJsonObject
            if (u.has("member") && u["member"].asString.equals(user.toString())) {
                return true
            }
        }
        return false
    }

    private fun isMemberSysopInJson(json: String?, member: Username): Boolean {
        json ?: return false
        val o = JsonParser.parseString(json).asJsonObject
        if (o["member"].asString.equals(member.toString()) && o[ROLE_TYPE].asString.equals(SystemRoleType.`System Operator`.literal)) {
            return true
        }
        //else
        return false
    }

    private fun userIsInRole(json: String, role: SystemRoleType): Boolean {
        val o = JsonParser.parseString(json).asJsonObject
        val roles = o[SYSTEM_ROLES].asJsonArray
        roles.forEach {
            val e = it.asString
            if (e.equals(role.literal)) {
                return true
            }
        }
        return false
    }
}