package com.rcode3.apb.core

import org.jooq.CloseableDSLContext
import org.jooq.Configuration
import org.jooq.ConnectionProvider
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.jooq.impl.DefaultConfiguration
import org.jooq.impl.DefaultConnectionProvider
import java.sql.Connection
import java.sql.DriverManager
import java.util.*


private val testUrl = "testUrl"

private val testDbUser = "testDbUser"

private val testDbPw = "testDbPw"

fun testDslContext(): CloseableDSLContext {
    return DSL.using(
        System.getProperty(testUrl),
        System.getProperty(testDbUser),
        System.getProperty(testDbPw)
    )
}

fun testJooqConfiguration(connectionProvider: ConnectionProvider = testConnectionProvider()): Configuration {
    return DefaultConfiguration().set(connectionProvider).set(SQLDialect.POSTGRES)
}

fun testConnectionProvider(): DefaultConnectionProvider {
    Class.forName("org.postgresql.Driver")
    val url = System.getProperty(testUrl)
    val props = Properties()
    props.setProperty("user", System.getProperty(testDbUser))
    props.setProperty("password", System.getProperty(testDbPw))
    val conn: Connection = DriverManager.getConnection(url, props)
    return DefaultConnectionProvider(conn)
}

