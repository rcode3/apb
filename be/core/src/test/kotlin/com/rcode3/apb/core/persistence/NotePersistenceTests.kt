package com.rcode3.apb.core.persistence

import com.google.gson.JsonParser
import com.rcode3.apb.core.Jigs
import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.constants.ACCT
import com.rcode3.apb.core.constants.ACTIVITY_PUB_JSON
import com.rcode3.apb.core.constants.ACTOR_ID
import com.rcode3.apb.core.constants.ID
import com.rcode3.apb.core.dto.NotePost
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.PostableId
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.logic.punFromActorId
import com.rcode3.apb.jooq.tables.references.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test


class NotePersistenceTests {
    private val appContext = TestAppContext()
    private val jigs by lazy {
        Jigs(appContext)
    }
    private val notes by lazy {
        NotePersistence(appContext)
    }
    private val postables by lazy {
        PostablePersistence(appContext)
    }

    private var testUser1: Pair<Username, ActorId>? = null
    private var testUser2: Pair<Username, ActorId>? = null
    private var person1: ActorId? = null
    private var postingAddresses: PostingAddresses? = null

    @BeforeEach
    fun setup() {
        testUser1 = jigs.createTestUser()
        testUser2 = jigs.createTestUser()
        person1 = jigs.createPerson(testUser1!!.first)
        postingAddresses = PostingAddresses(
            to = arrayOf(testUser1!!.second.toString()),
            cc = arrayOf(testUser2!!.second.toString()),
            bto = arrayOf(),
            bcc = arrayOf()
        )
    }

    @AfterEach
    fun tearDown() {
        appContext.dsf.jooqDsl().truncate(POSTABLE).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
    }

    @Test
    fun `test Create Note As Users Person`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNotNull(noteResponse)
        val json = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        assertTrue(json[ACTOR_ID].asString.contains(punFromActorId(testUser1!!.second).toString()))
        println(noteResponse)
    }

    @Test
    fun `test Create Note As Users Other Person`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = person1!!, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNotNull(noteResponse)
        val json = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        assertTrue(json[ACTOR_ID].asString.contains(punFromActorId(person1!!).toString()))
    }

    @Test
    fun `test Fail To Create Note As Other Person`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser2!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNull(noteResponse)
    }

    @Test
    fun `test Fail To Create Note As Users Other Person`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = person1!!, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser2!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNull(noteResponse)
    }

    @Test
    fun `test Fail To Create Note As Suspended User`() {
        jigs.suspendUser(testUser1!!.first)
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNull(noteResponse)
    }

    @Test
    fun `test Fetch a Note`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNotNull(noteResponse)
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject

        val postableFetchResponse = postables.fetchPostable(PostableId(noteJson[ID].asString))
        assertNotNull(postableFetchResponse)
        val json = JsonParser.parseString(postableFetchResponse).asJsonObject
        assertEquals(noteJson[ACTOR_ID].asString,
            json[com.rcode3.apb.core.constants.ACTOR]
                .asJsonObject[ACTIVITY_PUB_JSON]
                .asJsonObject[ID]
                .asString
        )
        assertNotNull(
            json[com.rcode3.apb.core.constants.ACTOR]
                .asJsonObject[ACCT]
        )
        assertEquals(noteJson[ID].asString,
            json[com.rcode3.apb.core.constants.POSTABLE]
                .asJsonObject[ACTIVITY_PUB_JSON]
                .asJsonObject[ID]
                .asString
        )
    }
}