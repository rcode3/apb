package com.rcode3.apb.core.util

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime

class TimestampTests {

    @Test
    fun `test HTTP time formatting for day of month less than 10`() {
        val testDate = OffsetDateTime.parse("2021-08-02T07:08:00Z")
        HTTP_DATE_TIME_FORMATTER.format(testDate) shouldBe "Mon, 02 Aug 2021 07:08:00 GMT"
    }

    @Test
    fun `test HTTP time formatting for day of month greater than 10`() {
        val testDate = OffsetDateTime.parse("2021-08-12T07:08:00Z")
        HTTP_DATE_TIME_FORMATTER.format(testDate) shouldBe "Thu, 12 Aug 2021 07:08:00 GMT"
    }

    @Test
    fun `test XSD time date with UTC`() {
        val testDate = OffsetDateTime.parse("2021-08-02T07:08:00Z")
        XSD_DATE_TIME_FORMATTER.format(testDate) shouldBe "2021-08-02T07:08:00Z"
    }

    @Test
    fun `test XSD time date with offset`() {
        val testDate = OffsetDateTime.parse("2021-08-02T07:08:00+04:00")
        XSD_DATE_TIME_FORMATTER.format(testDate) shouldBe "2021-08-02T03:08:00Z"
    }
}