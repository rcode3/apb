package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.punFromActorId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * Tests for things that make the articles JSON. This is separate from the rest of the action tests
 * because these do not need to reset the database everytime.
 */
class ArticleIdTests {

    val TEST_ACTOR = ActorId("https://example.com/pub/p/test_actor")

    @Test
    fun `test makeArticleId with no data`() {
        val id = ArticlePersistence.makeArticleId(null, null, null)
        assertEquals(4, id.split(Regex("-")).size)
    }

    @Test
    fun `test makeArticleId with just ActorId`() {
        val id = ArticlePersistence.makeArticleId(null, null, TEST_ACTOR)
        assertEquals(4, id.split(Regex("-")).size)
        assertEquals(2, id.split(Regex(",")).size)
        assertTrue(id.startsWith(punFromActorId(TEST_ACTOR).toString()))
    }

    @Test
    fun `test makeArticleId with suggested local id that needs to changes`() {
        val id = ArticlePersistence.makeArticleId("test-id", null, null)
        assertEquals("test-id", id)
    }

    @Test
    fun `test makeArticleId with suggested local id that has uppercase character`() {
        val id = ArticlePersistence.makeArticleId("test-Id", null, null)
        assertEquals("test-id", id)
    }

    @Test
    fun `test makeArticleId suggested local id that has underscore`() {
        val id = ArticlePersistence.makeArticleId("test_Id", null, null)
        assertEquals("test-id", id)
    }

    @Test
    fun `test makeArticleId with suggested local id that has a space`() {
        val id = ArticlePersistence.makeArticleId("test Id", null, null)
        assertEquals("test-id", id)
    }

    @Test
    fun `test makeArticleId with suggested local id that needs no changes and a ActorId`() {
        val id = ArticlePersistence.makeArticleId("test-id", null, TEST_ACTOR)
        assertEquals("test_actor,test-id", id)
    }

    @Test
    fun `test makeArticleId with a suggested local id with uppercase char and a ActorId`() {
        val id = ArticlePersistence.makeArticleId("test-Id", null, TEST_ACTOR)
        assertEquals("test_actor,test-id", id)
    }

    @Test
    fun `test makeArticleId with a suggested id that has underscore and a ActorId`() {
        val id = ArticlePersistence.makeArticleId("test_Id", null, TEST_ACTOR)
        assertEquals("test_actor,test-id", id)
    }

    @Test
    fun `test makeArticleId with a suggested id that has a space and a ActorId`() {
        val id = ArticlePersistence.makeArticleId("test Id", null, TEST_ACTOR)
        assertEquals("test_actor,test-id", id)
    }

    @Test
    fun `test makeArticleId with an html title that needs no changes`() {
        val id = ArticlePersistence.makeArticleId(null, "test id", null)
        assertEquals("test-id", id)
    }

    @Test
    fun `test makeArticleId with an html title that has an uppercase char`() {
        val id = ArticlePersistence.makeArticleId(null, "test ID", null)
        assertEquals("test-id", id)
    }

    @Test
    fun `test makeArticleId with an html title that has an underscore char`() {
        val id = ArticlePersistence.makeArticleId(null, "test_id", null)
        assertEquals("test-id", id)
    }

    @Test
    fun `test makeArticleId with an html title needing no changes and a ActorId`() {
        val id = ArticlePersistence.makeArticleId(null, "test id", TEST_ACTOR)
        assertEquals("test_actor,test-id", id)
    }

    @Test
    fun `test makeArticleId with an html title with an uppercase char and a ActorId`() {
        val id = ArticlePersistence.makeArticleId(null, "test ID", TEST_ACTOR)
        assertEquals("test_actor,test-id", id)
    }

    @Test
    fun `test makeArticleId with an html title with an underscore char and a ActorId`() {
        val id = ArticlePersistence.makeArticleId(null, "test_id", TEST_ACTOR)
        assertEquals("test_actor,test-id", id)
    }
}