package com.rcode3.apb.core

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.app.DataSourceFactory
import org.jooq.impl.DefaultConfiguration
import org.jooq.tools.jdbc.MockConnection
import org.jooq.tools.jdbc.MockDataProvider
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.sql.Connection

fun mockDsf(provider: MockDataProvider): DataSourceFactory {
    val connection: Connection = MockConnection(provider)
    val configuration = DefaultConfiguration().derive(connection)
    return mock<DataSourceFactory> {
        on { jooqDsl() } doReturn configuration.dsl()
    }
}

fun mockAppContext(dataSourceFactory: DataSourceFactory): AppContext {
    return mock<AppContext> {
        on { dsf } doReturn dataSourceFactory
    }
}