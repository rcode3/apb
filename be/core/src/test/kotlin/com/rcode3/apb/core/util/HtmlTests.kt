package com.rcode3.apb.core.util

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class HtmlTests {
    @Test
    fun testFindTitleWithTitle() {
        Assertions.assertEquals(
            "foo",
            Html.findTitle(
                """
                <html>
                <head>
                <title>foo</title>
                </head>
                <body>
                bar
                </body>
                </html>
            """.trimIndent()
            )
        )
    }

    @Test
    fun testFindTitleWithH1() {
        Assertions.assertEquals(
            "foo",
            Html.findTitle(
                """
                <html>
                <body>
                <h1>foo</h1>
                bar
                </body>
                </html>
            """.trimIndent()
            )
        )
    }

    @Test
    fun testFindTitleWithP() {
        Assertions.assertEquals(
            "foo",
            Html.findTitle(
                """
                <html>
                <body>
                <p>foo</p>
                bar
                </body>
                </html>
            """.trimIndent()
            )
        )
    }
}