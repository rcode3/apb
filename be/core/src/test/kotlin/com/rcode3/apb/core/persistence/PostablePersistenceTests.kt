package com.rcode3.apb.core.persistence

import com.google.gson.JsonParser
import com.rcode3.apb.core.Jigs
import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.constants.ID
import com.rcode3.apb.core.dto.NotePost
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.PostableId
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.util.PG_DATE_NEGATIVE_INFINITY
import com.rcode3.apb.core.util.PG_DATE_POSITIVE_INFINITY
import com.rcode3.apb.jooq.enums.PostableType
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.POSTABLE
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime

class PostablePersistenceTests {
    private val appContext = TestAppContext()
    private val jigs by lazy {
        Jigs(appContext)
    }
    private val notes by lazy {
        NotePersistence(appContext)
    }
    private val postables by lazy {
        PostablePersistence(appContext)
    }

    private var testUser1: Pair<Username, ActorId>? = null
    private var testUser2: Pair<Username, ActorId>? = null
    private var person1: ActorId? = null
    private var postingAddresses: PostingAddresses? = null

    @BeforeEach
    fun setup() {
        testUser1 = jigs.createTestUser()
        testUser2 = jigs.createTestUser()
        person1 = jigs.createPerson(testUser1!!.first)
        postingAddresses = PostingAddresses(
            to = arrayOf(person1.toString()),
            cc = arrayOf(),
            bto = arrayOf(),
            bcc = arrayOf()
        )
    }
    @AfterEach
    fun tearDown() {
        appContext.dsf.jooqDsl().truncate(POSTABLE).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
    }

    @Test
    fun `fetch a postable and check its id`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        noteJson[ID].asString shouldNotBe null
        val fetch = postables.fetchPostablePage(nextDate = PG_DATE_POSITIVE_INFINITY, prevDate = null, limit = 2, fromActorId = null, toActorId = null, postableType = null)
        fetch.size shouldBe 1
        val fetchJson = JsonParser.parseString(fetch.first().activityPubJson).asJsonObject
        fetchJson[ID] shouldNotBe null
        fetchJson[ID].asString shouldBe noteJson[ID].asString
    }

    @Test
    fun `fetch a note and check its id`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        noteJson[ID].asString shouldNotBe null
        val fetch = postables.fetchPostablePage(nextDate = PG_DATE_POSITIVE_INFINITY, prevDate = null, limit = 2, fromActorId = null, toActorId = null, postableType = PostableType.Note)
        fetch.size shouldBe 1
        val fetchJson = JsonParser.parseString(fetch.first().activityPubJson).asJsonObject
        fetchJson[ID] shouldNotBe null
        fetchJson[ID].asString shouldBe noteJson[ID].asString
    }

    @Test
    fun `fetch an article and fail`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        noteJson[ID].asString shouldNotBe null
        val fetch = postables.fetchPostablePage(nextDate = PG_DATE_POSITIVE_INFINITY, prevDate = null, limit = 2, fromActorId = null, toActorId = null, postableType = PostableType.Article)
        fetch.size shouldBe 0
    }

    @Test
    fun `fetch a note from an actor`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        noteJson[ID].asString shouldNotBe null
        val fetch = postables.fetchPostablePage(nextDate = PG_DATE_POSITIVE_INFINITY, prevDate = null, limit = 2, fromActorId = testUser1!!.second, toActorId = null, postableType = null)
        fetch.size shouldBe 1
        val fetchJson = JsonParser.parseString(fetch.first().activityPubJson).asJsonObject
        fetchJson[ID] shouldNotBe null
        fetchJson[ID].asString shouldBe noteJson[ID].asString
    }

    @Test
    fun `fetch a note from another actor and fail`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        noteJson[ID].asString shouldNotBe null
        val fetch = postables.fetchPostablePage(nextDate = PG_DATE_POSITIVE_INFINITY, prevDate = null, limit = 2, fromActorId = testUser2!!.second, toActorId = null, postableType = null)
        fetch.size shouldBe 0
    }

    @Test
    fun `fetch a note to an actor`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        noteJson[ID].asString shouldNotBe null
        val fetch = postables.fetchPostablePage(nextDate = PG_DATE_POSITIVE_INFINITY, prevDate = null, limit = 2, fromActorId = null, toActorId = person1, postableType = null)
        fetch.size shouldBe 1
        val fetchJson = JsonParser.parseString(fetch.first().activityPubJson).asJsonObject
        fetchJson[ID] shouldNotBe null
        fetchJson[ID].asString shouldBe noteJson[ID].asString
        fetchJson["attributedTo"].asString shouldBe "${testUser1!!.second}"
    }

    @Test
    fun `fetch a note to another actor and fail`() {
        val noteResponse = notes.createNote(
            NotePost(html = "foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        val noteJson = JsonParser.parseString(noteResponse!!.returnedJson).asJsonObject
        noteJson[ID].asString shouldNotBe null
        val fetch = postables.fetchPostablePage(nextDate = PG_DATE_POSITIVE_INFINITY, prevDate = null, limit = 2, fromActorId = null, toActorId = testUser2!!.second, postableType = null)
        fetch.size shouldBe 0
    }

    @Test
    fun `fetch notes in batches forward`() {
        for (i in 1..20) {
            notes.createNote(
                NotePost(html = "$i foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
                testUser1!!.first,
                appContext.dsf.jooqConfiguration()
            )
        }

        var nextDate: OffsetDateTime? = PG_DATE_POSITIVE_INFINITY
        var count = 0
        for (j in 1..6) {
            val notes = postables.fetchPostablePage(
                prevDate = null,
                nextDate = nextDate,
                fromActorId = null,
                toActorId = null,
                postableType = null,
                limit = 6
            )
            count += notes.size
            if (notes.size > 0 ) {
                nextDate = notes.last().modifiedAt
            } else {
                break
            }
        }

        count shouldBe 20
    }

    @Test
    fun `fetch notes paging backwards`() {
        for (i in 1..20) {
            notes.createNote(
                NotePost(html = "$i foobar", actorId = testUser1!!.second, fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
                testUser1!!.first,
                appContext.dsf.jooqConfiguration()
            )
        }

        val firstPage = postables.fetchPostablePage(
            prevDate = null,
            nextDate = PG_DATE_POSITIVE_INFINITY,
            fromActorId = null,
            toActorId = null,
            postableType = null,
            limit = 6
        )
        firstPage.size shouldBe 6

        val secondPage = postables.fetchPostablePage(
            prevDate = null,
            nextDate = firstPage.last().modifiedAt,
            fromActorId = null,
            toActorId = null,
            postableType = null,
            limit = 6
        )
        secondPage.size shouldBe 6

        val firstPageAgain = postables.fetchPostablePage(
            prevDate = secondPage.first().modifiedAt,
            nextDate = null,
            fromActorId = null,
            toActorId = null,
            postableType = null,
            limit = 6
        )
        firstPageAgain.size shouldBe 6
        firstPageAgain shouldBe firstPage
    }

    @Test
    fun `test create remote postable`() {
        postables.createRemotePostable(
            testUser1!!.second,
            PostableId("https://example.com/thing"),
           """{"id":"https://example.com/thing"}""",
            PostableType.Article,
            postingAddresses!!
        )
    }
}