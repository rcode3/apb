package com.rcode3.apb.core.dto

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class ValidatorsTests {

    @Test
    fun testIsSingleToken() {
        assertTrue(singleTokenRegex.matches("oneword"))
        assertFalse(singleTokenRegex.matches("two words"))
    }

    @Test
    fun hasNoSubdelims() {
        assertTrue(noSubDelimsRegex.matches("thingy"))
        assertFalse(noSubDelimsRegex.matches("thingy*g"))
        assertFalse(noSubDelimsRegex.matches("thingy\$g"))
    }
}