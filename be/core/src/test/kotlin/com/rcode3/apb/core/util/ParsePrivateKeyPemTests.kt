package com.rcode3.apb.core.util

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.security.Signature

class ParsePrivateKeyPemTests {

    @Test
    fun `test parsing of an rsa private key in PEM format`() {
        val pemKeyPair = genRsa2048()
        parseRsaPrivateKeyPem(pemKeyPair.privateKey)
    }

    @Test
    fun `test parsing of example rsa private key in PEM format`() {
        val key = """
            -----BEGIN RSA PRIVATE KEY-----
            MIIEogIBAAKCAQEA9CnV1ThcspKK8Re3ABVygxY49sFlC7PzQ5rPVJ++jsKCehJV
            MO+qZo0kh/Pzj01jZnlgSqW+3E473uRnob252CIAvMnG6xcFWdIyYKWdw9U73eQu
            3uUE8yXHb7aIltftvfU5eYLIFduYIfSrnHwl5qQgzZrLQvfaKBq02j6fsK0RWd74
            TmUIvkZUgOvuXD3+Xwp1PFQ3YDTu7UbysgDg2uc47qGqzxDM55NBorjRt+OvbpzV
            opCRx6cz70ZMDtao1aMPbxZwyCZXHRRPi1jJ4xONfUf+GiToHmONb2X1GPl24tLt
            qFg1a8LNPHgc6J8s4rzCk1I9yv8qSnvEL/EpSwIDAQABAoIBAHhHx7RjMJ8zu8ir
            MxaSejgDkYpVmU6bloI+WFbK3xf60EYMk/Uoi9w7ObzTu69jo00PDBWZ8x0aWToj
            cL3n9g54Kb7W8+xKCfPGrdSYodCgUf6scz6QudCCUu5AkKcmtlK25Fpsx96ksBxC
            KOxM6GrnfdVrpShXaxwIlAFTil2A3kHmZVUhMZFEuYkqiTLnT01aaifmpHoYyrCf
            G6b94O7NkNSi758Du8L6VOjxd7yFZgJ26YZVXMQoBPhHAYD2QgYx0tVC93uB8Ro8
            kfFxioR7aUjO9heJzfoTiivunKXXSQmrpc9zDvo6bcs9ZEbc1/0cmr4hGVEJfDp5
            tM35tokCgYEA/376skoB79ue69q7bGamy/ydeVmULO6+K1Sis7muKIbo+Y5v273c
            PO+yI4QK0uu6glu9cfz1LmCFvETkl7nmYKs61Iq5pv01fsI0trEWbfFXGhrfuvZm
            I2JB9o+NxdU+wtf4gCTraQYgiPnpHkBGZXNkfg1f5LFsmD/dwgTAN0cCgYEA9KUi
            HOYuD1vH45pdv/Ke+VHEsda1AAatRbTG0N6q2QYA9lo9z9g849bwouctz76h22Tv
            M1TMsT/XpNhE6BpbtZBPDdYtqn3hNiRqw6GnzM2OENILBYAnrZL5QCmzTvRnglt1
            NqAdUaHQitWHNDn6EFR/x+pHQp54cvFYyGPNh90CgYAHCmrCKwPupjC08ywjCpki
            Ge4FABDzRAj9jLGHHmDMQRt+dQxkE9FT5I8WJ3dc5J4ZJYoM8BmZk5Nb4tIh1D+i
            /lT6sm/3SBcHTTbCqhKiKPprf74Wmt+prfV/Y+lWQFAgcm8oqNvL4x1WaPyhlsfZ
            19o3FwtDdBg4AdqMlDDv1QKBgEbHtAvqaK8W3cky8ybCuk3l42OUe/+K7vy45cCu
            e32YqBSeXT3SESPWHv7ypaVyQP/pvu75/NLXZruRurXwUlJAcy7MpZ+rFcqFVMJO
            9e5iFlU1f+zSfChS2nw+1FrmiUAE1++6+WCR0XUpfBmXIIsy91lOpdlXYf/XZdls
            Tp2dAoGADkQEZxr0TcI1356HSQlicb4aOpVhbB99jKz7R7YNoDGdG9AeWLstgaq6
            BnWN2/IWBvwfE+zo8WX4nzgGS17ApfQaw/fscIfkgSdTYFUL5QyZMiFqZ3RywHDD
            u3ieTIrnZNOykVHeVjFK+xzq/6fFF+gaHp0XUl3GOth4ftr+BRw=
            -----END RSA PRIVATE KEY-----
        """.trimIndent()
        parseRsaPrivateKeyPem(key)
    }

    @Test
    fun `test signing and verifying with our keys`() {
        val toBeSigned = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        val pemKeyPair = genRsa2048()
        val privateKey = parseRsaPrivateKeyPem(pemKeyPair.privateKey)
        val signer = Signature.getInstance("SHA256withRSA")
        signer.initSign(privateKey)
        signer.update(toBeSigned.toByteArray())
        val signature = signer.sign()

        val verifier = Signature.getInstance("SHA256withRSA")
        val publicKey = parseRsaPublicKeyPem(pemKeyPair.publicKey)
        verifier.initVerify(publicKey)
        verifier.update(toBeSigned.toByteArray())
        verifier.verify(signature) shouldBe true
    }
}