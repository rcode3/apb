package com.rcode3.apb.core.logic

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LocalFileUploadTests {

    @Test
    fun testResizeStringWithExtension() {
        assertEquals("test--resized.txt", LocalFileUpload.Companion.getResizeString("test.txt"))
    }

    @Test
    fun testResizeStringWithoutExtension() {
        assertEquals("test_txt--resized", LocalFileUpload.Companion.getResizeString("test_txt"))
    }
}