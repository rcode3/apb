package com.rcode3.apb.core.app

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory

class TestAppContext(
    val configFileResource: String = "test-application.conf",
    val configMap: Map<String, Any>? = null
) : AppContext {
    override val config: Config
        get() {
            if (configMap != null) {
                val other = ConfigFactory.parseMap(configMap)
                val resoureConfig = ConfigFactory.load(configFileResource).resolve()
                return other.resolve().withFallback(resoureConfig)
            }
            //else
            return ConfigFactory.load(configFileResource).resolve()
        }
    override val dsf: DataSourceFactory
        get() = EmbeddedPostgresDataSourceFactory()
    override val persistenJobQueue: PersistentJobQueue
        get() = PersistentJobQueue(this)
    override val scheduledJobs: ScheduledJobs
        get() = ScheduledJobs(this)
    override val common: Common
        get() = Common(config)
}