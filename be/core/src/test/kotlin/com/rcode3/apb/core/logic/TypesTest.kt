package com.rcode3.apb.core.logic

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

class TypesTest {

    @Test
    fun `test ActorId`() {
        val id = "https://example.com/pub/p/person"
        assertEquals(id, ActorId(id).toString())
    }

    @Test
    fun `test PostableId`() {
        val id = "https://example.com/pub/a/person"
        assertEquals(id, PostableId(id).toString())
    }

    @Test
    fun `test WebfingerAcct`() {
        val id = "user@example.com"
        assertEquals(id, WebfingerAcct(id).toString())
    }

    @Test
    fun `test Username`() {
        val id = "Alice"
        assertNotEquals(id, Username(id))
        assertEquals(id.lowercase(), Username(id).toString())
    }

    @Test
    fun `test WebfingerJson`() {
        val json = """{"foo":"bar"}"""
        assertEquals(json, WebfingerJson(json).toString())
    }

    @Test
    fun `test ActivityPubJson`() {
        val json = """{"foo":"bar"}"""
        assertEquals(json, ActivityPubJson(json).toString())
    }

    @Test
    fun `test PreferredUsername`() {
        val id = "Alice"
        assertNotEquals(id, PreferredUsername(id).toString())
        assertEquals(id.lowercase(), PreferredUsername(id).toString())
    }
}