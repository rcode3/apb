package com.rcode3.apb.core.persistence

import com.google.gson.JsonParser
import com.rcode3.apb.core.Jigs
import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.constants.ACCT
import com.rcode3.apb.core.constants.ACTIVITY_PUB_JSON
import com.rcode3.apb.core.constants.ACTOR_ID
import com.rcode3.apb.core.constants.ID
import com.rcode3.apb.core.dto.ArticlePost
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.core.logic.*
import com.rcode3.apb.jooq.tables.references.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test


class ArticlePersistenceTests {
    private val appContext = TestAppContext()
    private val jigs by lazy {
        Jigs(appContext)
    }
    private val articleActions by lazy {
        ArticlePersistence(appContext)
    }
    private val postableActions by lazy {
        PostablePersistence(appContext)
    }

    private var testUser1: Pair<Username, ActorId>? = null
    private var testUser2: Pair<Username, ActorId>? = null
    private var person1: ActorId? = null
    private var postingAddresses: PostingAddresses? = null

    @BeforeEach
    fun setup() {
        testUser1 = jigs.createTestUser()
        testUser2 = jigs.createTestUser()
        person1 = jigs.createPerson(testUser1!!.first)
        postingAddresses = PostingAddresses(
            to = arrayOf(testUser1.toString()),
            cc = arrayOf(testUser2.toString()),
            bto = arrayOf(),
            bcc = arrayOf()
        )
    }

    @AfterEach
    fun tearDown() {
        appContext.dsf.jooqDsl().truncate(POSTABLE).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
    }

    @Test
    fun `test Create Article As Users Person`() {
        val articleResponse = articleActions.createArticle(
            ArticlePost(html = "foobar", actorId = testUser1!!.second, localId = "foo-bar", fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNotNull(articleResponse)
        val json = JsonParser.parseString(articleResponse!!.returnedJson).asJsonObject
        assertTrue(json[ACTOR_ID].asString.contains(punFromActorId(testUser1!!.second).toString()))
    }

    @Test
    fun `test Create Article As Users Other Person`() {
        val articleResponse = articleActions.createArticle(
            ArticlePost(html = "foobar", actorId = person1!!, localId = "foo-bar", fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNotNull(articleResponse)
        val json = JsonParser.parseString(articleResponse!!.returnedJson).asJsonObject
        assertTrue(json[ACTOR_ID].asString.contains(punFromActorId(person1!!).toString()))
    }

    @Test
    fun `test Fail To Create Article As Other Person`() {
        val articleResponse = articleActions.createArticle(
            ArticlePost(html = "foobar", actorId = testUser2!!.second, localId = "foo-bar", fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNull(articleResponse)
    }

    @Test
    fun `test Fail To Create Article As Users Other Person`() {
        val articleResponse = articleActions.createArticle(
            ArticlePost(html = "foobar", actorId = person1!!, localId = "foo-bar", fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser2!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNull(articleResponse)
    }

    @Test
    fun `test Fail To Create Article As a Suspended User`() {
        jigs.suspendUser(testUser1!!.first)
        val articleResponse = articleActions.createArticle(
            ArticlePost(html = "foobar", actorId = testUser1!!.second, localId = "foo-bar", fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNull(articleResponse)
    }

    @Test
    fun `test Fetch an Article`() {
        val articleResponse = articleActions.createArticle(
            ArticlePost(html = "foobar", actorId = testUser1!!.second, localId = "foo-bar", fileIds = arrayOf("1","2"), postingAddresses = postingAddresses!!),
            testUser1!!.first,
            appContext.dsf.jooqConfiguration()
        )
        assertNotNull(articleResponse)
        val articleJson = JsonParser.parseString(articleResponse!!.returnedJson).asJsonObject

        val postableFetchResponse = postableActions.fetchPostable(PostableId(articleJson[ID].asString))
        assertNotNull(postableFetchResponse)
        println(postableFetchResponse)
        val json = JsonParser.parseString(postableFetchResponse).asJsonObject
        assertEquals(articleJson[ACTOR_ID].asString,
            json[com.rcode3.apb.core.constants.ACTOR]
                .asJsonObject[ACTIVITY_PUB_JSON]
                .asJsonObject[ID]
                .asString
        )
        assertNotNull(
            json[com.rcode3.apb.core.constants.ACTOR]
                .asJsonObject[ACCT]
        )
        assertEquals(articleJson[ID].asString,
            json[com.rcode3.apb.core.constants.POSTABLE]
                .asJsonObject[ACTIVITY_PUB_JSON]
                .asJsonObject[ID]
                .asString
        )
    }
}