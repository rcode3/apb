package com.rcode3.apb.core.app

import io.zonky.test.db.postgres.embedded.EmbeddedPostgres
import org.flywaydb.core.Flyway
import javax.sql.DataSource

class EmbeddedPostgresDataSourceFactory : DataSourceFactory {
    override fun getInstance(): DataSource {
        return pg.postgresDatabase
    }

    companion object {
        val pg = EmbeddedPostgres.start()

        init {
            val flyway = Flyway.configure().dataSource(pg.postgresDatabase).load()
            flyway.migrate()
        }
    }
}