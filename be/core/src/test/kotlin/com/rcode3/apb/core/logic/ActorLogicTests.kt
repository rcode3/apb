package com.rcode3.apb.core.logic

import com.google.gson.JsonParser
import com.rcode3.apb.jooq.enums.ActorType
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ActorLogicTests {

    @Test
    fun `create webfinger json`() {
        val wf = webfingerJson(
            actorId = ActorId("testpun"),
            avatarUrl = "foo.png",
            avatarMimeType = "image/png",
            webfingerAcct = WebfingerAcct("testuser@example.com"),
            profileUrl = "https://example.com/testuser/profile"
        )
        val json = JsonParser.parseString(wf.toString()).asJsonObject
        Assertions.assertNotNull(json["links"])
        var avatarFound = false
        json["links"].asJsonArray.forEach { jsonElement ->
            val o = jsonElement.asJsonObject
            Assertions.assertNotNull(o["rel"])
            if (o["rel"].asString.equals("http://webfinger.net/rel/avatar")) {
                avatarFound = true
            }
        }
        avatarFound shouldBe true
    }

    @Test
    fun `create actor ap json`() {
        val ap = actorJson(
            actorId = ActorId("testuser"),
            preferredUserName = PreferredUsername("testpun"),
            publicKeyPem = """
                ----- BEGIN PULIC KEY ---
                abcdefghijklmnop
                ----- END PUBLIC KEY ---
            """.trimIndent(),
            iconUrl = "foo.png",
            bannerUrl = "bar.png",
            inboxUrl = "https://example.com/inbox",
            publicKeyId = "testuser@example.com",
            actorType = ActorType.Person,
            apBsseUrl = "https://example.com"
        )
        val json = JsonParser.parseString(ap.toString()).asJsonObject
        json["icon"] shouldNotBe null
        json["image"] shouldNotBe null
        json["endpoints"] shouldNotBe null
        json["endpoints"].asJsonObject["sharedInbox"].asString shouldBe "https://example.com/sharedInbox"
    }
}