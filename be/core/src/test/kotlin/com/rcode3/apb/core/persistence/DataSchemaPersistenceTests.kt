package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.persistence.DataSchemaPersistence
import com.rcode3.apb.core.mockAppContext
import com.rcode3.apb.core.mockDsf
import com.rcode3.apb.jooq.tables.records.FlywaySchemaHistoryRecord
import com.rcode3.apb.jooq.tables.references.FLYWAY_SCHEMA_HISTORY
import org.jooq.DSLContext
import org.jooq.Result
import org.jooq.impl.DSL
import org.jooq.impl.DefaultConfiguration
import org.jooq.tools.jdbc.MockDataProvider
import org.jooq.tools.jdbc.MockResult
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDateTime


class DataSchemaPersistenceTests {

    @Test
    fun testFetchFlywaySchema() {
        val provider = MockDataProvider {
            val create: DSLContext = DSL.using(DefaultConfiguration())
            val result: Result<FlywaySchemaHistoryRecord> = create.newResult(FLYWAY_SCHEMA_HISTORY)
            result.add(
                FlywaySchemaHistoryRecord(
                    installedRank = 1,
                    installedOn = LocalDateTime.now(),
                    version = "1",
                    description = "test 1",
                    type = "script",
                    checksum = 0,
                    installedBy = "test",
                    success = true,
                    executionTime = 30
                )
            )

            arrayOf(
                MockResult(1, result)
            )
        }

        val mockAppContext = mockAppContext(mockDsf(provider))

        assertEquals("1", DataSchemaPersistence(mockAppContext).fetchFlywaySchema())
    }

    @Test
    fun testFetchFlywaySchemaWithPg() {
        DataSchemaPersistence(TestAppContext()).fetchFlywaySchema()
    }
}