package com.rcode3.apb.core.persistence

import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.FILE
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals

@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FilePersistenceTests {

    private val appContext = TestAppContext()
    private val actions = FilePersistence(appContext)
    private val userName = "testy_mc_test_alot_fileactiontests"

    private var fileId1: Int? = null
    private var fileId2: Int? = null

    @BeforeAll
    fun setup() {
        val userActions = UserPersistence(appContext)
        val newUser = NewUserRegistration(
            name = userName,
            email = "testy@example.com",
            plainTextPassword = "a plain text password",
            status = UserPrincipalStatusType.Active
        )
        userActions.registerNewUser(newUser)
    }

    @AfterAll
    fun tearDown() {
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
        appContext.dsf.jooqDsl().truncate(FILE).cascade().execute()
    }

    @Test
    @Order(1)
    fun testAddFile() {
        fileId1 = actions.addFile(
            FilePersistence.General(
                url = "https://example.com/thing",
                localPath = "example/file",
                originalFilename = "afile.txt",
                owner = Username(userName),
                contentType = null,
                usage = null
            )
        )
    }

    @Test
    @Order(2)
    fun testAddThumbnail() {
        val tx = appContext.dsf.jooqConfiguration()
        FilePersistence.addResizedFile(
            tx,
            fileId1!!,
            resizeUrl = "http://example.com/thumb",
            resizeLocalPath = "example-thumbnail"
        )
    }

    @Test
    @Order(3)
    fun testRemoveFile() {
        actions.removeFile(fileId1!!, userName)
    }

    @Test
    @Order(4)
    fun testAddUnOwnedFile() {
        fileId1 = actions.addFile(
            FilePersistence.General(
                url = "https://example.com/thing",
                localPath = "example/file",
                originalFilename = "afile.txt",
                owner = Username(userName),
                contentType = null,
                usage = null
            )
        )
    }

    @Test
    @Order(5)
    fun testAddAnotherUnOwnedFile() {
        fileId2 = actions.addFile(
            FilePersistence.General(
                url = "https://example.com/thing",
                localPath = "example/file",
                originalFilename = "anotherfile.txt",
                owner = Username(userName),
                contentType = null,
                usage = null
            )
        )
    }

    @Test
    @Order(6)
    fun testGetFiles() {
        val files = actions.getFiles(arrayOf(fileId1.toString(), fileId2.toString()), userName)
        assertEquals(files.size, 2)
    }

    @Test
    @Order(7)
    fun testRemoveUnownedFile() {
        actions.removeFile(fileId1!!)
    }
}