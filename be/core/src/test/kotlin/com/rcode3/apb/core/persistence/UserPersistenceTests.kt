package com.rcode3.apb.core.persistence

import com.google.gson.JsonNull
import com.google.gson.JsonParser
import com.rcode3.apb.core.app.TestAppContext
import com.rcode3.apb.core.dto.NewActor
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.logic.PreferredUsername
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType
import com.rcode3.apb.jooq.tables.records.UserPrincipalRecord
import com.rcode3.apb.jooq.tables.references.ACTOR
import com.rcode3.apb.jooq.tables.references.ACTOR_OWNER
import com.rcode3.apb.jooq.tables.references.USER_PRINCIPAL
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*

@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserPersistenceTests {

    private val userName = Username("newUserRegistrationTest")
    private val userEmail = "newuser@example.com"
    private val userPassword = "password"
    private val usersActor = "newUserOtherActor"
    private val nonExistentUser = Username(userName.toString() + "notthere")
    private val wrongPassword = "wrong" + userPassword

    private val appContext = TestAppContext()

    @AfterAll
    fun tearDown() {
        appContext.dsf.jooqDsl().truncate(ACTOR_OWNER).cascade().execute()
        appContext.dsf.jooqDsl().truncate(ACTOR).cascade().execute()
        appContext.dsf.jooqDsl().truncate(USER_PRINCIPAL).cascade().execute()
    }

    @Test
    @Order(1)
    fun `test register new user`() {
        val action = UserPersistence(appContext)

        val newUser = NewUserRegistration(
            name = userName.toString(),
            email = userEmail,
            plainTextPassword = userPassword,
            status = UserPrincipalStatusType.Active
        )
        action.registerNewUser(newUser)

        assertNotNull(getUser())

        val personPersistence = PersonPersistence(appContext)
        val person = personPersistence.fetchPersonByPunAndOwner(PreferredUsername(userName.toString()), userName)
        assertNotNull(person)
        assertTrue(person!!.defaultActor)
    }


    @Test
    @Order(2)
    fun `test CheckUserNameAvailability`() {
        val action = UserPersistence(appContext)
        assertFalse(action.checkUserNameAvailability(userName))
        assertTrue(action.checkUserNameAvailability(nonExistentUser))

        val newPersonActor = NewActor(usersActor)
        val personActions = PersonPersistence(appContext)
        personActions.createNewPersonActor(newPersonActor, userName)

        assertFalse(action.checkUserNameAvailability(userName))
    }


    @Test
    @Order(3)
    fun `test CheckPassword`() {
        val action = UserPersistence(appContext)
        val goodLogin = action.checkPassword(userName, userPassword, "foo")
        assertNotNull(goodLogin)
        val goodLoginJson = JsonParser.parseString(goodLogin).asJsonObject
        assertEquals(userName.toString(), goodLoginJson["name"].asString)
        assertEquals(JsonNull.INSTANCE, goodLoginJson["theme"])
        assertEquals(JsonNull.INSTANCE, goodLoginJson["password"])
        assertNull(action.checkPassword(nonExistentUser, userPassword, "bar"))
        assertNull(action.checkPassword(userName, wrongPassword, "baz"))

        val userInDb = getUser()
        assertEquals("baz", userInDb!!.lastSeenFrom)
        assertNotNull(userInDb.lastSeenAt)
        assertNotNull(userInDb.lastLogin)
    }

    private fun getUser(): UserPrincipalRecord? {
        val userInDb = appContext.dsf.jooqDsl()
            .selectFrom(USER_PRINCIPAL)
            .where(USER_PRINCIPAL.NAME.eq(userName.toString()))
            .fetchOne()
        return userInDb
    }

    @Test
    @Order(4)
    fun `test IsUserActive`() {
        val action = UserPersistence(appContext)
        val result = action.isUserPrincipalActive(userName, "bubba")
        assertNotNull(result)

        val userInDb = getUser()
        assertEquals("bubba", userInDb!!.lastSeenFrom)
    }

    @Test
    @Order(5)
    fun `test SetUserTheme`() {
        val action = UserPersistence(appContext)
        action.setUserTheme(userName, "glorious")

        val userInDb = getUser()
        assertEquals("glorious", userInDb!!.theme)
    }

    @Test
    @Order(6)
    fun `test CheckPasswordForLastLogin`() {
        val action = UserPersistence(appContext)
        val goodLogin = action.checkPassword(userName, userPassword, "foo")
        assertNotNull(goodLogin)

        val userInDb = getUser()
        assertEquals("foo", userInDb!!.lastSeenFrom)
        assertNotNull(userInDb.lastSeenAt)
        assertNotNull(userInDb.lastLogin)
        assertNotNull(userInDb.previousLogin)
    }
}