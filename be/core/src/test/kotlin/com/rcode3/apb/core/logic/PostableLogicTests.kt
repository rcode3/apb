package com.rcode3.apb.core.logic

import com.google.gson.JsonParser
import com.rcode3.apb.core.dto.PostingAddresses
import com.rcode3.apb.jooq.enums.PostableType
import com.rcode3.apb.jooq.tables.pojos.File
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.equality.shouldBeEqualToComparingFields
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PostableLogicTests {

    private val actor1 = "https://example.com/actor1"
    private val actor2 = "https://example.com/actor2"
    private val postingAddresses = PostingAddresses(
        to = arrayOf(actor1), cc = arrayOf(actor2), bto = arrayOf(), bcc = arrayOf())
    private val attributedTo = ActorId("https://example.com/actor3")

    @Test
    fun `test postable json had id, name, and addressees`() {
        val postable = postableJsonString(PostableId("https://example.xyz"), "<p>test</p>", "foo", listOf(), PostableType.Note, postingAddresses, attributedTo)
        val json = JsonParser.parseString(postable).asJsonObject
        Assertions.assertEquals("https://example.xyz", json["id"].asString)
        Assertions.assertEquals("foo", json["name"].asString)
        Assertions.assertEquals(listOf(actor1), json["to"].asJsonArray.map { it.asString })
        Assertions.assertEquals(listOf(actor2), json["cc"].asJsonArray.map { it.asString })
    }

    @Test
    fun `test postable has no title`() {
        val postable = postableJsonString(PostableId("https://example.xyz"), "<p>test</p>", null, listOf(), PostableType.Note, postingAddresses, attributedTo)
        val json = JsonParser.parseString(postable).asJsonObject
        Assertions.assertEquals("https://example.xyz", json["id"].asString)
        Assertions.assertEquals("test", json["name"].asString)
    }

    @Test
    fun `test postable has one image`() {
        val file = File(originalFilename = "foo.txt", url = "https://example.com/foo", resizeUrl = "https://example.com/resizedFoo")
        val postable = postableJsonString(PostableId("https://example.xyz"), "<p>test</p>", null, listOf(file), PostableType.Note, postingAddresses, attributedTo)
        val json = JsonParser.parseString(postable).asJsonObject
        Assertions.assertEquals("https://example.xyz", json["id"].asString)
        Assertions.assertEquals("test", json["name"].asString)
        Assertions.assertEquals(file.url, json["image"].asJsonObject["url"].asString)
        Assertions.assertEquals(file.resizeUrl, json["icon"].asJsonObject["url"].asString)
    }

    @Test
    fun `test postable with multiple images`() {
        val file1 = File(originalFilename = "foo.txt", url = "https://example.com/foo", resizeUrl = "https://example.com/resizedFoo")
        val file2 = File(originalFilename = "bar.txt", url = "https://example.com/bar", resizeUrl = "https://example.com/resizedBar")
        val postable = postableJsonString(PostableId("https://example.xyz"), "<p>test</p>", null, listOf(file1, file2), PostableType.Note, postingAddresses, attributedTo)
        val json = JsonParser.parseString(postable).asJsonObject
        Assertions.assertEquals("https://example.xyz", json["id"].asString)
        Assertions.assertEquals("test", json["name"].asString)
        Assertions.assertEquals(file1.url, json["image"].asJsonArray.first().asJsonObject["url"].asString)
        Assertions.assertEquals(file1.resizeUrl, json["icon"].asJsonArray.first().asJsonObject["url"].asString)
        Assertions.assertEquals(file2.url, json["image"].asJsonArray.last().asJsonObject["url"].asString)
        Assertions.assertEquals(file2.resizeUrl, json["icon"].asJsonArray.last().asJsonObject["url"].asString)
    }

    @Test
    fun `test parsing of activity pub postable`() {
        val postableId = PostableId("https://example.xyz")
        val file1 = File(originalFilename = "foo.txt", url = "https://example.com/foo", resizeUrl = "https://example.com/resizedFoo")
        val file2 = File(originalFilename = "bar.txt", url = "https://example.com/bar", resizeUrl = "https://example.com/resizedBar")
        val postable = postableJsonString(postableId, "<p>test</p>", null, listOf(file1, file2), PostableType.Note, postingAddresses, attributedTo)
        val parsed = parsePostableActivityPub(postable)
        parsed.postableId shouldBe postableId
        parsed.postableType shouldBe PostableType.Note
        parsed.postingAddresses shouldBe postingAddresses
        parsed.owningActor shouldBe attributedTo
        parsed.iconUrls shouldContain "https://example.com/resizedBar"
        parsed.iconUrls shouldContain "https://example.com/resizedFoo"
        parsed.imageUrls shouldContain "https://example.com/bar"
        parsed.imageUrls shouldContain "https://example.com/foo"
    }
}