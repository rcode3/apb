package com.rcode3.apb.core

import com.rcode3.apb.core.app.AppContext
import com.rcode3.apb.core.dto.NewActor
import com.rcode3.apb.core.dto.NewUserRegistration
import com.rcode3.apb.core.logic.ActorId
import com.rcode3.apb.core.logic.Username
import com.rcode3.apb.core.persistence.ActorPersistence
import com.rcode3.apb.core.persistence.PersonPersistence
import com.rcode3.apb.core.persistence.UserPersistence
import com.rcode3.apb.jooq.enums.UserPrincipalStatusType

class Jigs(val appContext: AppContext) {

    val plainTextPassword = "secretpassword"

    val userActions by lazy {
        UserPersistence(appContext)
    }

    val personActions by lazy {
        PersonPersistence(appContext)
    }

    val actorActions by lazy {
        ActorPersistence(appContext)
    }

    var userCount = 0
    var personCount = 0

    fun createTestUsers(count: Int = 1, status: UserPrincipalStatusType = UserPrincipalStatusType.Active) {
        (1..count).forEach {
            createTestUser(status)
        }
    }

    fun createTestUser(status: UserPrincipalStatusType = UserPrincipalStatusType.Active): Pair<Username, ActorId> {
        val name = "testUser$userCount"
        val retval = userActions.registerNewUser(
            NewUserRegistration(
                name = name,
                email = "testUser$userCount@example.com",
                plainTextPassword = plainTextPassword,
                status = status
            )
        )
        userCount++
        return retval
    }

    fun createPerson(owner: Username): ActorId {
        val pun = "person$personCount"
        val retval = personActions.createNewPersonActor(
            NewActor(pun, false, false),
            owner
        )
        personCount++
        return retval
    }

    fun suspendUser(username: Username) = userActions.suspendUser(username)
}