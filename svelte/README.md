# APB Svelte/Routify

Created with the Routify-TS starter found here: https://github.com/lamualfa/routify-ts

## To setup

```shell
npm install
```

## Run unit tests

```shell
npm test
```

## Dev mode

```shell
npm run dev
```

## Build

To build with no SSG:

```shell
npm run build:app
```

Be default, the API end point is `/api` (i.e. a relative path).
To build it for a different end point, use the environment variable
`APB_API_BASE_URL` such as:

```shell
export APB_API_BASE_URL="http://localhost:8000/api"
npm run build:app
```

## Serve

To serve with no SSR:

```shell
npm run serve:nossr
```