import {noteUrl} from "../../src/api/notes";

describe("test making UI URL for ntoes", () =>{

    let noteBaseUrl = "https://apb.example.com/actpub/note"

    it('should return /o for nonlocal', function () {
        let id = "https://foo.example.com/note/blah"
        expect(noteUrl(id, noteBaseUrl)).toBe(`/o?note=${encodeURIComponent(id)}`)
    });

    it('should return /p/pun/n/note for local', function () {
        let id = "https://apb.example.com/actpub/note/alice_2021-05-13-542232"
        expect(noteUrl(id,noteBaseUrl)).toBe(`/p/alice/n/2021-05-13-542232`)
    })

    it('should return /p/pun/n/note for local even with trailing slash', function () {
        let id = "https://apb.example.com/actpub/note/alice_2021-05-13-542232"
        expect(noteUrl(id,noteBaseUrl + "/")).toBe(`/p/alice/n/2021-05-13-542232`)
    })
})