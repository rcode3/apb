import {imageUrls} from "../../src/common/imageUrls";

describe("get image urls from activity pub", () =>{

    it('should return empty array if no image', function () {
        const noteApJson = {
        }
        expect(imageUrls(noteApJson).length).toBe(0)
    });

    it('should return one url', function () {
        const noteApJson = {
            image: {
                type: "Image",
                name: "foo",
                url: "https://example.com"
            }
        }
        expect(imageUrls(noteApJson).length).toBe(1)
    });

    it('should return two urls', function () {
        const noteApJson = {
            image: [
                {
                    type: "Image",
                    name: "foo",
                    url: "https://example.com/foo"
                },
                {
                    type: "Image",
                    name: "bar",
                    url: "https://example.com/bar"
                }
            ]
        }
        expect(imageUrls(noteApJson).length).toBe(2)
    });
})