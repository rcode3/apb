import {actorUrl} from "../../src/common/actorUrl";

describe("get actor urls from actor id", () =>{
    let personBaseUrl = "https://apb.example.com/pub/p"
    let groupBaseUrl = "https://apb.example.com/pub/g"

    it('should return /o for nonlocal', function () {
        let id = "https://foo.example.com/actor/blah"
        expect(actorUrl(id, personBaseUrl)).toBe(`/o?actor=${encodeURIComponent(id)}`)
    });

    it('should return /p/pun for local person', function () {
        let id = "https://apb.example.com/pub/p/alice"
        expect(actorUrl(id,personBaseUrl)).toBe(`/p/alice`)
    })

    it('should return /p/pun for local person with trailing slash', function () {
        let id = "https://apb.example.com/pub/p/alice"
        expect(actorUrl(id,personBaseUrl + "/")).toBe(`/p/alice`)
    })

    it('should return /g/pun for local group', function () {
        let id = "https://apb.example.com/pub/g/alice*g"
        expect(actorUrl(id,groupBaseUrl)).toBe(`/g/alice`)
    })

    it('should return /g/pun for local group with trailing slash', function () {
        let id = "https://apb.example.com/pub/g/alice*g"
        expect(actorUrl(id,groupBaseUrl + "/")).toBe(`/g/alice`)
    })
})