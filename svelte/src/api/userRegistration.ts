import {API_BASE_URL} from "./apibaseurl";
import {userinfo} from "../stores/userinfostore";
import {sessionExpiry} from "../stores/sessionExpiry";

export async function loginUser(username : string, password : string) : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/login/user`
    let body = JSON.stringify({"name": username, "password": password})
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    });

    if (response.ok) {
        let data = await response.json()
        let result = data["result"]
        if (result) {
            userinfo.set(data["user"])
            sessionExpiry.set({expiry: new Date(data["sessionExpiry"])})
            startBackgroundIntervals()
        }
        return result
    } else {
        throw new Error("logging in user");
    }
}

export async function logout() : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/login/logout`
    const response = await self.fetch(fetchURL);

    userinfo.set(null)
    cancelBackgroundIntervals()
    if (response.ok) {
        let data = await response.json()
        return data["result"]
    } else {
        console.log("logout return error")
        return false
    }
}

export async function refreshLogin() : Promise<boolean> {
    console.log("refreshing login")
    let fetchURL = `${API_BASE_URL}/login/refresh`
    const response = await self.fetch(fetchURL);

    if (response.ok) {
        let data = await response.json()
        sessionExpiry.set({expiry: new Date(data["sessionExpiry"])})
        return true
    } else {
        console.log("refresh login error")
        return false
    }
}

let sessionExpirationDate : Date
const sessionExpirySubscription = sessionExpiry.subscribe( value => {
    if (value) {
        sessionExpirationDate = value.expiry
    }
})

let expirationCheckInternvalId : NodeJS.Timeout = null
let refreshIntervalId : NodeJS.Timeout = null

function startBackgroundIntervals() {
    expirationCheckInternvalId = setInterval(() => {
        if (sessionExpirationDate < new Date()) {
            userinfo.set(null)
            cancelBackgroundIntervals()
            window.location.assign("/login")
        }
    }, 60000)

    refreshIntervalId = setInterval( () => {
        refreshLogin().then(()=>{})
    }, 3600000)
}

function cancelBackgroundIntervals() {
    if (expirationCheckInternvalId) {
        console.log("cancelling background interval for expiration check")
        clearInterval(expirationCheckInternvalId)
    }
    if (refreshIntervalId) {
        console.log("cancelling refresh interval")
        clearInterval(refreshIntervalId)
    }
}

export async function fetchUserAvailabilty(name : string) : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/login/available/${name}`
    const response = await self.fetch(fetchURL);

    if (response.ok) {
        let available = await response.json()
        return !available["result"]
    } else {
        throw new Error("fetching user availability");
    }
}

export async function registerUser(username : string, email : string, password : string) : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/login/register`
    let body = JSON.stringify({
        "name": username,
        "email": email,
        "plainTextPassword": password
    })
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    });

    if (response.ok) {
        let available = await response.json()
        return available["result"]
    } else {
        throw new Error("registering user");
    }
}
