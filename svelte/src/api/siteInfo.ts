import {API_BASE_URL} from "./apibaseurl";
import {siteinfo} from "../stores/siteinfostore";

export const SITE_SHORT_NAME = "shortName"
export const SITE_LONG_NAME = "longName"
export const SITE_REGISTRATION_ENABLED = "registrationEnabled"
export const SITE_DEFAULT_THEME = "defaultTheme"
export const SITE_HOST = "host"
export const SITE_AP_BASE_URL = "apBaseUrl"
export const SITE_AP_NOTE_BASE_URL = "apNoteBaseUrl"
export const SITE_AP_ARTICLE_BASE_URL = "apArticleBaseUrl"
export const SITE_AP_PERSON_BASE_URL = "apPersonBaseUrl"

export async function fetchSiteInfo() {
    let fetchURL = `${API_BASE_URL}/site/info`
    const response = await self.fetch(fetchURL);

    if (response.ok) {
        let data = await response.json()
        siteinfo.set(data)
        return data
    } else {
        throw new Error("fetching site info");
    }
}
