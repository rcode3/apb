import {API_BASE_URL} from "./apibaseurl";
import type {PostingAddresses} from "./postables";

export interface NotePostResponse{
    id: string
    preferredUsername: string
    actorId: string,
}

export async function postNote(actorId: string, html: string, fileIds: string[], postingAddresses: PostingAddresses) : Promise<NotePostResponse> {
    let fetchURL = `${API_BASE_URL}/notes`
    let body = JSON.stringify({
        "actorId" : actorId,
        "html" : html,
        "fileIds": fileIds,
        "postingAddresses": {
            "to": postingAddresses.to,
            "bto": [],
            "cc": [],
            "bcc": []
        }
    })
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    })

    if (response.ok) {
        let data = await response.json()
        return data
    } else {
        throw new Error(`error posting note:${response.status} - ${response.statusText}`)
    }
}

export function noteUrl(id: string, noteBaseUrl: string): string {
    if (id.startsWith(noteBaseUrl)) {
        let localId = id.substr(noteBaseUrl.length)
        if (localId.startsWith("/")) {
            localId = localId.substring(1, localId.length)
        }
        let parts = localId.split(",", 2)
        if (parts.length < 2 ) {
            throw new Error(`note id is malformed for local note: ${id}`)
        }
        return `/p/${parts[0]}/n/${parts[1]}`
    }
    //else
    return `/o?note=${encodeURIComponent(id)}`
}

export function localNoteId(localPart: string, preferredUsername: string, noteBaseUrl: string): string {
   return `${noteBaseUrl}/${preferredUsername},${localPart}`
}
