import {API_BASE_URL} from "./apibaseurl";

export interface ActorInfo {
    activityPubJson: object,
    webfingerJson: object,
    accessibilityStatus: string,
    lastHeardAt: string,
    lastCheckedAt: string,
    createdAt: string,
    modifiedAt: string,
    defaultActor: boolean
}

//TODO provide a way to narrow the actor type
export async function getAllActors(nextDate: string | null, limit: number) : Promise<Array<ActorInfo>> {
    let fetchUrl = new URL(`${API_BASE_URL}/actors`, location.href)
    let searchParams = new URLSearchParams({limit: limit.toString()})
    if (nextDate) {
        searchParams.append("nextDate", nextDate)
    }
    fetchUrl.search = searchParams.toString()
    const response = await self.fetch(fetchUrl.toString())
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error("error fetching all actors")
    }
}

export async function getActor(actorId: string) : Promise<ActorInfo> {
    let fetchUrl = new URL(`${API_BASE_URL}/actors`, location.href)
    let searchParams = new URLSearchParams({actorId: actorId})
    fetchUrl.search = searchParams.toString()
    const response = await self.fetch(fetchUrl.toString())
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error(`error fetching actorId ${actorId}`)
    }
}

export async function getAccount(acct: string) : Promise<ActorInfo> {
    let fetchUrl = new URL(`${API_BASE_URL}/webfinger`, location.href)
    let searchParams = new URLSearchParams({acct: acct})
    fetchUrl.search = searchParams.toString()
    const response = await self.fetch(fetchUrl.toString())
    if (response.ok) {
        return await response.json()
    } else if (response.status == 404) {
        return null
    } else {
        throw new Error(`error fetching acct ${acct}`)
    }

}

