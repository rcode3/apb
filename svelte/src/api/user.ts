import {API_BASE_URL} from "./apibaseurl";
import {isUserLoggedIn} from "../stores/userinfostore";

export async function setUserTheme(themeName : string) : Promise<boolean> {
    if (!isUserLoggedIn()) {
        return false
    }
    let fetchURL = `${API_BASE_URL}/user/theme`
    let body = JSON.stringify({
        "theme": themeName
    })
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    });

    if (response.ok) {
        let available = await response.json()
        return available["result"]
    } else {
        throw new Error("setting user theme");
    }
}

export interface UserInfo {
    name: string,
    lastSeenAt: string,
    lastSeenFrom: string,
    lastLogin: string,
    previousLogin: string,
    email: string,
    emailVerified: string,
    emailVerifiedAt: string,
    theme: string,
    status: string,
    createdAt: string,
    modifiedAt: string
}

export async function getUserInfo() : Promise<UserInfo> {
    let fetchURL = `${API_BASE_URL}/user/info`
    const response = await self.fetch(fetchURL);
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error("error fetching user info")
    }
}

