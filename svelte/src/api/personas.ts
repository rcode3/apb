import {API_BASE_URL} from "./apibaseurl";

export interface PersonaInfo {
    preferredUsername: string,
    status: string,
    createdAt: string,
    modifiedAt: string,
    defaultActor: boolean
}

export interface Persona {
    preferredUsername: string,
    status: string,
    createdAt: string,
    modifiedAt: string,
    id: string,
    acct: string,
    visibility: string,
    defaultActor: boolean,
    activityPubJson: object
}

export interface Person {
    activityPubJson: object,
    webfingerJson: object,
    accessibilityStatus: string,
    lastHeardAt: string,
    lastCheckedAt: string,
    createdAt: string,
    modifiedAt: string,
}

export async function getPersonas() : Promise<PersonaInfo[]> {
    let fetchUrl = `${API_BASE_URL}/personas`
    const response = await self.fetch(fetchUrl)
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error("error fetching personas")
    }
}

export async function getPersona(persona: string) : Promise<Persona> {
    let fetchUrl = `${API_BASE_URL}/personas/${persona}`
    const response = await self.fetch(fetchUrl)
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error(`error fetching persona ${persona}`)
    }
}

/**
 * Gets a local person.
 * @param pun
 */
export async function getPerson(pun: string) : Promise<Person> {
    let fetchUrl = `${API_BASE_URL}/persons/${pun}`
    const response = await self.fetch(fetchUrl)
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error(`error fetching person ${pun}`)
    }
}

export async function getAllPersons(nextDate: string | null, limit: number) : Promise<Array<Person>> {
    let fetchUrl = new URL(`${API_BASE_URL}/persons`, location.href)
    let searchParams = new URLSearchParams({limit: limit.toString()})
    if (nextDate) {
        searchParams.append("nextDate", nextDate)
    }
    fetchUrl.search = searchParams.toString()
    const response = await self.fetch(fetchUrl.toString())
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error("error fetching all persons")
    }
}

/**
 * Returns true if the name is taken
 * @param name
 */
export async function checkPersonaAvailable(name: string): Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/personas/${name}/check`
    const response = await self.fetch(fetchURL);

    if (response.ok) {
        let available = await response.json()
        return !available["result"]
    } else {
        throw new Error("fetching persona availability");
    }
}

export async function registerPersona(personaName : string) : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/personas`
    let body = JSON.stringify({
        "preferredUsername": personaName
    })
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    });

    if (response.ok) {
        let available = await response.json()
        return available["result"]
    } else {
        throw new Error("registering persona");
    }
}

export async function setPersonaVisibility(preferredUsername: string, visibility: string) : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/personas/${preferredUsername}/visibility`
    let body = `"${visibility}"`
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    });

    if (response.ok) {
        let available = await response.json()
        return available["result"]
    } else {
        throw new Error("setting visibility");
    }
}

export async function setAsDefaultActor(preferredUsername: string) : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/personas/${preferredUsername}/default`
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
    });

    if (response.ok) {
        let available = await response.json()
        return available["result"]
    } else {
        throw new Error("setting actor as default");
    }
}

export async function setPersonaNameSummary(preferredUsername: string, name: string, summary: string) : Promise<boolean> {
    let fetchURL = `${API_BASE_URL}/personas/${preferredUsername}/nameSummary`
    let body = JSON.stringify({
        "name": name,
        "summary": summary
    })
    const response = await self.fetch(fetchURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    });

    if (response.ok) {
        let available = await response.json()
        return available["result"]
    } else {
        throw new Error("setting nameSummary");
    }
}
