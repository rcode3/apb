import {API_BASE_URL} from "./apibaseurl";

export interface UploadInfo {
    type: string,
    url: string
}

export async function getUploadInfo() : Promise<UploadInfo> {
    let fetchURL = `${API_BASE_URL}/uploadInfo`
    const response = await self.fetch(fetchURL)
    if (response.ok) {
        return await response.json()
    } else {
        throw new Error("error getting upload info")
    }
}
