import {API_BASE_URL} from "./apibaseurl";

export interface PostingAddresses {
    to: string[],
    bto: string[],
    cc: string[],
    bcc: string[]
}

export interface PostableFetchResponse{
    postable: object,
    actor: object
}

export async function getPostable(id: string) : Promise<PostableFetchResponse> {
    let fetchUrl = new URL(`${API_BASE_URL}/postables`,location.href)
    fetchUrl.search = new URLSearchParams({id: id}).toString()
    const response = await self.fetch(fetchUrl.toString())

    if (response.ok) {
        let note = await response.json()
        return note
    } else {
        throw new Error(`error fetching postable:${response.status} - ${response.statusText}`)
    }
}
