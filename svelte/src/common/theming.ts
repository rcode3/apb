/**
 * This file sets the theme for the website. It is separate from the users preference logic because the
 * site could have a default theme as well as a visitor can set a default theme in localstorage.
 *
 * The order of preference is: user's saved theme, local storage theme, site default, software default
 */
import {setUserTheme} from "../api/user";
import {userinfo} from "../stores/userinfostore";

export function setAndSaveTheme(themeName: string) {
    let ctName = canonicalizeThemeName(themeName)
    saveLocalTheme(ctName)
    setUserTheme(ctName)
    setTheme(ctName)
}

export const APB_DEFAULT_THEME = "theme_green_on_black"

export function canonicalizeThemeName(themeName: string) : string {
    if (!themeName || themeName === "") {
        return APB_DEFAULT_THEME
    }
    //else
    return themeName
}

const THEME_STORAGE_NAME = "theme"

export function saveLocalTheme(themeName: string) {
    localStorage.setItem(THEME_STORAGE_NAME, themeName)
}

export function setSiteOrLocalTheme(siteThemName: string) {
    let localThemeName = localStorage.getItem(THEME_STORAGE_NAME)
    if (localThemeName) {
        setTheme(canonicalizeThemeName(localThemeName))
    } else if (siteThemName) {
        setTheme(canonicalizeThemeName(siteThemName))
    }
}

export function setThemeFromUser() {
    let userTheme = null
    userinfo.subscribe(value =>{
        if (value) {
            userTheme = value.theme
        }
    })
    if (userTheme) {
        setTheme(userTheme)
    }
}

export function setTheme(themeName: string) {
    window.document.body.classList.add(themeName);
    window.document.body.classList.forEach( function (value){
        if (value != themeName) {
            window.document.body.classList.remove(value);
        }
    });
}
