export function actorUrl(actorId: string, actorBaseUrl: string): string {
    if (actorId.startsWith(actorBaseUrl)) {
        let localId = actorId.substr(actorBaseUrl.length)
        if (localId.startsWith("/")) {
            localId = localId.substring(1, localId.length)
        }
        if (localId.endsWith("*g")) {
            localId = localId.substring(0, localId.length - 2)
            return `/g/${localId}`
        }
        //else
        return `/p/${localId}`
    }
    //else
    return `/o?actor=${encodeURIComponent(actorId)}`
}

export function getActorId(preferredUsername: string, actorBaseUrl: string): string {
    return `${actorBaseUrl}/${preferredUsername}`
}