export interface imageUrl {
    name: string,
    url: string
}

export function imageUrls(noteApJson: object) : imageUrl[] {
    let retval: imageUrl[] = []
    if (noteApJson["image"]) {
        if (Array.isArray(noteApJson["image"])) {
            noteApJson["image"].forEach( (image) => {
                let i: imageUrl = {name: image.name, url: image.url}
                retval.push(i)
            })
        } else {
            let image = noteApJson["image"]
            let i: imageUrl = {name: image.name, url: image.url}
            retval.push(i)
        }
    }
    return retval
}
