import { writable } from 'svelte/store';

export const siteinfo = writable({
    shortName : "apb",
    longName : "activity pub bbs",
    registrationEnabled: false
});