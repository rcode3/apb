import { writable } from 'svelte/store';

const USERINFO_LS = "apb_userinfo"

const storedUserinfo : UserInfoType = JSON.parse(localStorage.getItem(USERINFO_LS))
export const userinfo = writable(storedUserinfo);

export interface UserInfoType {
    name: string,
    theme?: string
}
userinfo.subscribe(value => {
    localStorage.setItem(USERINFO_LS, JSON.stringify(value));
});

export function isUserLoggedIn() : boolean {
    let retval = false
    userinfo.subscribe( value => {
        if (value) {
            retval = true
        }
    })
    return retval
}