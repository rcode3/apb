import { writable } from "svelte/store";

export const sessionExpiry = writable({
    expiry : new Date()
});